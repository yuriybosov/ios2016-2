//
//  ViewController.swift
//  lesson_74_Share
//
//  Created by Yurii Bosov on 7/23/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

import UIKit
import NoteData

class ViewController: UIViewController {
    
    let dataSource = Note.noteList()
    
    @IBOutlet weak var createButton: UIBarButtonItem?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("app dataSource \(dataSource.count)")
        //        createButton?.isEnabled = dataSource.isEmpty
    }
    
    @IBAction func createButtonClicked() {
        Note.createNoteWith(title: "title1", text: "text1")
        Note.createNoteWith(title: "title2", text: "text2")
        Note.createNoteWith(title: "title3", text: "text3")
        //        createButton?.isEnabled = false
    }
}


