//
//  ShareViewController.swift
//  NoteShare
//
//  Created by Yurii Bosov on 7/23/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

import UIKit
import Social
import MobileCoreServices

class ShareViewController: SLComposeServiceViewController {

    override func isContentValid() -> Bool {
        return true
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let context = self.extensionContext else {
            return
        }
        
        guard let inputItems = context.inputItems as? [NSExtensionItem] else {
            return
        }
        print("==========================================")
        for item in inputItems {
            guard let attachments = item.attachments as? [NSItemProvider]  else {
                return
            }
            
            for provider in attachments {
                
                let typeText = kUTTypeText as String
                let typeURL = kUTTypeURL as String
                
                if provider.hasItemConformingToTypeIdentifier(typeText) {
                    
                    provider.loadItem(forTypeIdentifier: typeText, options: nil, completionHandler: { (coding, error) in
                        if let str = coding as? String {
                            print("!!!!!! share text \(str)")
                        }
                    })
                    
                } else if provider.hasItemConformingToTypeIdentifier(typeURL) {
                    provider.loadItem(forTypeIdentifier: typeURL, options: nil, completionHandler: { (coding, error) in
                        if let url = coding as? URL {
                            print("!!!!!! share url \(url)")
                        }
                    })
                }
            }
        }
    }
    
    override func didSelectPost() {
        self.extensionContext!.completeRequest(returningItems: [], completionHandler: nil)
    }

    override func configurationItems() -> [Any]! {
        return []
    }

}
