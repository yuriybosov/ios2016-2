//
//  main.m
//  lesson 5_NSArray
//
//  Created by Yuriy Bosov on 7/21/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        // класс для числовых значений
        NSNumber *num1 = [NSNumber numberWithInt:10];
        NSLog(@"num1 = %@", num1);
        
        NSNumber *num2 = [NSNumber numberWithBool:YES];
        NSLog(@"num2 = %@", num2);
        
        // сокращенный способ создание и инициализации объекта NSNumber *
        NSNumber *num3 = @(10.0);
        NSLog(@"num3 = %@", num3);
        
        CGFloat f = num3.floatValue;
        NSLog(@"f = %f", f);
        
        NSNumber *num4 = @(num1.integerValue + num3.integerValue);
        NSLog(@"num4 = %@", num4);
        
        // num to str
        NSString *str = num4.stringValue;
        NSLog(@"str = %@", str);
        
        // метод сравнения для NSNumber
        if ([num1 isEqualToNumber:num3]) {
            NSLog(@"равны");
        } else {
            NSLog(@"НЕ равны");
        }
        
        //массив
        // метод создания массива (в конце nil обязательно)
        NSArray *array = [NSArray arrayWithObjects:num1, num2, num3, num4, str, nil];
        NSLog(@"array = %@", array);
        
        // аналогичный метод, но в "сокращенном" виде
        array = @[num1, num2, num3, num4];
        NSLog(@"array = %@", array);
        
        // методы NSArray
        // получить кол-во элементов в массиве
        NSUInteger count = array.count;
        NSLog(@"кол-во элементов в массиве = %lu", count);
        
        // получить объект по индексу
        NSUInteger index = 2;
        if (index < count) {
            
            id obj = [array objectAtIndex:index];
            // или "сокращенным" способом
            obj = array[index];
            
            NSLog(@"objectAtIndex %lu = %@", index, obj);
        } else {
            NSLog(@"Выход за пределы массива");
        }
        
        // получить индекс по объекту
        NSNumber *num5 = @(10.04);
        index = [array indexOfObject:num5];
        if (index == NSNotFound) {
            NSLog(@"%@ НЕ содержится в массиве", num5);
        } else {
            NSLog(@"indexOfObject %@ = %lu", num5, index);
        }
        
        // узнать, содержится ли объект в массиве
        if ([array containsObject:num5]) {
            NSLog(@"%@ содержится в массиве", num5);
        } else {
            NSLog(@"%@ НЕ содержится в массиве", num5);
        }
        
        // получить первый элемент массива
        id obj = array.firstObject;
        
        // получить послдений элемент массива
        obj = array.lastObject;
        
        //
        NSString *allObjects = [array componentsJoinedByString:@","];
        NSLog(@"allObjects = %@", allObjects);
    }
    return 0;
}
