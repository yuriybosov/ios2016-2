//
//  main.m
//  lesson 8_1 NOARC
//
//  Created by Yuriy Bosov on 8/28/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyClass.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        MyClass *obj1 = [[MyClass alloc] init];
    
        NSLog(@"%lu", obj1.retainCount);
        [obj1 retain];  // увеличивает счетчик ссылок на 1
        
        NSArray *array = [[[NSArray alloc] initWithObjects:obj1, nil] autorelease];
        
        [obj1 release];
        [obj1 release];
    }
    return 0;
}
