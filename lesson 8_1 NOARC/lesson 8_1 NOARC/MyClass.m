//
//  MyClass.m
//  lesson 8_1 NOARC
//
//  Created by Yuriy Bosov on 8/28/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "MyClass.h"

@implementation MyClass

- (void)dealloc {
    [super dealloc];
    NSLog(@"%@ dealloc", NSStringFromClass([self class]));
}

@end
