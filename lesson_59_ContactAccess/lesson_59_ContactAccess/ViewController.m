//
//  ViewController.m
//  lesson_59_ContactAccess
//
//  Created by Yurii Bosov on 5/28/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"
#import <Contacts/Contacts.h>
#import <ContactsUI/ContactsUI.h>
#import <MessageUI/MessageUI.h>

@interface ViewController () <CNContactPickerDelegate, CNContactViewControllerDelegate, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate> {
    
    CNContactStore *store;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Contacts Access";
    
    store = [[CNContactStore alloc] init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(contactStoreDidChange:) name:CNContactStoreDidChangeNotification object:nil];
}

#pragma mark - Actions

- (IBAction)selectContactButtonClicked {
    CNContactPickerViewController *controller = [CNContactPickerViewController new];
    controller.delegate = self;
    [self presentViewController:controller animated:YES completion:nil];
}

- (IBAction)createNewEmptyContactButtonClicked {
    CNContactViewController *controller = [CNContactViewController viewControllerForNewContact:nil];
    controller.delegate = self;
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:controller];
    [self presentViewController:nc animated:YES completion:nil];
}

- (IBAction)createNewContactButtonClicked {
    
    CNMutableContact *contact = [CNMutableContact new];
    contact.givenName = @"Vasilii";
    contact.familyName = @"Pupkin";
    
    CNPhoneNumber *number = [CNPhoneNumber phoneNumberWithStringValue:@"+380505350926"];
    CNLabeledValue *value = [CNLabeledValue labeledValueWithLabel:CNLabelPhoneNumberMobile value:number];
    contact.phoneNumbers = @[value];
    
    CNContactViewController *controller = [CNContactViewController viewControllerForNewContact:contact];
    controller.delegate = self;
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:controller];
    [self presentViewController:nc animated:YES completion:nil];
}

- (IBAction)contactListButtonClicked:(id)sender {
    CNAuthorizationStatus status = [CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts];
    switch (status) {
        case CNAuthorizationStatusNotDetermined:
            // деалем запрос на авторизацию
        {
            [store requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error) {
                if (error) {
                    NSLog(@"%@", error);
                } else {
                    // запрос разрешен, можно вычитать все контакты
                    [self contactsList];
                }
            }];
        }
            break;
        case CNAuthorizationStatusDenied:
        case CNAuthorizationStatusRestricted:
            NSLog(@"пользователь не дал разрешение на контакты");
            break;
        case CNAuthorizationStatusAuthorized:
            // запрос был дан ранее, можно вычитать все контакты
            [self contactsList];
            break;
    }
}

- (IBAction)sendMail:(id)sender {
    
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *controller = [MFMailComposeViewController new];
        controller.mailComposeDelegate = self;
        [controller setSubject:@"Test subjecr"];
        [controller setToRecipients:@[@"mycompany@gmail.com"]];
        [self presentViewController:controller animated:YES completion:nil];
    } else {
        NSLog(@"почта не настроена");
    }
}

- (IBAction)sendMessage:(id)sender {
    if ([MFMessageComposeViewController canSendText]) {
        
        MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
        controller.messageComposeDelegate = self;
        controller.subject = @"Test subject";
        controller.body = @"Test message";
        controller.recipients = @[@"0506103233"];
        
        [self presentViewController:controller animated:YES completion:nil];
    }
    
}

#pragma mark - Notification

- (void)contactStoreDidChange:(NSNotification *)notification {
    NSLog(@"notification %@", notification);
}

#pragma mark -

- (void)contactsList {
    //
    NSPredicate *predicate = [CNContact predicateForContactsInContainerWithIdentifier:store.defaultContainerIdentifier];

    NSArray *keys = @[CNContactFamilyNameKey,
                      CNContactGivenNameKey,
                      CNContactEmailAddressesKey,
                      CNContactPhoneNumbersKey];
    NSError *error = nil;
    NSArray *contacts = [store unifiedContactsMatchingPredicate:predicate keysToFetch:keys error:&error];
    if (error) {
        NSLog(@"error %@",error);
    } else {
        NSLog(@"contacts %@",contacts);
    }
}

#pragma mark - CNContactPickerDelegate

// нужно реализовть один из методов:
// - выбор контакта
// - выбор поля у контакта
// - выбор нескольких контактов

//- (void)contactPicker:(CNContactPickerViewController *)picker didSelectContact:(CNContact *)contact {
//    NSLog(@"contact %@", contact);
//}

- (void)contactPicker:(CNContactPickerViewController *)picker didSelectContactProperty:(CNContactProperty *)contactProperty{
    NSLog(@"contactProperty %@", contactProperty);
}

//- (void)contactPicker:(CNContactPickerViewController *)picker didSelectContacts:(NSArray<CNContact*> *)contacts {
//    NSLog(@"contacts %@", contacts);
//}

#pragma mark - CNContactViewControllerDelegate

- (void)contactViewController:(CNContactViewController *)viewController didCompleteWithContact:(nullable CNContact *)contact {
    [viewController dismissViewControllerAnimated:YES completion:nil];
    NSLog(@"create new contact %@", contact);
}

#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    
    [controller dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - MFMessageComposeViewControllerDelegate

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    [controller dismissViewControllerAnimated:YES completion:nil];
}

@end
