//
//  ViewController.m
//  lesson 18_1_UILabel
//
//  Created by Yuriy Bosov on 9/29/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (nonatomic, weak) IBOutlet UILabel *label;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // задаем текст
    _label.text = @"Copyright © 2016 iOS. All rights reserved. Copyright © 2016 iOS. All rights reserved. Copyright © 2016 iOS. All rights reserved. Copyright © 2016 iOS. All rights reserved.";
    
    // цвет текста
    _label.textColor = [UIColor greenColor];
    
    // цвет шрифт
    _label.font = [UIFont boldSystemFontOfSize:20];
    
    // максимальное кол-во линий
    _label.numberOfLines = 0; // если задали 0, то в UILabel будет отображен весь текст, если это не ограничивается констрентами
    
    // выравнивание текста
    _label.textAlignment = NSTextAlignmentJustified;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

@end
