//
//  MyNotificationDelegate.h
//  lesson_63_LocalNotification
//
//  Created by Yurii Bosov on 6/15/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>

@import UserNotifications;

@interface MyNotificationDelegate : NSObject <UNUserNotificationCenterDelegate>

@end
