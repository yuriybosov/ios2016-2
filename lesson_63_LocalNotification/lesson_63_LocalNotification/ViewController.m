//
//  ViewController.m
//  lesson_63_LocalNotification
//
//  Created by Yurii Bosov on 6/15/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"
@import UserNotifications;


@implementation ViewController

#pragma mark - Actions 

- (IBAction)buttonClicked:(id)sender {
    [[UNUserNotificationCenter currentNotificationCenter] getNotificationSettingsWithCompletionHandler:^(UNNotificationSettings * _Nonnull settings) {
        if (settings.authorizationStatus == UNAuthorizationStatusAuthorized ) {
            
            UNMutableNotificationContent *content = [[UNMutableNotificationContent alloc] init];
            
            content.title = @"Title";
            content.subtitle = @"subtitle";
            content.body = @"Body";
            content.badge = @10;
            content.sound = [UNNotificationSound defaultSound];
            
            UNTimeIntervalNotificationTrigger *trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:10 repeats:NO];
            
            UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:@"notification_id" content:content trigger:trigger];
            
            [[UNUserNotificationCenter currentNotificationCenter] addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
                if (error) {
                    NSLog(@"Error %@", error);
                }
            }];
        }
    }];
}

- (IBAction)buttonClicked2:(id)sender {
    [[UNUserNotificationCenter currentNotificationCenter] getNotificationSettingsWithCompletionHandler:^(UNNotificationSettings * _Nonnull settings) {
        if (settings.authorizationStatus == UNAuthorizationStatusAuthorized ) {
            
            UNMutableNotificationContent *content = [[UNMutableNotificationContent alloc] init];
            
            content.title = @"Title 2";
            content.subtitle = @"Subtitle with action";
            content.body = @"Body";
            content.badge = @1;
            content.sound = [UNNotificationSound defaultSound];
            
            UNTimeIntervalNotificationTrigger *trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:10 repeats:NO];
            
            // add custom action
            UNNotificationAction *snoozeAction = [UNNotificationAction actionWithIdentifier:@"snooze" title:@"Snooze" options:UNNotificationActionOptionNone];
            UNNotificationAction *deleteAction = [UNNotificationAction actionWithIdentifier:@"delete" title:@"Delete" options:UNNotificationActionOptionDestructive];
            
            //
            UNNotificationCategory *category = [UNNotificationCategory categoryWithIdentifier:@"UYLReminderCategory" actions:@[snoozeAction,deleteAction]intentIdentifiers:@[] options:UNNotificationCategoryOptionNone];
            
            NSSet *categories = [NSSet setWithObject:category];
            
            [[UNUserNotificationCenter currentNotificationCenter] setNotificationCategories:categories];
            
            content.categoryIdentifier = @"UYLReminderCategory";

            
            UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:@"notification_id_2" content:content trigger:trigger];
            
            [[UNUserNotificationCenter currentNotificationCenter] addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
                if (error) {
                    NSLog(@"Error %@", error);
                }
            }];
        }
    }];
}

@end
