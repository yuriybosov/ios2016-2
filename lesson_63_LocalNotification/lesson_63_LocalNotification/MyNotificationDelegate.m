//
//  MyNotificationDelegate.m
//  lesson_63_LocalNotification
//
//  Created by Yurii Bosov on 6/15/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "MyNotificationDelegate.h"

@import UIKit;

@implementation MyNotificationDelegate

#pragma mark - UNUserNotificationCenterDelegate

- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {

    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    completionHandler(UNNotificationPresentationOptionNone);
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler {
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    if ([response.actionIdentifier isEqualToString:@"snooze"]) {
        
        [center addNotificationRequest:response.notification.request withCompletionHandler:^(NSError * _Nullable error) {
            NSLog(@"error %@", error);
        }];
        
    } else if ([response.actionIdentifier isEqualToString:@"delete"]) {
        
    }
    
    completionHandler();
}

@end
