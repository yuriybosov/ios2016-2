//
//  AppDelegate.m
//  lesson_63_LocalNotification
//
//  Created by Yurii Bosov on 6/15/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "AppDelegate.h"
#import "MyNotificationDelegate.h"
@import UserNotifications;


@interface AppDelegate () {
    MyNotificationDelegate *delegate;
}

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    // выполняем запрос авторизации на использование локальных уведомлений
    UNAuthorizationOptions options = UNAuthorizationOptionBadge | UNAuthorizationOptionAlert | UNAuthorizationOptionSound;
    
    [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:options completionHandler:^(BOOL granted, NSError * _Nullable error) {
        NSLog(@"error %@", error);
        NSLog(@"granted %i", granted);
    }];
    
    delegate = [MyNotificationDelegate new];
    [UNUserNotificationCenter currentNotificationCenter].delegate = delegate;
    
    return YES;
}

@end
