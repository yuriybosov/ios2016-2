//
//  TabBarController.m
//  lesson_47_Big_Progect
//
//  Created by Yurii Bosov on 1/29/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "TabBarController.h"

@interface TabBarController ()

@end

@implementation TabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tabBar.tintColor = [UIColor whiteColor];
    self.tabBar.barTintColor = [UIColor colorWithRedI:33 greenI:35 blueI:37 alpha:1];
    self.tabBar.itemPositioning =  UITabBarItemPositioningCentered;
    self.tabBar.itemWidth = 68;
    
    self.navigationController.navigationBarHidden = YES;

#warning TODO add selectionIndicatorImage
//    self.tabBar.selectionIndicatorImage = [UIImage imageNamed:@"selectedTabbarTrainer"];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

@end
