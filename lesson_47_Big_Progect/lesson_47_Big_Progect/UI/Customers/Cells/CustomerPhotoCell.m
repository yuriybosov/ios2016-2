//
//  CustomerPhotoCell.m
//  lesson_47_Big_Progect
//
//  Created by Yurii Bosov on 2/10/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "CustomerPhotoCell.h"

@implementation CustomerPhotoCell

-(void)awakeFromNib {
    [super awakeFromNib];
    
    self.photoView.layer.cornerRadius = 8;
    self.photoView.layer.borderWidth = 2;
    self.photoView.layer.borderColor = [[UIColor whiteColor] CGColor];
}

@end
