//
//  CustomerCell.m
//  lesson_47_Big_Progect
//
//  Created by Yurii Bosov on 2/9/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "CustomerCell.h"

@implementation CustomerCell

- (void)setCustomer:(CustomerModel *)customer {
    _customer = customer;
    
    self.photoView.image = [UIImage imageNamed:_customer.photo];
    self.nameLabel.text = _customer.name;
    self.descriptionLabel.text = [_customer customerInfo];
}

@end
