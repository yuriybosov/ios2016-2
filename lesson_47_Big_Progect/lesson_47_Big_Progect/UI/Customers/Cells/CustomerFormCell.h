//
//  CustomerFormCell.h
//  lesson_47_Big_Progect
//
//  Created by Yurii Bosov on 2/10/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomerFormCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *valueLabel;

@end
