//
//  CustomerFormController.h
//  lesson_47_Big_Progect
//
//  Created by Yurii Bosov on 2/10/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "BaseTableController.h"
#import "CustomerModel.h"

@interface CustomerFormController : BaseTableController

@property (nonatomic, strong) CustomerModel *customer;

@end
