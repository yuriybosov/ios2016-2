//
//  CustomerDetailedController.m
//  lesson_47_Big_Progect
//
//  Created by Yurii Bosov on 2/9/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "CustomerDetailedController.h"

@interface CustomerDetailedController () {
    IBOutlet UIImageView *photoView;
    IBOutlet UILabel *textLabel;
}

@end

@implementation CustomerDetailedController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    photoView.layer.cornerRadius = photoView.frame.size.width/2;
    photoView.layer.borderWidth = 2;
    photoView.layer.borderColor = [[UIColor colorWithRedI:85 greenI:202 blueI:115 alpha:1] CGColor];
    
    photoView.image = [UIImage imageNamed:self.customer.photo];
    
    textLabel.text = [self.customer customerInfo];
    
    self.titleLabel.text = self.customer.name;
    self.bgImageView.image = [UIImage imageNamed:self.customer.photo];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.destinationViewController respondsToSelector:@selector(setCustomer:)]) {
        [segue.destinationViewController performSelector:@selector(setCustomer:) withObject:self.customer];
    }
}

@end
