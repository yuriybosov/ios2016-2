//
//  CustomerDetailedController.h
//  lesson_47_Big_Progect
//
//  Created by Yurii Bosov on 2/9/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "BaseViewController.h"
#import "CustomerModel.h"

@interface CustomerDetailedController : BaseViewController

@property (nonatomic, strong) CustomerModel *customer;

@end
