//
//  CustomerFormController.m
//  lesson_47_Big_Progect
//
//  Created by Yurii Bosov on 2/10/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "CustomerFormController.h"
#import "CustomerInfoCell.h"
#import "CustomerFormCell.h"
#import "CustomerFormSectionHeader.h"
#import "CustomerPhotoCell.h"
#import <MWPhotoBrowser/MWPhotoBrowser.h>
#import "NavigationController.h"

@interface CustomerFormController () <UICollectionViewDataSource, UICollectionViewDelegate, MWPhotoBrowserDelegate>

@end

@implementation CustomerFormController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.titleLabel.text = @"Анкета";
    self.bgImageView.image = [UIImage imageNamed:self.customer.photo];
    
    if (self.customer.userPhotos.count == 0) {
        self.tableView.tableFooterView = nil;
    }
}

#pragma mark - TableView Data Sources

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.customer.info.count ? 2 : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger number = 0;
    
    if  (section == 0) {
        number = 5;
    } else if (section == 1) {
        number = self.customer.info.count;
    }
    
    return number;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = nil;
    
    if (indexPath.section == 0) {
        CustomerInfoCell *infoCell = [tableView dequeueReusableCellWithIdentifier:@"CustomerInfoCell"];
        if (indexPath.row == 0 ) {          // name
            infoCell.logoView.image = [UIImage imageNamed:@"setEditUser"];
            infoCell.infoLabel.text = self.customer.name;
        } else if (indexPath.row == 1) {    // gender
            infoCell.logoView.image = [UIImage imageNamed:@"gender"];
            infoCell.infoLabel.text = [self.customer genderStringValue];
        } else if (indexPath.row == 2) {    // age
            infoCell.logoView.image = [UIImage imageNamed:@"calendar"];
            infoCell.infoLabel.text = [NSString stringWithFormat:@"%@ лет", self.customer.age];
        } else if (indexPath.row == 3) {    // height
            infoCell.logoView.image = [UIImage imageNamed:@"standingMan"];
            infoCell.infoLabel.text = [NSString stringWithFormat:@"%@ см", self.customer.height];
        } else if (indexPath.row == 4) {    // weight
            infoCell.logoView.image = [UIImage imageNamed:@"streetView"];
            infoCell.infoLabel.text = [NSString stringWithFormat:@"%@ кг", self.customer.weight];
        }
        
        cell = infoCell;
    } else if (indexPath.section == 1) {
        
        CustomerFormCell *formCell = [tableView dequeueReusableCellWithIdentifier:@"CustomerFormCell"];
        
        NSString *key = self.customer.info.allKeys[indexPath.row];
        NSString *value = self.customer.info[key];
        
        formCell.titleLabel.text = key;
        formCell.valueLabel.text = value;
        
        cell = formCell;
    }
    
    return cell;
}

#pragma mark - TableView Delegate

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *result = nil;
    
    if (section == 0) {
        result = [CustomerFormSectionHeader createWithTitle:@"ПАРАМЕТРЫ"];
    } else if(section == 1 && self.customer.info.count > 0) {
        result = [CustomerFormSectionHeader createWithTitle:@"ИНФОРМАЦИЯ"];
    }
    
    return result;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat result = 44;
    
    if (indexPath.section == 1) {
        result = UITableViewAutomaticDimension;
    }
    
    return result;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat result = 44;
    
    if (indexPath.section == 1) {
        result = UITableViewAutomaticDimension;
    }
    
    return result;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 34;
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.customer.userPhotos.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CustomerPhotoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CustomerPhotoCell" forIndexPath:indexPath];
    
    cell.photoView.image = [UIImage imageNamed:self.customer.userPhotos[indexPath.item]];
    
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    MWPhotoBrowser *photoBrowser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    [photoBrowser setCurrentPhotoIndex: indexPath.item];
    
    NavigationController *nc = [[NavigationController alloc] initWithRootViewController:photoBrowser];
    [self presentViewController:nc animated:YES completion:nil];
}

#pragma mark - MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser{
    return self.customer.userPhotos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    MWPhoto *photo = [[MWPhoto alloc] initWithImage:[UIImage imageNamed:self.customer.userPhotos[index]]];
    return photo;
}

@end
