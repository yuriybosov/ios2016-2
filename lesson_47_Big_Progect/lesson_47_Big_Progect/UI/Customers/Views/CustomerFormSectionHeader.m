//
//  CustomerFormSectionHeader.m
//  lesson_47_Big_Progect
//
//  Created by Yurii Bosov on 2/10/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "CustomerFormSectionHeader.h"

@implementation CustomerFormSectionHeader

+ (instancetype)createWithTitle:(NSString *)title {
    CustomerFormSectionHeader *view = [[NSBundle mainBundle] loadNibNamed:@"CustomerFormSectionHeader" owner:nil options:nil].firstObject;
    view.titleLabel.text = title;
    return view;
}

@end
