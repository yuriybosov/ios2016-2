//
//  CustomerFormSectionHeader.h
//  lesson_47_Big_Progect
//
//  Created by Yurii Bosov on 2/10/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomerFormSectionHeader : UIView

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;

+ (instancetype)createWithTitle:(NSString *)title;

@end
