//
//  CustomersListController.m
//  lesson_47_Big_Progect
//
//  Created by Yurii Bosov on 1/29/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "CustomersListController.h"
#import "CustomerCell.h"
#import "CustomerDetailedController.h"

@interface CustomersListController () {
    NSArray *dataSources;
}

@end


@implementation CustomersListController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.titleLabel.text = @"КЛИЕНТЫ";
    
    dataSources = [CustomerModel testData];
    [self.tableView reloadData];
}

#pragma mark - Table Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataSources.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CustomerCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CustomerCell" forIndexPath:indexPath];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.customer = dataSources[indexPath.row];
    
    return cell;
}

#pragma mark - TableView Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 225;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CustomerDetailedController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"CustomerDetailedController"];
    controller.customer = dataSources[indexPath.row];
    
    [self.tabBarController.navigationController pushViewController:controller animated:YES];
}

@end
