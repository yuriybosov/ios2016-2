//
//  TrainingDetailedController.h
//  lesson_47_Big_Progect
//
//  Created by Yurii Bosov on 2/12/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "BaseTableController.h"
#import "TrainingModel.h"
#import "CustomerModel.h"

@interface TrainingDetailedController : BaseTableController

@property (nonatomic, strong) TrainingModel *training;
@property (nonatomic, strong) CustomerModel *customer;

@end
