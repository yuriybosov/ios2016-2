//
//  ExerciseEditController.h
//  lesson_47_Big_Progect
//
//  Created by Yurii Bosov on 2/19/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "BaseViewController.h"
#import "ExerciseModel.h"

typedef enum : NSUInteger {
    ExerciseEditStateNew,
    ExerciseEditStateEdit
} ExerciseEditState;

@interface ExerciseEditController : BaseViewController

@property (nonatomic, strong) ExerciseModel *exercise;
@property (nonatomic, assign) ExerciseEditState state;

@end
