//
//  AddTrainingController.m
//  lesson_47_Big_Progect
//
//  Created by Yurii Bosov on 2/16/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "AddTrainingController.h"
#import "ExerciseCell.h"
#import "ExerciseEditController.h"

@interface AddTrainingController () <UITextFieldDelegate, UITextViewDelegate, MGSwipeTableCellDelegate> {
    
    IBOutlet UITextField *titleTextField;
    IBOutlet UITextView *messageTextView;
    
    NSMutableArray *dataSources;
}

@end

@implementation AddTrainingController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.bgImageView.image = [UIImage imageNamed:self.customer.photo];
    self.titleLabel.text = @"ТРЕНИРОВКА";
    self.tableViewHeader.backgroundColor = [UIColor clearColor];
    
    titleTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Введите название" attributes:@{NSFontAttributeName:titleTextField.font, NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    dataSources = [NSMutableArray arrayWithArray:[ExerciseModel testData]];
    [self.tableView reloadData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

#pragma mark - Actions

- (IBAction)addExerciseButtonClicked{
}

- (IBAction)saveButtonClicked{
}

#pragma mark - TableView Data Sources

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataSources.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ExerciseCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ExerciseCell"];
    
    cell.exercise = dataSources[indexPath.row];
    cell.delegate = self;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 90;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [messageTextView becomeFirstResponder];
    return NO;
}

#pragma mark - UITextViewDelegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    BOOL result = YES;
    
    if ([text rangeOfCharacterFromSet:[NSCharacterSet newlineCharacterSet]].location != NSNotFound) {
        
        result = NO;
        [textView resignFirstResponder];
    }
    
    return result;
}

#pragma mark - MGSwipeTableCellDelegate

- (BOOL)swipeTableCell:(ExerciseCell *)cell tappedButtonAtIndex:(NSInteger)index direction:(MGSwipeDirection)direction fromExpansion:(BOOL)fromExpansion {
    
    BOOL result = NO;
    
    if (index == 0) { // delete
        
        NSIndexPath *path = [self.tableView indexPathForCell:cell];
        
        [dataSources removeObjectAtIndex:path.row];
        [self.tableView deleteRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationAutomatic];
    } else {        // edit
        result = YES;
        
        // open etid exersize controller
        ExerciseEditController *editController = [self.storyboard instantiateViewControllerWithIdentifier:@"ExerciseEditController"];
        editController.exercise = cell.exercise;
        editController.state = ExerciseEditStateEdit;
        
        [self.navigationController pushViewController:editController animated:YES];
    }
    
    return result;
}

@end
