//
//  TrainingsListController.h
//  lesson_47_Big_Progect
//
//  Created by Yurii Bosov on 2/12/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "BaseTableController.h"
#import "CustomerModel.h"

@interface TrainingsListController : BaseTableController

@property (nonatomic, strong) CustomerModel *customer;

@end
