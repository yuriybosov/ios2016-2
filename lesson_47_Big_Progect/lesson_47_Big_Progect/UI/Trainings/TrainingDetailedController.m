//
//  TrainingDetailedController.m
//  lesson_47_Big_Progect
//
//  Created by Yurii Bosov on 2/12/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "TrainingDetailedController.h"
#import "ExerciseModelCell.h"

@interface TrainingDetailedController () {
    NSMutableArray *dataSources;
}

@end

@implementation TrainingDetailedController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    dataSources = [NSMutableArray new];
    for (ExerciseModel *exercise in self.training.exercises) {
        [dataSources addObject:[ExerciseModelView modelViewWithExercise:exercise]];
    }
    
    self.titleLabel.text = self.training.title;
    self.bgImageView.image = [UIImage imageNamed:self.customer.photo];
}

#pragma mark - UITableView Data Sources

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataSources.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ExerciseModelCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ExerciseModelCell"];
    cell.modelView = dataSources[indexPath.row];
    return cell;
}

#pragma mark - UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ExerciseModelView *modelView = dataSources[indexPath.row];
    if (modelView.state == ExerciseModelViewStateSmall) {
        modelView.state = ExerciseModelViewStateBig;
    } else if (modelView.state == ExerciseModelViewStateBig) {
        modelView.state = ExerciseModelViewStateDynamic;
    } else if (modelView.state == ExerciseModelViewStateDynamic) {
        modelView.state = ExerciseModelViewStateSmall;
    }
    
    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ExerciseModelView *modelView = dataSources[indexPath.row];
    return [modelView cellHeight];
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ExerciseModelView *modelView = dataSources[indexPath.row];
    return [modelView cellHeight];
}

@end
