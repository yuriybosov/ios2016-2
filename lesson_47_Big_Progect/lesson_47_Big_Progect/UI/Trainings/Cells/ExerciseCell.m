//
//  ExerciseCell.m
//  lesson_47_Big_Progect
//
//  Created by Yurii Bosov on 2/12/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ExerciseCell.h"

@implementation ExerciseCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [UIHelper addShadowForView:self.myContentView];
    
    MGSwipeButton *editButton = [MGSwipeButton buttonWithTitle:@"" icon:[UIImage imageNamed:@"editButton"] backgroundColor:[UIColor colorWithRedI:85 greenI:202 blueI:115 alpha:1]];
    editButton.buttonWidth = 65;
    
    MGSwipeButton *deleteButton = [MGSwipeButton buttonWithTitle:@"" icon:[UIImage imageNamed:@"deleteButton"] backgroundColor:[UIColor colorWithRedI:226 greenI:75 blueI:75 alpha:1]];
    deleteButton.buttonWidth = 65;
    
    self.rightButtons = @[deleteButton, editButton];
}

- (void)setExercise:(ExerciseModel *)exercise {
    
    _exercise = exercise;
    
    self.titleLabel.text = _exercise.title;
    self.descriptionLabel.text = _exercise.text;
    self.photoView.image = [UIImage imageNamed:_exercise.photoSmall];
}

@end
