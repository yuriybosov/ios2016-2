//
//  ExerciseModelCell.h
//  lesson_47_Big_Progect
//
//  Created by Yurii Bosov on 2/23/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ExerciseModelView.h"

@interface ExerciseModelCell : UITableViewCell

@property (nonatomic, strong) ExerciseModelView *modelView;

@property (nonatomic, strong) IBOutlet UILabel *titleLabel;

@property (nonatomic, strong) IBOutlet UIImageView *photoView;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *photoViewTop;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *photoViewHeight;

@property (nonatomic, strong) IBOutlet UIView *descriptionView;
@property (nonatomic, strong) IBOutlet UILabel *descriptionLabel;
@property (nonatomic, strong) IBOutlet UIButton *moreButton;

@end
