//
//  ExerciseCell.h
//  lesson_47_Big_Progect
//
//  Created by Yurii Bosov on 2/12/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MGSwipeTableCell.h>
#import "ExerciseModel.h"

@interface ExerciseCell : MGSwipeTableCell

// small state

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *descriptionLabel;
@property (nonatomic, weak) IBOutlet UIImageView *photoView;

@property (nonatomic, weak) IBOutlet UIView *myContentView;

// big state

@property (nonatomic, strong) ExerciseModel *exercise;

@end
