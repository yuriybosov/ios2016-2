//
//  ExerciseModelCell.m
//  lesson_47_Big_Progect
//
//  Created by Yurii Bosov on 2/23/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ExerciseModelCell.h"

@implementation ExerciseModelCell

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {

}

- (void)setModelView:(ExerciseModelView *)modelView {
    _modelView = modelView;
    
    self.titleLabel.text = modelView.exercise.title;
    self.photoView.image = [UIImage imageNamed:modelView.exercise.photoSmall];
    
    switch (_modelView.state) {
        case ExerciseModelViewStateSmall:
            self.photoViewTop.constant = 0;
            self.photoViewHeight.constant = 80;
            
            self.descriptionView.hidden = YES;
            self.descriptionLabel.text = nil;
            
            break;
            
        case ExerciseModelViewStateBig:
        case ExerciseModelViewStateDynamic:
            self.photoViewTop.constant = 70;
            self.photoViewHeight.constant = 150;
            
            self.descriptionView.hidden = NO;
            self.descriptionLabel.text = _modelView.exercise.text;
            
            break;
            
    }
}

@end
