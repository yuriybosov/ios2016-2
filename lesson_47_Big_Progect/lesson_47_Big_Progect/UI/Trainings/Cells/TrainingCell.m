//
//  TrainingCell.m
//  lesson_47_Big_Progect
//
//  Created by Yurii Bosov on 2/12/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "TrainingCell.h"

@implementation TrainingCell

- (void)setTraining:(TrainingModel *)training {
    _training = training;
    
    self.photoView.image = [UIImage imageNamed:_training.photo];
    self.titleLabel.text = _training.title;
    self.descriptionLabel.text = _training.text;
    
    if (_training.complitionDate) {
        self.statusView.image = [UIImage imageNamed:@"exFinished"];
        
        NSString *dateStr = [[DateFormatter ddMMyy] stringFromDate:_training.complitionDate];
        NSString *fullStr = [NSString stringWithFormat:@"Выполнил: %@", dateStr];
        NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:fullStr attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor], NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Bold" size:17]}];
        [attrStr addAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Light" size:17]} range:[fullStr rangeOfString:dateStr]];
        
        self.statusLabel.attributedText = attrStr;
        
        if (_training.duration) {
            
            NSString *durationStr = [NSString stringWithFormat:@"%@ мин",_training.duration];
            fullStr = [NSString stringWithFormat:@"Время: %@", durationStr];
            
            attrStr = [[NSMutableAttributedString alloc] initWithString:fullStr attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor], NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Bold" size:17]}];
            [attrStr addAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Light" size:17]} range:[fullStr rangeOfString:durationStr]];
            
            self.durationLabel.attributedText = attrStr;
        } else {
            self.durationLabel.text = nil;
        }
        
    } else {
        self.statusView.image= [UIImage imageNamed:@"exAttention"];
        self.statusLabel.text = @"Новая";
        self.durationLabel.text = nil;
    }
}

@end
