//
//  TrainingsListController.m
//  lesson_47_Big_Progect
//
//  Created by Yurii Bosov on 2/12/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "TrainingsListController.h"
#import "TrainingCell.h"
#import "TrainingDetailedController.h"

@interface TrainingsListController () {
    NSArray *dataSources;
}

@end

@implementation TrainingsListController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.titleLabel.text = @"ТРЕНИРОВКИ";
    self.bgImageView.image = [UIImage imageNamed:self.customer.photo];
    
    dataSources = [TrainingModel testData];
    [self.tableView reloadData];
}

#pragma mark - TableView Data Sources

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataSources.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TrainingCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TrainingCell"];
    cell.training = dataSources[indexPath.row];
    return cell;
}

#pragma mark - TableView Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 220;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.destinationViewController respondsToSelector:@selector(setCustomer:)]) {
        [segue.destinationViewController performSelector:@selector(setCustomer:) withObject:self.customer];
    }
    
    if ([sender isKindOfClass:[TrainingCell class]] &&
        [segue.destinationViewController isKindOfClass:[TrainingDetailedController class]]) {
        
        TrainingDetailedController *controller = segue.destinationViewController;
        TrainingCell *cell = sender;
        controller.training = cell.training;
    }
}

@end
