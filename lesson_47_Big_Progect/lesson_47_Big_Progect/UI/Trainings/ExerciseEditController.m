//
//  ExerciseEditController.m
//  lesson_47_Big_Progect
//
//  Created by Yurii Bosov on 2/19/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ExerciseEditController.h"

@interface ExerciseEditController () {
    IBOutlet UIButton *saveButton;
    IBOutlet UITextView *commentTextView;
}

@end

@implementation ExerciseEditController

- (void)viewDidLoad {
    [super viewDidLoad];
    commentTextView.text = self.exercise.text;
    self.titleLabel.text = @"ВВЕДИТЕ ПАРАМЕРТЫ";
    
    if (self.state == ExerciseEditStateEdit) {
        [saveButton setTitle:@"СОХРАНИТЬ" forState:UIControlStateNormal];
    } else {
        [saveButton setTitle:@"ДОБАВИТЬ" forState:UIControlStateNormal];
    }
}

#pragma mark - Button Actions

- (IBAction)saveButtonClicked:(id)sender{
    if (self.state == ExerciseEditStateEdit) {
        // сохраняем текст
        self.exercise.text = commentTextView.text;
        
        // прячем экран
        [self.navigationController popViewControllerAnimated:YES];
        
    } else {
        //
    }
}

@end
