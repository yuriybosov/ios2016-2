//
//  ProfileController.m
//  lesson_47_Big_Progect
//
//  Created by Yurii Bosov on 1/29/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ProfileController.h"

@interface ProfileController () {
    IBOutlet UIVisualEffectView *effectView;
}

@end

@implementation ProfileController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    effectView.alpha = 0.2;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

@end
