//
//  BaseViewController.h
//  lesson_47_Big_Progect
//
//  Created by Yurii Bosov on 1/29/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

@property (nonatomic, strong) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) IBOutlet UIImageView *bgImageView;

@end
