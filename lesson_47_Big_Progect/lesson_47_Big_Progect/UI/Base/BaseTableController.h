//
//  BaseTableController.h
//  lesson_47_Big_Progect
//
//  Created by Yurii Bosov on 2/5/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseTableController : BaseViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) IBOutlet UIView *tableViewHeader;
@property (nonatomic, strong) IBOutlet UIView *tableViewFooter;

@end
