//
//  BaseViewController.m
//  lesson_47_Big_Progect
//
//  Created by Yurii Bosov on 1/29/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //
    self.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:36];
    self.titleLabel.textColor = [UIColor whiteColor];
    self.titleLabel.numberOfLines = 0;
    
    //
    self.bgImageView.image = [UIImage imageNamed:@"authBkg"];
    self.bgImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.bgImageView.alpha = 0.5;
    self.bgImageView.clipsToBounds = YES;
    
    //
    self.view.backgroundColor = [UIColor colorWithRedI:22 greenI:23 blueI:24 alpha:1];
    
    // если текущий котроллер не является рутовым то создадим nav. back button
    if (self.navigationController.viewControllers.count &&
        self.navigationController.viewControllers[0] != self) {
        
        UIBarButtonItem *navBackButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"navMenuBack"] style:UIBarButtonItemStyleDone target:self action:@selector(navBackButtonClicked:)];
        self.navigationItem.leftBarButtonItem = navBackButton;
    }
    
    // если текущий котроллер является рутовым то прячем navBar (см. дизайн)
    if (self.navigationController.viewControllers.count &&
        self.navigationController.viewControllers[0] == self) {
        self.navigationController.navigationBarHidden = YES;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // если текущий котроллер не является рутовым то отобразим navbar
    if (self.navigationController.viewControllers.count &&
        self.navigationController.viewControllers[0] != self) {
        
        [self.navigationController setNavigationBarHidden:NO
                                                 animated:YES];
    } else{ // иначе спрячем navbar
        [self.navigationController setNavigationBarHidden:YES
                                                 animated:YES];
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

#pragma mark - Actions

- (IBAction)navBackButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
