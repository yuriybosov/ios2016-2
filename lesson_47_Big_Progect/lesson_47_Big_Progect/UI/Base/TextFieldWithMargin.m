//
//  TextFieldWithMargin.m
//  lesson_47_Big_Progect
//
//  Created by Yurii Bosov on 2/16/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "TextFieldWithMargin.h"

static CGFloat margin = 23;

@implementation TextFieldWithMargin

- (CGRect)textRectForBounds:(CGRect)bounds
{
    
    CGRect inset = CGRectMake(bounds.origin.x + margin, bounds.origin.y, bounds.size.width - margin, bounds.size.height);
    return inset;
}

- (CGRect)editingRectForBounds:(CGRect)bounds
{
    
    CGRect inset = CGRectMake(bounds.origin.x + margin, bounds.origin.y, bounds.size.width - margin, bounds.size.height);
    return inset;
}

@end
