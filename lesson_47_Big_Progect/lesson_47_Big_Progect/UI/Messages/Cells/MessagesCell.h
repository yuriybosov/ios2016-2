//
//  MessagesCell.h
//  lesson_47_Big_Progect
//
//  Created by Yurii Bosov on 2/5/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessagesCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *userAvatar;
@property (nonatomic, weak) IBOutlet UILabel *userNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *messageLabel;
@property (nonatomic, weak) IBOutlet UIView *bottomSeparator;

@end
