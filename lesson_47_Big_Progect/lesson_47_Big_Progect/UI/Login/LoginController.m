//
//  LoginController.m
//  lesson_47_Big_Progect
//
//  Created by Yurii Bosov on 1/29/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "LoginController.h"

@interface LoginController () <UITextFieldDelegate> {
    IBOutlet UITextField *emailTextField;
    IBOutlet UITextField *passwordTextField;
}

@end

@implementation LoginController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Placeholder
    emailTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"E-mail" attributes:@{NSFontAttributeName:emailTextField.font, NSForegroundColorAttributeName:[UIColor whiteColor]}];
    passwordTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSFontAttributeName:passwordTextField.font, NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
//    // отступы
//    emailTextField.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 23, 30)];
//    emailTextField.leftView.backgroundColor = [UIColor clearColor];
//    emailTextField.leftViewMode = UITextFieldViewModeAlways;
//    
//    emailTextField.rightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 23, 30)];
//    emailTextField.rightView.backgroundColor = [UIColor clearColor];
//    emailTextField.rightViewMode = UITextFieldViewModeAlways;
//    
//    passwordTextField.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 23, 30)];
//    passwordTextField.leftView.backgroundColor = [UIColor clearColor];
//    passwordTextField.leftViewMode = UITextFieldViewModeAlways;
//    
//    passwordTextField.rightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 23, 30)];
//    passwordTextField.rightView.backgroundColor = [UIColor clearColor];
//    passwordTextField.rightViewMode = UITextFieldViewModeAlways;
    
}

- (IBAction)loginButtonClicked:(id)sender{
    // 1. валидируем поля ввода
    // 2. делаем запрос на сервер, получаем и обрабатываем ответ с сервера
    // 3. и только после этого выполним переход на др экран
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Tabbar" bundle:nil];
    UIViewController *controller = [storyboard instantiateInitialViewController];
    [UIApplication sharedApplication].keyWindow.rootViewController = controller;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == emailTextField) {
        [passwordTextField becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    return YES;
}

@end
