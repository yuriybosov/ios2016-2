//
//  UIHelper.h
//  lesson_47_Big_Progect
//
//  Created by Yurii Bosov on 2/19/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIHelper : NSObject

+ (void)addShadowForView:(UIView *)view;

@end
