//
//  DateFormatter.m
//  lesson_47_Big_Progect
//
//  Created by Yurii Bosov on 2/12/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "DateFormatter.h"

@implementation DateFormatter

+ (NSDateFormatter *)ddMMyy {
    static dispatch_once_t onceToken;
    static NSDateFormatter *ddMMyyFormatter;
    
    dispatch_once(&onceToken, ^{
        ddMMyyFormatter = [NSDateFormatter new];
        ddMMyyFormatter.dateFormat = @"dd.MM.yy";
    });
    
    return ddMMyyFormatter;
}

@end
