//
//  UIHelper.m
//  lesson_47_Big_Progect
//
//  Created by Yurii Bosov on 2/19/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "UIHelper.h"

@implementation UIHelper

+ (void)addShadowForView:(UIView *)view {
    view.clipsToBounds = NO;
    view.layer.shadowColor = [[UIColor blackColor] colorWithAlphaComponent:0.2].CGColor;
    view.layer.shadowOffset = CGSizeMake(0, 2);
    view.layer.shadowOpacity = 0.7;
    view.layer.shadowRadius = 2;}

@end
