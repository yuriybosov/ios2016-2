//
//  UIColor+Extension.h
//  lesson_47_Big_Progect
//
//  Created by Yurii Bosov on 1/29/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Extension)

+ (UIColor*)colorWithRedI:(NSUInteger)red    // 0~255
                   greenI:(NSUInteger)green  // 0~255
                    blueI:(NSUInteger)blue   // 0~255
                   alpha:(CGFloat)alpha;    // 0~1

@end
