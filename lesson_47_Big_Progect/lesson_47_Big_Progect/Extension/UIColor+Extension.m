//
//  UIColor+Extension.m
//  lesson_47_Big_Progect
//
//  Created by Yurii Bosov on 1/29/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "UIColor+Extension.h"

@implementation UIColor (Extension)

+ (UIColor*)colorWithRedI:(NSUInteger)red    // 0~255
                   greenI:(NSUInteger)green  // 0~255
                    blueI:(NSUInteger)blue   // 0~255
                   alpha:(CGFloat)alpha{    // 0~1
    
    return [UIColor colorWithRed:red/255.f
                           green:green/255.f
                            blue:blue/255.f
                           alpha:alpha];
}
@end
