//
//  TrainingModel.m
//  lesson_47_Big_Progect
//
//  Created by Yurii Bosov on 2/12/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "TrainingModel.h"

@implementation TrainingModel

+ (NSArray *)testData {
    NSMutableArray *data = [NSMutableArray array];
    
    TrainingModel *model = [TrainingModel new];
    model.photo = @"ivan";
    model.title = @"Силовая тренировка";
    model.text = @"Силовая тренировка на все группы мышц";
    model.exercises = [ExerciseModel testData];
    [data addObject:model];
    
    model = [TrainingModel new];
    model.photo = @"ivan";
    model.title = @"Кардио тренировка";
    model.text = @"Кардио тренировка с приминением ног";
    model.complitionDate = [NSDate dateWithTimeIntervalSince1970:1484006400];
    model.duration = @90;
    model.exercises = [ExerciseModel testData];
    [data addObject:model];
    
    return data;
}

@end
