//
//  TrainingModelView.h
//  lesson_47_Big_Progect
//
//  Created by Yurii Bosov on 2/23/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ExerciseModel.h"

typedef enum : NSUInteger {
    ExerciseModelViewStateSmall,
    ExerciseModelViewStateBig,
    ExerciseModelViewStateDynamic,
} ExerciseModelViewState;

@interface ExerciseModelView : NSObject

@property (nonatomic, strong) ExerciseModel *exercise;
@property (nonatomic, assign) ExerciseModelViewState state;

+ (instancetype)modelViewWithExercise:(ExerciseModel *)exercise;
- (CGFloat)cellHeight;

@end

