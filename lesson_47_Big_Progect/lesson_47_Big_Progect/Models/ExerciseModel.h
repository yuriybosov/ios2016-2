//
//  ExerciseModel.h
//  lesson_47_Big_Progect
//
//  Created by Yurii Bosov on 2/12/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ExerciseModel : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) NSString *photoSmall;
@property (nonatomic, strong) NSArray *photos;
@property (nonatomic, strong) NSString *youtubeVideoId;

+ (NSArray *)testData;

@end
