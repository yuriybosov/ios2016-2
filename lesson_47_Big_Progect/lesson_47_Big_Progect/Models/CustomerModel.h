//
//  CustomerModel.h
//  lesson_47_Big_Progect
//
//  Created by Yurii Bosov on 2/9/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    GenderNone,
    GenderMale,
    GenderFemale
} Gender;

@interface CustomerModel : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSNumber *age;
@property (nonatomic, strong) NSNumber *height;
@property (nonatomic, strong) NSNumber *weight;
@property (nonatomic, strong) NSString *photo;
@property (nonatomic, strong) NSNumber *countOfTrainings;
@property (nonatomic, assign) Gender gender;

@property (nonatomic, strong) NSDictionary *info;
@property (nonatomic, strong) NSArray *userPhotos;

+ (NSArray *)testData;

- (NSString *)customerInfo;
- (NSString *)genderStringValue;



@end
