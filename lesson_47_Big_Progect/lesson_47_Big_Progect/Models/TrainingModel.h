//
//  TrainingModel.h
//  lesson_47_Big_Progect
//
//  Created by Yurii Bosov on 2/12/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ExerciseModel.h"

@interface TrainingModel : NSObject

@property (nonatomic, strong) NSString *photo;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *text;

@property (nonatomic, strong) NSDate *complitionDate;
@property (nonatomic, strong) NSNumber *duration; // min

@property (nonatomic, strong) NSArray<ExerciseModel *> *exercises;

+ (NSArray *)testData;

@end
