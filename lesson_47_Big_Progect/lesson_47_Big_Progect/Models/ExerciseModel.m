//
//  ExerciseModel.m
//  lesson_47_Big_Progect
//
//  Created by Yurii Bosov on 2/12/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ExerciseModel.h"

@implementation ExerciseModel

+ (NSArray *)testData {
    NSMutableArray *array = [NSMutableArray array];
    
    ExerciseModel *model = [ExerciseModel new];
    model.title = @"Title Title TitleTitle Title TitleTitle Title Title";
    model.text = @"Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text ";
    model.photoSmall = @"ivan";
    [array addObject:model];
    
    model = [ExerciseModel new];
    model.title = @"Title";
    model.text = @"Text Text Text";
    model.photoSmall = @"ivan";
    [array addObject:model];
    
    model = [ExerciseModel new];
    model.title = @"Title qwerqwer";
    model.text = @"Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text ";
    model.photoSmall = @"ivan";
    [array addObject:model];
    
    return array;
}

@end
