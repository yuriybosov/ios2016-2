//
//  CustomerModel.m
//  lesson_47_Big_Progect
//
//  Created by Yurii Bosov on 2/9/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "CustomerModel.h"

@implementation CustomerModel

+ (NSArray *)testData{
    NSMutableArray *data = [NSMutableArray new];
    
    CustomerModel *model = [CustomerModel new];
    model.name = @"Сергей";
    model.age = @(25);
    model.height = @(178);
    model.weight = @(85);
    model.gender = GenderMale;
    model.photo = @"userPhoto";
    model.countOfTrainings = @(9);
    model.info = @{@"Болезни / травмы:":@"test test test test test test test test test test test",
                   @"Проблемные места:":@"test test ",
                   @"Уровень подготовленности:":@"",
                   @"Образ жизни:":@"test test ",
                   @"Заниматьс:":@"в Зале",
                   @"Заниматься:":@"пн. ср. сб",
                   @"Получить от тренировки:":@"похудеть"};
    model.userPhotos = @[@"ivan2",@"ivan2",@"ivan2",@"ivan2",@"ivan2"];
    [data addObject:model];
    
    model = [CustomerModel new];
    model.name = @"Юрий";
    model.age = @(30);
    model.height = @(185);
    model.weight = @(95);
    model.gender = GenderMale;
    model.photo = @"userPhoto";
    model.countOfTrainings = @(3);
    model.userPhotos = @[@"ivan2"];
    [data addObject:model];
    
    model = [CustomerModel new];
    model.name = @"Александр";
    model.age = @(28);
    model.height = @(190);
    model.weight = @(100);
    model.gender = GenderMale;
    model.photo = @"userPhoto";
    model.countOfTrainings = @(2);
    [data addObject:model];
    
    return data;
}

- (NSString *)customerInfo {
    return [NSString stringWithFormat:@"%@ лет. %@ кг\nОсталось %@ тренировок", self.age, self.weight, self.countOfTrainings];
}

- (NSString *)genderStringValue {
    NSString *result = @"Не задан";
    if (self.gender == GenderMale) {
        result = @"Муж.";
    } else if (self.gender == GenderFemale) {
        result = @"Жен.";
    }
    return result;
}

@end
