//
//  TrainingModelView.m
//  lesson_47_Big_Progect
//
//  Created by Yurii Bosov on 2/23/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ExerciseModelView.h"

@implementation ExerciseModelView

+ (instancetype)modelViewWithExercise:(ExerciseModel *)exercise {
    
    ExerciseModelView *modelView = [ExerciseModelView new];
    modelView.exercise = exercise;
    return modelView;
}

- (CGFloat)cellHeight {
    CGFloat result = UITableViewAutomaticDimension;

    if (self.state == ExerciseModelViewStateSmall) {
        result = 90;
    } else if (self.state == ExerciseModelViewStateBig) {
        result = 300;
    }
    
    return result;
}

@end
