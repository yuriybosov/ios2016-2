//
//  Note.swift
//  lessen_73_ShareExtension
//
//  Created by Yurii Bosov on 7/20/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

import UIKit

public class Note: NSObject, NSCoding {
    
    public var title: String
    public var text: String
    public let id: Int
    
    init(_ title: String, _ text: String) {
        self.title = title
        self.text = text
        self.id = Int(Date().timeIntervalSince1970)
        super.init()
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(self.title, forKey: "title")
        aCoder.encode(self.text, forKey: "text")
        aCoder.encode(self.id, forKey: "id")
    }
    
    public required init?(coder aDecoder: NSCoder) {
        self.title = aDecoder.decodeObject(forKey: "title") as? String ?? ""
        self.text = aDecoder.decodeObject(forKey: "text") as? String ?? ""
        self.id = aDecoder.decodeInteger(forKey: "id")
    }
    
    public static func createNoteWith(title: String, text: String){
        let note = Note(title, text)
        var noteArray = Note.noteList()
        noteArray.append(note)
        
        let data = NSKeyedArchiver.archivedData(withRootObject: noteArray)
        let ud = UserDefaults(suiteName: "group.com.note.data")
        ud?.set(data, forKey: "notes")
    }
    
    public static func noteList() -> [Note] {
        var noteArray = [Note]()
        if let ud = UserDefaults(suiteName: "group.com.note.data") {
            if let data = ud.object(forKey: "notes") as? Data {
                if let tempArray = NSKeyedUnarchiver.unarchiveObject(with: data) as? [Note] {
                    noteArray.append(contentsOf: tempArray)
                }
            }
        }
        return noteArray
    }
}
