//
//  ViewController.m
//  lesson_62_GoogleMapClusterPins
//
//  Created by Yurii Bosov on 6/8/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "ATMModel.h"
#import <AFNetworking.h>
#import <SVProgressHUD.h>
#import <GMUMarkerClustering.h>

@interface ViewController () <GMSMapViewDelegate> {
    GMUClusterManager *clusterManager;
}

@property (nonatomic, weak) IBOutlet GMSMapView *maps;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.maps.myLocationEnabled = YES;
    self.maps.settings.myLocationButton = YES;
    
    id<GMUClusterAlgorithm> algorithm = [[GMUNonHierarchicalDistanceBasedAlgorithm alloc] init];
    id<GMUClusterIconGenerator> iconGenerator = [[GMUDefaultClusterIconGenerator alloc] init];
    id<GMUClusterRenderer> renderer =
    [[GMUDefaultClusterRenderer alloc] initWithMapView:self.maps
                                  clusterIconGenerator:iconGenerator];
    clusterManager =
    [[GMUClusterManager alloc] initWithMap:self.maps algorithm:algorithm renderer:renderer];
    
    [self fetchData];
}

#pragma mark - Request \ Response

- (void)fetchData {
    
    [SVProgressHUD show];
    
    NSString *urlString = [NSString stringWithFormat:@"https://api.privatbank.ua/p24api/infrastructure?json&atm"];
    
    [[AFHTTPSessionManager manager] GET:urlString parameters:@{@"city":@"Днепропетровск"} progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self fetchDataWithResponce:responseObject
                              error:nil];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self fetchDataWithResponce:nil
                              error:error];
    }];
}

- (void)fetchDataWithResponce:(id)responseObject
                        error:(NSError *)error {
    if (error) {
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        [SVProgressHUD dismissWithDelay:5];
    } else {
        [SVProgressHUD dismiss];
        
        NSArray *devices=[responseObject objectForKey:@"devices"];
        
        NSError *parserError = nil;
        NSArray *models = [ATMModel arrayOfModelsFromDictionaries:devices error:&parserError];
        
        NSLog(@"%@", parserError);
        NSLog(@"%@", models);
        
        if (models.count) {
            [self showATM:models];
        }
    }
}

- (void)showATM:(NSArray *)ATM {
    
    __block GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
    
    // показали пины
    [ATM enumerateObjectsUsingBlock:^(ATMModel * obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        bounds = [bounds includingCoordinate:obj.position];
        [clusterManager addItem:obj];
    }];
    
    // показали область в пинами
    GMSCameraUpdate *camera = [GMSCameraUpdate fitBounds:bounds];
    [self.maps animateWithCameraUpdate:camera];
}

#pragma mark - GMSMapViewDelegate


@end
