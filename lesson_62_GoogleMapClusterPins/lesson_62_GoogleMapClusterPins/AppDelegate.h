//
//  AppDelegate.h
//  lesson_62_GoogleMapClusterPins
//
//  Created by Yurii Bosov on 6/8/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

