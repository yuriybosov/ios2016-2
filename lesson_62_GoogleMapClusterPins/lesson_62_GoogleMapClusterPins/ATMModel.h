//
//  ATMModel.h
//  lesson_62_GoogleMapClusterPins
//
//  Created by Yurii Bosov on 6/8/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import <GMUClusterItem.h>

@class TimeWork;

@interface ATMModel : JSONModel <GMUClusterItem>

@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *cityRU;
@property (nonatomic, strong) NSString *fullAddressRu;
@property (nonatomic, strong) NSString *placeRu;
@property (nonatomic, strong) NSNumber *latitude;
@property (nonatomic, strong) NSNumber *longitude;
@property (nonatomic, strong) TimeWork *tw;

@end


@interface TimeWork : JSONModel

@property (nonatomic, strong) NSString *mon;
@property (nonatomic, strong) NSString *tue;
@property (nonatomic, strong) NSString *wed;
@property (nonatomic, strong) NSString *thu;
@property (nonatomic, strong) NSString *fri;
@property (nonatomic, strong) NSString *sat;
@property (nonatomic, strong) NSString *sun;
@property (nonatomic, strong) NSString *hol;

@end
