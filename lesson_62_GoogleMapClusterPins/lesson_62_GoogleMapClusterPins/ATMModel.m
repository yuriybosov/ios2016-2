//
//  ATMModel.m
//  lesson_62_GoogleMapClusterPins
//
//  Created by Yurii Bosov on 6/8/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ATMModel.h"

@implementation ATMModel

#pragma mark - GMUClusterItem

- (CLLocationCoordinate2D)position {
    return CLLocationCoordinate2DMake(self.latitude.doubleValue, self.longitude.doubleValue);
}

@end
