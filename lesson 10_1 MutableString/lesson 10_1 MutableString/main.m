//
//  main.m
//  lesson 10_1 MutableString
//
//  Created by Yuriy Bosov on 9/1/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        // NSMutableString
        // 1. создание
        NSMutableString *str1 = [[NSMutableString alloc] init];
        str1 = [[NSMutableString alloc] initWithString:@"TEST"];
        str1 = [[NSMutableString alloc] initWithFormat:@"%@", @"Test"];
        NSLog(@"str1 = %@", str1);
        
        // 2. Объединение строк
        [str1 appendString:@"qqqqq"];
        NSLog(@"str1 = %@", str1);
        
        [str1 appendFormat:@"%@", @"EEEEEE"];
        NSLog(@"str1 = %@", str1);
        
        // 3. вставка строки по индексу
        // нужна проверка на выход за пределы строки
        [str1 insertString:@"yyyyyy" atIndex:8];
        NSLog(@"str1 = %@", str1);
        
        // 4. удаление в диапазоне
        NSRange range = NSMakeRange(0, 4);
        [str1 deleteCharactersInRange:range];
        NSLog(@"str1 = %@", str1);
    }
    return 0;
}
