//
//  News.h
//  lesson 21_UITableView_CustomCell_Plist
//
//  Created by Yuriy Bosov on 10/9/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface News : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) NSString *imageName;

@end
