//
//  DataManager.h
//  lesson 21_UITableView_CustomCell_Plist
//
//  Created by Yuriy Bosov on 10/9/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "News.h"

@interface DataManager : NSObject

@property (nonatomic, readonly) NSArray<News *> *newsList;

+ (DataManager *)sharedInstance;

@end
