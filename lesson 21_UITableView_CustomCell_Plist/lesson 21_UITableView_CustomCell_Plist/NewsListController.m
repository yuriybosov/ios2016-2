//
//  NewsListController.m
//  lesson 21_UITableView_CustomCell_Plist
//
//  Created by Yuriy Bosov on 10/9/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "NewsListController.h"
#import "NewsCell.h"
#import "DataManager.h"

@interface NewsListController ()

@end

@implementation NewsListController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"News";
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [DataManager sharedInstance].newsList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NewsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NewsCell" forIndexPath:indexPath];
    
    News *news = [[DataManager sharedInstance].newsList objectAtIndex:indexPath.row];
    
    cell.lbTitle.text = news.title;
    cell.lbText.text = news.text;
    cell.newsLogo.image = [UIImage imageNamed:news.imageName];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 120;
}

@end
