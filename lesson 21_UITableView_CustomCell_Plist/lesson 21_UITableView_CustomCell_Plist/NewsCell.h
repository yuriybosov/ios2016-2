//
//  NewsCell.h
//  lesson 21_UITableView_CustomCell_Plist
//
//  Created by Yuriy Bosov on 10/9/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *lbTitle;
@property (nonatomic, weak) IBOutlet UILabel *lbText;
@property (nonatomic, weak) IBOutlet UIImageView *newsLogo;

@end
