//
//  DataManager.m
//  lesson 21_UITableView_CustomCell_Plist
//
//  Created by Yuriy Bosov on 10/9/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "DataManager.h"

@implementation DataManager

@synthesize newsList = _newsList;

+ (DataManager *)sharedInstance {
   
    static DataManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[DataManager alloc] init];
    });
    
    return manager;
}

- (NSArray<News *> *)newsList {
    
    if (!_newsList) {
        //TODO CREATE ARRAY OF NEWS
        
        // pathForResource:ofType: - получение пути для файла, где pathForResource - имя файла, ofType - расширение файла
        NSString *path = [[NSBundle mainBundle] pathForResource:@"data" ofType:@"plist"];
        // arrayWithContentsOfFile: - создаем массив на основе .plist файла, где path - пусть к .plist файлу
        NSArray *newsRawData = [NSArray arrayWithContentsOfFile:path];
        
        NSMutableArray *tempArray = [NSMutableArray array];
        for (NSDictionary *newsData in newsRawData) {
            
            News *news = [[News alloc] init];
            news.title = [newsData objectForKey:@"title"];
            news.text = [newsData objectForKey:@"text"];
            news.imageName = [newsData objectForKey:@"imageName"];
            
            [tempArray addObject:news];
        }
        
        _newsList = [NSArray arrayWithArray:tempArray];
    }
    
    return _newsList;
}

@end
