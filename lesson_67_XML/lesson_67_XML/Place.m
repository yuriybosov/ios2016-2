//
//  Place.m
//  lesson_67_XML
//
//  Created by Yurii Bosov on 6/25/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "Place.h"

@implementation Place

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@, %@,%@", _address, _latitude, _longitude];
}

@end
