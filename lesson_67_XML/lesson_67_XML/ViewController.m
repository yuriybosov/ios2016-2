//
//  ViewController.m
//  lesson_67_XML
//
//  Created by Yurii Bosov on 6/25/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"
#import <AFNetworking.h>
#import <XMLDictionary.h>
#import "Place.h"


@interface ViewController () <NSXMLParserDelegate> {
    NSMutableArray <Place *> *places;
    NSString *tempElementName;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    places = [NSMutableArray new];
    
    //
    NSURL *url = [NSURL URLWithString:@"https://maps.googleapis.com"];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:url];
//    manager.responseSerializer = [[AFXMLParserResponseSerializer alloc] init];
    manager.responseSerializer = [[AFHTTPResponseSerializer alloc] init];
    NSDictionary *params = @{@"key":@"AIzaSyD1nF3rJ-3SUUTWva-ujVOa2aqp3wSy0eg", @"address":@"Днепр Кирова 2", @"language":@"ru"};
    
    [manager GET:@"maps/api/geocode/xml" parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id responseObject) {
        
        if ([responseObject isKindOfClass:[NSXMLParser class]]) {
            [responseObject setDelegate:self];
            [responseObject parse];
        } else if ([responseObject isKindOfClass:[NSData class]]) {
            // use XMLDictionary pod
            id data = [NSDictionary dictionaryWithXMLData:responseObject];
            NSLog(@"data %@", data);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error %@", error);
    }];
}

#pragma mark - NSXMLParserDelegate

- (void)parserDidStartDocument:(NSXMLParser *)parser {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    NSLog(@"\n%@",[places componentsJoinedByString:@"\n"]);
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary<NSString *,NSString *> *)attributeDict {

    if (tempElementName == nil) {
        tempElementName = elementName;
    } else {
        tempElementName = [tempElementName stringByAppendingFormat:@".%@",elementName];
    }
    
    NSLog(@"%@", tempElementName);
    
    if ([tempElementName isEqualToString:@"GeocodeResponse.result"]) {
        Place *place = [Place new];
        [places addObject:place];
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(nonnull NSString *)elementName namespaceURI:(nullable NSString *)namespaceURI qualifiedName:(nullable NSString *)qName {
    
    NSString *removeString = [NSString stringWithFormat:@".%@",elementName];
    
    if ([tempElementName hasSuffix:removeString]) {
        tempElementName = [tempElementName substringToIndex:tempElementName.length - removeString.length];
    }
    NSLog(@"%@", tempElementName);
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    if ([tempElementName isEqualToString:@"GeocodeResponse.result.formatted_address"]){
        places.lastObject.address = string;
    } else if ([tempElementName isEqualToString:@"GeocodeResponse.result.place_id"]){
        places.lastObject.placeID = string;
    } else if ([tempElementName isEqualToString:@"GeocodeResponse.result.geometry.location.lat"]){
        places.lastObject.latitude = @(string.floatValue);
    } else if ([tempElementName isEqualToString:@"GeocodeResponse.result.geometry.location.lng"]){
        places.lastObject.longitude = @(string.floatValue);
    }
}

@end
