//
//  main.m
//  lesson 5_1_for_while_forin
//
//  Created by Yuriy Bosov on 7/21/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        // цикл while
        NSInteger i = 0;
        while ( i <= 4 ) {
            // тело цикла
            NSLog(@"while - step %li", i);
            i++;
        }
        
        // цикл do while. в цикле do while тело цикла выполнится хотя бы один раз
        i = 0;
        do {
            // тело цикла
            NSLog(@"do while - step %li", i);
            i++;
        } while ( i <= 4 );
        
        // цикл for
        for (NSInteger i = 0; i <= 4; i++) {
            NSLog(@"for - step %li", i);
        }
        
        // цикл for in
        NSArray *array = @[@"1",@"2",@"3",@"4",@"5"];
        for (NSString *obj in array) {
            NSLog(@"for in - obj = %@", obj);
        }
        
        // пример на break, continue
        // пройтись по всем элементам массива, если это число - вывести в лог, если это NSData то ничего не делать, если это строка - то прервать цикл
        array = @[@(100), @(200), @(300), [NSData new], @(-100), @"test", @(-400), [NSData new]];
        
        for (id object in array) {
            
            if ([object isKindOfClass:[NSNumber class]]) {
                NSLog(@"object = %@", object);
            } else if ([object isKindOfClass:[NSData class]]) {
                continue;
            } else if ([object isKindOfClass:[NSString class]]) {
                break;
            }
            
            NSLog(@"тело цикла");
        }
        
        NSLog(@"%@",[NSDate date]);
        
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        df.dateFormat = @"yyyy MMM DD, hh:mm , EEEE, zzzz";
        df.locale = [NSLocale localeWithLocaleIdentifier:@"RU"];
        NSLog(@"%@", [df stringFromDate:[NSDate date]]);
    }
    return 0;
}
