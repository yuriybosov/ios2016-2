
#import <Foundation/Foundation.h>
#import "NewsSite.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        // создали сайт новостей
        NewsSite *site = [[NewsSite alloc] init];
        site.title = @"Лига.Нет";
        
        // добавляем новости
        [site addNews:[News newsWithTitle:@"Новость1"
                                     text:@"Текст Текст Текст Текст"
                                 category:@"Политика"
                                     tags:@[@"политика",@"финансы"]]];
        
        [site addNews:[News newsWithTitle:@"Новость2"
                                     text:@"Текст Текст Текст Текст"
                                 category:@"Спорт"
                                     tags:@[@"спорт",@"плавание"]]];
        
        [site addNews:[News newsWithTitle:@"Новость3"
                                     text:@"Текст Текст Текст Текст"
                                 category:@"Спорт"
                                     tags:@[@"спорт",@"бег",@"хобби"]]];
        
        [site addNews:[News newsWithTitle:@"Новость4"
                                     text:@"Текст Текст Текст Текст"
                                 category:@"Политика"
                                     tags:@[@"политика",@"президент",@"спорт"]]];
        
        News *news5 = [News newsWithTitle:@"Новость5"
                                     text:@"Текст Текст Текст Текст"
                                 category:@"Рыбалка"
                                     tags:@[@"рыбалка",@"хобби"]];
        [site addNews:news5];
        
        NSLog(@"%@", site);
        [site showNewsCategoriesInfo];
        
        // удаление
//        [site removeNews:news5];
//        NSLog(@"%@", site);
//        [site showNewsCategoriesInfo];
        
        NSLog(@"%@", [site tagsCloud]);
    }
    return 0;
}
