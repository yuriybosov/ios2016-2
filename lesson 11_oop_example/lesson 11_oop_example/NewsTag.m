//
//  NewsTag.m
//  lesson 11_oop_example
//
//  Created by Yuriy Bosov on 9/8/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "NewsTag.h"

@implementation NewsTag

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@ : %lu", _title, _count];
}

@end

@implementation NSArray (NewsTag)

- (NewsTag *)searchNewsTagByTitle:(NSString *)title {
    
    NewsTag *result = nil;
    
    for (NewsTag *obj in self) {
        if ([[obj.title lowercaseString] isEqualToString:[title lowercaseString]])
        {
            result = obj;
            break;
        }
    }
    
    return result;
}

@end

