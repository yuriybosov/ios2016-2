//
//  News.m
//  lesson 11_oop_example
//
//  Created by Yuriy Bosov on 9/8/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "News.h"

@implementation News

+ (News *)newsWithTitle:(NSString *)title
                   text:(NSString *)text
               category:(NSString *)categoryName
                   tags:(NSArray *)tags {
    
    News *obj = [[News alloc] init];
    
    obj.title = title;
    obj.text = text;
    obj.categoryName = categoryName;
    obj.tags = tags;
    
    return obj;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"title = %@, category = %@, count = %luu", _title, _categoryName,(unsigned long) _text.length];
}

@end
