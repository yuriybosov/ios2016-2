//
//  News.h
//  lesson 11_oop_example
//
//  Created by Yuriy Bosov on 9/8/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface News : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) NSString *categoryName;
@property (nonatomic, strong) NSArray *tags;

+ (News *)newsWithTitle:(NSString *)title
                   text:(NSString *)text
               category:(NSString *)categoryName
                   tags:(NSArray *)tags;

@end
