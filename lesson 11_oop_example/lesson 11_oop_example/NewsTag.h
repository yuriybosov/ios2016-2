//
//  NewsTag.h
//  lesson 11_oop_example
//
//  Created by Yuriy Bosov on 9/8/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NewsTag : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) NSUInteger count;

@end


@interface NSArray (NewsTag)

- (NewsTag *)searchNewsTagByTitle:(NSString *)title;

@end
