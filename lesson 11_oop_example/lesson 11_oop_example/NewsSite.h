//
//  NewsSite.h
//  lesson 11_oop_example
//
//  Created by Yuriy Bosov on 9/8/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "News.h"
#import "NewsTag.h"

@interface NewsSite : NSObject

@property (nonatomic, strong) NSString *title;

- (void)showNewsCategoriesInfo;

- (void)addNews:(News *)news;
- (void)removeNews:(News *)news;

//search news
- (NSArray *)searchNewsByTitle:(NSString *)title;
- (NSArray *)searchNewsByCategory:(NSString *)category;
- (NSArray *)searchNewsByTags:(NSArray *)tags;
- (NSArray *)searchNewsByText:(NSString *)text;

// tags info
- (NSArray *)tagsCloud;

@end
