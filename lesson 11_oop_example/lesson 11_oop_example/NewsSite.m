//
//  NewsSite.m
//  lesson 11_oop_example
//
//  Created by Yuriy Bosov on 9/8/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "NewsSite.h"

NSString* newsCoutnToString(NSUInteger cout);

NSString* newsCoutnToString(NSUInteger cout) {
    
    NSString *str = [NSString stringWithFormat:@"%lu новости", cout];
    
    if (cout % 10 >= 5 ||
        (cout % 100 >= 11 && cout % 100 <= 14) ||
        cout % 10 == 0) {
        
        str = [NSString stringWithFormat:@"%lu новостей", cout];
        
    } else if (cout %10 == 1 && cout % 100 != 11){
        
        str = [NSString stringWithFormat:@"%lu новость", cout];
    }
    
    return str;
}

@interface NewsSite ()
{
    NSMutableDictionary *newsList;
}

@end

@implementation NewsSite

- (instancetype)init
{
    self = [super init];
    if (self) {
        newsList = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (NSString *)description
{
    NSUInteger newsCount = 0;
    for (NSArray *newsArray in newsList.allValues) {
        newsCount += newsArray.count;
    }
    
    return [NSString stringWithFormat:@"title = %@, categories count = %lu, news cout = %lu", _title, newsList.count, newsCount];
}

- (void)showNewsCategoriesInfo {
    
    NSArray *categories = [newsList.allKeys sortedArrayUsingSelector:@selector(compare:)];
    
    for (NSString *category in categories) {
        
        NSLog(@"%@: %@", category, newsCoutnToString([newsList[category] count]));
    }
}

- (void)addNews:(News *)news {
    
    NSMutableArray *newsArray = [newsList objectForKey:news.categoryName];
    
    if (newsArray == nil) {
        newsArray = [[NSMutableArray alloc] init];
        [newsList setObject:newsArray forKey:news.categoryName];
    }
    
    [newsArray addObject:news];
}

- (void)removeNews:(News *)news {
    NSMutableArray *newsArray = [newsList objectForKey:news.categoryName];
    [newsArray removeObject:news];
    
    // если для данной категории нет новостей, удаляем эту категорию и массив
    if (newsArray.count == 0) {
        [newsList removeObjectForKey:news.categoryName];
    }
}

- (NSArray *)searchNewsByTitle:(NSString *)title {
    NSMutableArray *result = [NSMutableArray new];
    
    // for
    for (NSArray *newsArray in newsList.allValues) {
        for (News *news in newsArray) {
            if ([news.title rangeOfString:title options:NSCaseInsensitiveSearch].location != NSNotFound) {
                [result addObject:news];
            }
        }
    }
    
    return result;
}

- (NSArray *)searchNewsByCategory:(NSString *)category {
    return [newsList objectForKey:category];
}

- (NSArray *)searchNewsByTags:(NSArray *)tags {
#warning TODO!!!
    return nil;
}
- (NSArray *)searchNewsByText:(NSString *)text {
#warning TODO!!!
    return nil;
}

- (NSArray *)tagsCloud {
    
    NSMutableArray *result = [NSMutableArray new];
    
    // ищем теги, добавляем их в массив result
    for (NSArray *newsArray in newsList.allValues) {
        for (News *news in newsArray) {
            for (NSString *tag in news.tags) {
                
                NewsTag *newsTag = [result searchNewsTagByTitle:tag];
                
                if (!newsTag) {
                    newsTag = [NewsTag new];
                    newsTag.title = tag;
                    [result addObject:newsTag];
                }
                
                newsTag.count++;
            }
        }
    }
    
    // сортируем теги по кол-ву
    NSSortDescriptor *sd = [NSSortDescriptor sortDescriptorWithKey:@"count"
                                                         ascending:NO];
    [result sortUsingDescriptors:@[sd]];
    return result;
}

@end
