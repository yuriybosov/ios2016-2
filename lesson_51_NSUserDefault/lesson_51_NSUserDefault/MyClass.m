//
//  MyClass.m
//  lesson_51_NSUserDefault
//
//  Created by Yurii Bosov on 3/9/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "MyClass.h"

@implementation MyClass

- (instancetype)init
{
    self = [super init];
    if (self) {
        identyfier = @([NSDate date].timeIntervalSince1970);
    }
    return self;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@, %@", _name, identyfier];
}

#pragma mark - NSCoding

// encodeWithCoder: вызовется перед сохранением объекта
- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:identyfier forKey:@"identyfier"];
    [aCoder encodeObject:_name forKey:@"name"];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        identyfier = [aDecoder decodeObjectForKey:@"identyfier"];
        _name = [aDecoder decodeObjectForKey:@"name"];
    }
    return self;
}

@end
