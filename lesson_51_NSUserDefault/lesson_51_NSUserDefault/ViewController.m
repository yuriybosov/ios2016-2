//
//  ViewController.m
//  lesson_51_NSUserDefault
//
//  Created by Yurii Bosov on 3/9/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"
#import "MyClass.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

#pragma mark - Button Actions

- (IBAction)saveButtonClicked:(id)sender{
    // сохраняем данные
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    //
    [ud setObject:@"Save_String" forKey:@"key1"];
    [ud setObject:@(100) forKey:@"key2"];
    [ud setObject:[NSDate date] forKey:@"key3"];
    [ud setBool:YES forKey:@"key4"];
    [ud setDouble:100.10 forKey:@"key5"];
    
    [ud setObject:@[@"a",@"b",@"c"] forKey:@"key6"];
    [ud setObject:@{@"key1":@"value1",@"key2":@"value2"} forKey:@"key7"];
}

- (IBAction)loadButtonClicked:(id)sender{
    // загружаем данные
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    NSString *str = [ud objectForKey:@"key1"];
    NSLog(@"str = %@", str);
    NSNumber *num = [ud objectForKey:@"key2"];
    NSLog(@"num = %@", num);
    NSDate *date = [ud objectForKey:@"key3"];
    NSLog(@"date %@", date);
    BOOL b = [ud boolForKey:@"key4"];
    NSLog(@"b = %i", b);
    double d = [ud doubleForKey:@"key5"];
    NSLog(@"d = %f", d);
    NSArray *array = [ud objectForKey:@"key6"];
    NSLog(@"array = %@", array);
    NSDictionary *dict = [ud objectForKey:@"key7"];
    NSLog(@"dict = %@", dict);
}

- (IBAction)resetButtonClicked:(id)sender {
    // очищаем все данные в UserDefault
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    // пример удаления под одному ключу
    [ud removeObjectForKey:@"key6"];
    [ud setObject:nil forKey:@"key7"];
    
    // пример удаления всех сохраненных данных
    [ud removePersistentDomainForName:[[NSBundle mainBundle] bundleIdentifier]];
}

- (IBAction)saveCustomData:(id)sender {
    MyClass *obj1 = [MyClass new];
    obj1.name = @"name1";
    
    MyClass *obj2 = [MyClass new];
    obj2.name = @"name2";
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:@[obj1, obj2]];
    
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"key8"];
}

- (IBAction)loadCustomData:(id)sender {
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"key8"];
    if ([data isKindOfClass:[NSData class]]) {
        id obj = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        NSLog(@"obj = %@", obj);
    }
}

@end
