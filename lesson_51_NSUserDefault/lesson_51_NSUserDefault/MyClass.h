//
//  MyClass.h
//  lesson_51_NSUserDefault
//
//  Created by Yurii Bosov on 3/9/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>

// для того что бы сохранить объект в NSUserDefault нужно что бы данный класс реализовал протокол NSCoding

@interface MyClass : NSObject <NSCoding> {
    NSNumber *identyfier;
}

@property (nonatomic, strong) NSString *name;

@end
