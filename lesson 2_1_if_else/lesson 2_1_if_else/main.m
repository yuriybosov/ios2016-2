//
//  main.m
//  lesson 2_1_if_else
//
//  Created by Yuriy Bosov on 7/10/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        NSInteger a = 20;
        NSInteger b = 10;
        
        NSLog(@"a = %li", a);
        NSLog(@"b = %li", b);
        
        // операторы сравнения
        // > больше
        if (a > b)
            NSLog(@"a больше b");
        
        // < меньше
        if (a < b)
            NSLog(@"a меньше b");
        
        // == проверка на равенсто
        if (a == b)
            NSLog(@"a равно b");
        
        // != проверка на НЕ равенсто
        if (a != b)
            NSLog(@"a не равно b");
        
        // >= больше или равно
        if (a >= b)
            NSLog(@"a больше или равно b");
        
        // <= меньше или равно
        if (a <= b)
            NSLog(@"a меньше или равно b");
        
        // пример
        
        NSUInteger age = 56;
        NSLog(@"age = %lu", age);
        
        if (age <= 3)
        {
            NSLog(@"младенец");
        }
        else if (age <= 10)
        {
            NSLog(@"ребенок");
        }
        else if (age <= 16)
        {
            NSLog(@"подросток");
        }
        else if (age <= 21)
        {
            NSLog(@"юноша");
        }
        else if (a <= 45)
        {
            NSLog(@"человек среднего возраста");
        }
        else
        {
            NSLog(@"человек пенсионого возраста");
        }
        
        // привет2
        CGFloat price = 0;
        
        if (age <= 6)
        {
            price = 0;
        }
        else if (age <= 55)
        {
            price = 100;
        }
        else
        {
            price = 50;
        }

        NSLog(@"для возраста = %lu цена билета = %0.2f грн", age, price);
        
        // тернарный оператор
        // <условие> ? <код при YES> : <код при NO> ;
        
        // пример c if else
        if  (age <= 55)
        {
            NSLog(@"работающий человек");
        }
        else
        {
            NSLog(@"пенсионер");
        }
        // это же условие с помощью тернарного оператора
        (age <= 55) ? NSLog(@"работающий человек") : NSLog(@"пенсионер");
        
        // пример на цену билета
        price = (age <= 55) ? 100 : 50;
    }
    return 0;
}
