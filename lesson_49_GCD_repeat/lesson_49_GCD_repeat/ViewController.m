//
//  ViewController.m
//  lesson_49_GCD_repeat
//
//  Created by Yurii Bosov on 3/3/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

#pragma mark - Methods

- (void)testWithText:(NSString *)text {
    
    CFTimeInterval start = CACurrentMediaTime();
    for (NSUInteger i = 0; i < 1000000000; i++) {
    }
    CFTimeInterval end = CACurrentMediaTime();
    NSLog(@"%@ - %f", text, end - start);
    
    if ([NSThread isMainThread]) {
        [self testEnd];
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self testEnd];
        });
    }
}

- (void)testEnd {
    self.view.backgroundColor = [UIColor colorWithHue:drand48() saturation:1.0 brightness:1.0 alpha:1.0];
}

#pragma mark - Buttons Actions

- (IBAction)btn1Clicked:(id)sender {
    // выполнить в главном потоке
    [self testWithText:@"in main"];
}

- (IBAction)btn2Clicked:(id)sender {
    // выполнить не в главном потоке
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        [self testWithText:@"in background"];
    });
}

- (IBAction)btn3Clicked:(id)sender {
    // выполнить один раз
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [self testWithText:@"once"];
    });
}

- (IBAction)btn4Clicked:(id)sender {
    // выполнить с задержкой
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        [self testWithText:@"after"];
    });
}

- (IBAction)btn5Clicked:(id)sender {
    // выполнить в кастомной очереди с последовательным выполнением
    dispatch_queue_t queue = dispatch_queue_create("com.complany_name.app_name.queue_name", DISPATCH_QUEUE_SERIAL);
    
    dispatch_async(queue, ^{
        [self testWithText:@"1"];
    });
    dispatch_async(queue, ^{
        [self testWithText:@"2"];
    });
    dispatch_async(queue, ^{
        [self testWithText:@"3"];
    });
    dispatch_async(queue, ^{
        [self testWithText:@"4"];
    });
    dispatch_async(queue, ^{
        [self testWithText:@"5"];
    });
    dispatch_async(queue, ^{
        [self testWithText:@"6"];
    });
    dispatch_async(queue, ^{
        [self testWithText:@"7"];
    });
    dispatch_async(queue, ^{
        [self testWithText:@"8"];
    });
}

- (IBAction)btn6Clicked:(id)sender {
    // выполнить в кастомной очереди с паралельным выполнением
    dispatch_queue_t queue = dispatch_queue_create("com.complany_name.app_name.queue_name", DISPATCH_QUEUE_CONCURRENT);
    
    dispatch_async(queue, ^{
        [self testWithText:@"1"];
    });
    dispatch_async(queue, ^{
        [self testWithText:@"2"];
    });
    dispatch_async(queue, ^{
        [self testWithText:@"3"];
    });
    dispatch_async(queue, ^{
        [self testWithText:@"4"];
    });
    dispatch_async(queue, ^{
        [self testWithText:@"5"];
    });
    dispatch_async(queue, ^{
        [self testWithText:@"6"];
    });
    dispatch_async(queue, ^{
        [self testWithText:@"7"];
    });
    dispatch_async(queue, ^{
        [self testWithText:@"8"];
    });
}

@end
