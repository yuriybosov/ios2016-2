//
//  AppDelegate.m
//  lesson_49_GCD_repeat
//
//  Created by Yurii Bosov on 3/3/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    // вызов "тяжелого" метода в главном потоке
//    [self testWithText:@"main thread"];
    
    // вызов "тяжелого" метода через performInBg
//    [self performSelectorInBackground:@selector(testWithText:) withObject:@"background thread"];
    
    // вызов "тяжелого" метода используя созданый поток
//    NSThread *thread = [[NSThread alloc] initWithTarget:self selector:@selector(testWithText:) object:@"background thread 2"];
//    [thread start];
    
    // вызов "тяжелого" метода используя GCD
    // 1. создали очередь
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
//     2. с помощью финкции dispatch_async добавили в очередь новую задачу. эта задача будет выполнятся асинхронно
    dispatch_async(queue, ^{
        [self testWithText:@"gcd"];
    });
    
    return YES;
}

- (void)testWithText:(NSString *)text {
    
    // время начало задачи
    CFTimeInterval start = CACurrentMediaTime();
    
    //
    for (NSUInteger i = 0; i < 1000000000; i++) {
        
    }
    
    // время завершение задачи
    CFTimeInterval end = CACurrentMediaTime();
    NSLog(@"%@ - %f", text, end - start);
}

@end
