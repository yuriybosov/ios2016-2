//
//  CollectionViewController.m
//  lesson_33_UICollectionView
//
//  Created by Yurii Bosov on 11/24/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#define offset 20
#define number_of_column_portrait 3
#define number_of_column_landsckape 5

#import "CollectionViewController.h"

@interface CollectionViewController ()

@end

@implementation CollectionViewController

static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 3;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 23;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    // Configure the cell
    UILabel *label = [cell viewWithTag:10];
    label.text = [NSString stringWithFormat:@"section %li\nrow %li", indexPath.section, indexPath.item];
    
    return cell;
}

#pragma mark Rotation

// при смене ориентации девайса выполним reloadData у коллекшен вью
- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [self.collectionView reloadData];
}

#pragma mark UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    // нужно определить, какая сейчас ориентация
    UIInterfaceOrientation orientation =  [[UIApplication sharedApplication] statusBarOrientation];
    
    NSInteger countColumn = 0;
    
    if (UIInterfaceOrientationIsLandscape(orientation)) {
        countColumn = number_of_column_landsckape;
    }else {
        countColumn = number_of_column_portrait;
    }
    
    CGFloat widthItem = (collectionView.frame.size.width - 2*offset - (countColumn - 1)*offset)/countColumn;
    
    CGSize size = CGSizeMake(widthItem, widthItem);
    
    return size;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
    // делаем так, что бы все отступы были одинаковые, и что бы между секциями убрать двойной отступ
    return UIEdgeInsetsMake((section == 0) ? offset : 0,
                            offset,
                            offset,
                            offset);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    
    return offset;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return offset;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.item % 2 == 0) {
        // четное, показать red
        [self performSegueWithIdentifier:@"toRed" sender:nil];
    } else {
        // нечетное, показать green
        [self performSegueWithIdentifier:@"toGreen" sender:nil];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSLog(@"prepareForSegue %@", segue);
}

@end
