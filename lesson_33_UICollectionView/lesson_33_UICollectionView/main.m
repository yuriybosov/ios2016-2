//
//  main.m
//  lesson_33_UICollectionView
//
//  Created by Yurii Bosov on 11/24/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
