//
//  CollectionViewController.h
//  lesson_33_UICollectionView
//
//  Created by Yurii Bosov on 11/24/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewController : UICollectionViewController <UICollectionViewDelegateFlowLayout>

@end
