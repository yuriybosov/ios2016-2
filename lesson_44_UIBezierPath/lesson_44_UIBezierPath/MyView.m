//
//  MyView.m
//  lesson_44_UIBezierPath
//
//  Created by Yurii Bosov on 1/15/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "MyView.h"

@implementation MyView

- (void)awakeFromNib {
    [super awakeFromNib];
    //
}

- (void)drawRect:(CGRect)rect {
    
    // 1. настройка цвета (контрура и заливки)
    [[UIColor redColor] setFill];
    [[UIColor greenColor] setStroke];
    
    // 2.1 квадрат
    UIBezierPath *pathRect = [UIBezierPath bezierPathWithRect:CGRectMake(10, 30, 50, 50)];
    pathRect.lineWidth = 4;
    [pathRect stroke];
    [pathRect fill];
    
    // 2.2 квадрат c закругленными углами (все закругленные)
    UIBezierPath *pathRect2 = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(70, 30, 50, 50) cornerRadius:10];
    pathRect2.lineWidth = 4;
    [pathRect2 stroke];
    [pathRect2 fill];
    
    // 2.3. квадрат c закругленными углами (можно указать какие именно закруглить)
    UIBezierPath *pathRect3 = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(130, 30, 50, 50) byRoundingCorners:UIRectCornerTopLeft|UIRectCornerTopRight cornerRadii:CGSizeMake(10, 10)];
    pathRect3.lineWidth = 4;
    [pathRect3 stroke];
    [pathRect3 fill];
    
    // 3. круг
    UIBezierPath *pathOval = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(200, 30, 80, 50)];
    pathOval.lineWidth = 4;
    [pathOval stroke];
    [pathOval fill];
    
    // 4.
    CGPoint center = CGPointMake(150, 400);
    
    UIBezierPath *pathArc1 = [UIBezierPath bezierPathWithArcCenter:center radius:100 startAngle:0 endAngle:2* M_PI/5 clockwise:YES];
    pathArc1.lineWidth = 4;
    
    [pathArc1 addLineToPoint:center];
    
    [pathArc1 closePath];
    [pathArc1 stroke];
    [pathArc1 fill];
}

@end
