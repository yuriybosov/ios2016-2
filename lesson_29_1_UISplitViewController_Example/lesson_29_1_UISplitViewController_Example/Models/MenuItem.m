//
//  MenuItem.m
//  lesson_29_1_UISplitViewController_Example
//
//  Created by Yurii Bosov on 11/6/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import "MenuItem.h"

@implementation MenuItem

+ (MenuItem *)itemWithTitle:(NSString *)title
                  imageName:(NSString *)imageName
               storyboardID:(NSString *)storyboardID {
    
    MenuItem *item = [MenuItem new];
    
    item.title = title;
    item.imageName = imageName;
    item.storyboardID = storyboardID;
    
    return item;
}

@end
