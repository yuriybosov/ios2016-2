//
//  MenuItem.h
//  lesson_29_1_UISplitViewController_Example
//
//  Created by Yurii Bosov on 11/6/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MenuItem : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *imageName;
@property (nonatomic, strong) NSString *storyboardID;

+ (MenuItem *)itemWithTitle:(NSString *)title
                  imageName:(NSString *)imageName
               storyboardID:(NSString *)storyboardID;

@end
