//
//  MenuController.m
//  lesson_29_1_UISplitViewController_Example
//
//  Created by Yurii Bosov on 11/6/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import "MenuController.h"
#import "MenuCell.h"
#import "MenuItem.h"

@interface MenuController () {
    NSArray *dataSources;
}

@end

@implementation MenuController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBarHidden = YES;
    
    self.splitViewController.preferredDisplayMode = UISplitViewControllerDisplayModeAllVisible;
    
    dataSources = @[[MenuItem itemWithTitle:@"News"
                                  imageName:@"news"
                               storyboardID:@"newsStoryboardID"],
                    
                    [MenuItem itemWithTitle:@"Articles"
                                  imageName:@"articles"
                               storyboardID:@"articlesStoryboardID"],
                    
                    [MenuItem itemWithTitle:@"Friends"
                                  imageName:@"friends"
                               storyboardID:@"friendsStoryboardID"],
                    
                    [MenuItem itemWithTitle:@"Favorites"
                                  imageName:@"favorites"
                               storyboardID:@"favoritesStoryboardID"],
                    
                    [MenuItem itemWithTitle:@"Settings"
                                  imageName:@"settings"
                               storyboardID:@"settingsStoryboardID"]];
    
    // show news controller
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
    {
        MenuItem *item = dataSources[0];
        UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:item.storyboardID];
        [self.splitViewController showDetailViewController:vc sender:nil];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return dataSources.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MenuCell *cell = [tableView dequeueReusableCellWithIdentifier:@"menuCellID" forIndexPath:indexPath];
    MenuItem *item = dataSources[indexPath.row];
    
    cell.lbTitle.text = item.title;
    cell.logoView.image = [UIImage imageNamed:item.imageName];
    
    return cell;
}

#pragma mark - Table delegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    ;
    if ([cell isKindOfClass:[MenuCell class]]) {
        MenuCell *menuCell = (MenuCell *)cell;
        menuCell.topSeparator.hidden = indexPath.row == 0 ? YES : NO;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    MenuItem *item = dataSources[indexPath.row];
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:item.storyboardID];
    [self.splitViewController showDetailViewController:vc sender:nil];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}

@end
