//
//  MenuCell.m
//  lesson_29_1_UISplitViewController_Example
//
//  Created by Yurii Bosov on 11/6/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import "MenuCell.h"

@implementation MenuCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
