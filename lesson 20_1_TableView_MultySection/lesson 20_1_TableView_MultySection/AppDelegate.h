//
//  AppDelegate.h
//  lesson 20_1_TableView_MultySection
//
//  Created by Yuriy Bosov on 10/6/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

