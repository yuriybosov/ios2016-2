//
//  ViewController.m
//  lesson_34_UITextField
//
//  Created by Yurii Bosov on 11/27/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UITextFieldDelegate> {
    
    IBOutlet UITextField *tfName;
    IBOutlet UITextField *tfEmail;
    IBOutlet UITextField *tfPassword;
    IBOutlet UITextField *tfCustomInputView;
    
    IBOutletCollection(UITextField) NSArray *inputFields;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    for (UITextField *tf in inputFields) {
        if (tf == inputFields.lastObject) {
            tf.returnKeyType = UIReturnKeyDone;
        } else{
            tf.returnKeyType = UIReturnKeyNext;
        }
    }
    
//    // create custom accessory view
//    UIView *accessoryView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
//    accessoryView.backgroundColor = [UIColor yellowColor];
//    tfCustomInputView.inputAccessoryView = accessoryView;
//    
//    // create custom input view
//    UIView *inputView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 150)];
//    inputView.backgroundColor = [UIColor blueColor];
//    tfCustomInputView.inputView = inputView;
}

#pragma mark - Action

- (IBAction)hideKeyboard:(id)sender {
    [self.view endEditing:YES];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {

    // этот вариант используем если полей мало
//    if (textField == tfName) {
//        //becomeFirstResponder - делает поле ввода активный, т.е. редактируемым (показывается клавиатура)
//        [tfEmail becomeFirstResponder];
//    } else if (textField == tfEmail){
//        [tfPassword becomeFirstResponder];
//    } else if (textField == tfPassword) {
//        // resignFirstResponder - делает поле ввода неактивным (сркывается клавиатура)
//        [tfPassword resignFirstResponder];
//    }
    
    // этот вариант используем, когда полей ввода много
    NSUInteger index = [inputFields indexOfObject:textField];
    if (index != NSNotFound) {
        if (index < inputFields.count - 1) {
            UITextField *nextTextField = [inputFields objectAtIndex:index + 1];
            [nextTextField becomeFirstResponder];
        } else {
            [textField resignFirstResponder];
        }
    }
    
    
    return YES;
}

@end
