//
//  ViewController.m
//  lesson_31_UIPageViewController
//
//  Created by Yurii Bosov on 11/17/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = self.bgColor;
}

- (void)setBgColor:(UIColor *)bgColor {
    _bgColor = bgColor;
    if ([self isViewLoaded]) {
        self.view.backgroundColor = _bgColor;
    }
}

@end
