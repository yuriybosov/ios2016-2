//
//  ViewController.h
//  lesson_31_UIPageViewController
//
//  Created by Yurii Bosov on 11/17/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic, strong) UIColor *bgColor;

@end

