//
//  MyPageViewController.m
//  lesson_31_UIPageViewController
//
//  Created by Yurii Bosov on 11/17/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import "MyPageViewController.h"
#import "ViewController.h"

@interface MyPageViewController () <UIPageViewControllerDataSource, UIPageViewControllerDelegate> {
    
    NSArray <UIColor *> *dataSources;
    NSInteger currentIndex;
    UIColor *currentColor;
    
    ViewController *vc1;
    ViewController *vc2;
//    ViewController *nextVC;
}

@end

@implementation MyPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor blackColor];
    
    vc1 = [ViewController new];
    vc2 = [ViewController new];
    
    dataSources = @[[UIColor yellowColor],
                    [UIColor blueColor],
                    [UIColor greenColor],
                    [UIColor grayColor],
                    [UIColor purpleColor],
                    [UIColor magentaColor]];
    
    self.dataSource = self;
    self.delegate = self;
    
    currentIndex = 0;
    currentColor = dataSources[currentIndex];
    vc1.bgColor = currentColor;
    
    [self setViewControllers:@[vc1]
                   direction:UIPageViewControllerNavigationDirectionForward
                    animated:NO
                  completion:nil];
}

#pragma mark - UIPageViewControllerDataSource

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    ViewController *result = nil;
    if (currentIndex > 0) {
        
        currentColor = dataSources[currentIndex - 1];
        vc2.bgColor = currentColor;
        result = vc2;
    }
    
    return result;
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    
    ViewController *result = nil;
    if (currentIndex < dataSources.count - 1) {
        
        currentColor = dataSources[currentIndex + 1];
        vc2.bgColor = currentColor;
        result =  vc2;
    }
    
    return result;
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
    return dataSources.count;
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController{
    return currentIndex;
}

#pragma makr - UIPageViewControllerDelegate

- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray<UIViewController *> *)previousViewControllers transitionCompleted:(BOOL)completed {
    if (completed) {
        // update current index
        // update prev\cur\next
        
        ViewController *temp = (ViewController *)[previousViewControllers firstObject];
        vc1 = vc2;
        vc2 = temp;
        
        currentIndex = [dataSources indexOfObject:vc1.bgColor];
    }
}

@end
