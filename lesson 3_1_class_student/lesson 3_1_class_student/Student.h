//
//  StudentASD.h
//  lesson 3_1_class_student
//
//  Created by Yuriy Bosov on 7/14/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef enum Gender {
    GenderNone,
    GenderMale,
    GenderFemale
}Gender;

@interface Student : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) NSUInteger age;
@property (nonatomic, assign) Gender gender;

+(Student *)createStudentWithName:(NSString *)name
                          withAge:(NSUInteger)age
                       withGender:(Gender)gender;

- (Student *)initWithName:(NSString *)name
                  withAge:(NSUInteger)age
               withGender:(Gender)gender;

@end
