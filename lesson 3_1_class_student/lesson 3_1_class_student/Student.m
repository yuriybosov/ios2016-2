//
//  StudentASD.m
//  lesson 3_1_class_student
//
//  Created by Yuriy Bosov on 7/14/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "Student.h"

@implementation Student

+(Student *)createStudentWithName:(NSString *)name
                          withAge:(NSUInteger)age
                       withGender:(Gender)gender {
    
    Student *student = [[Student alloc] initWithName:name
                                             withAge:age
                                          withGender:gender];
    return student;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        //
        _age = 16;
        _gender = GenderNone;
    }
    return self;
}

- (Student *)initWithName:(NSString *)name
                  withAge:(NSUInteger)age
               withGender:(Gender)gender {
    self = [super init];
    if (self){
        
        _name = name;
        _age = age;
        _gender = gender;
        
    }
    return self;
}

- (NSString *)description{
    return [NSString stringWithFormat:@"name = %@, age = %lu, gender = %u",_name, _age, _gender];
}

@end
