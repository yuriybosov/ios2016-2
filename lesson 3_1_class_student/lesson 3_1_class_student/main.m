//
//  main.m
//  lesson 3_1_class_student
//
//  Created by Yuriy Bosov on 7/14/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Student.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        // 1
        Student *student = [[Student alloc] init]; // new
        
        student.name = @"Yuriy";
        student.age = 21;
        student.gender = GenderMale;
        
        NSLog(@"%@", [student description]);
        
        Student *student2 = [[Student alloc] initWithName:@"qwer"
                                                  withAge:21
                                               withGender:GenderMale];
        NSLog(@"%@", student2);
        
        Student *student3 = [Student createStudentWithName:@"adf"
                                                   withAge:34
                                                withGender:GenderMale];
        NSLog(@"%@", student3);
    }
    return 0;
}
