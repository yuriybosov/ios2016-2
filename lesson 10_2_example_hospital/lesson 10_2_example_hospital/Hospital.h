//
//  Hospital.h
//  lesson 10_2_example_hospital
//
//  Created by Yuriy Bosov on 9/1/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    WeekDayMon,
    WeekDayTue,
    WeekDayWen,
    WeekDayThu,
    WeekDayFri
} WeekDay;

typedef enum : NSUInteger {
    WorkHours_9_11,
    WorkHours_11_13,
    WorkHours_14_16,
    WorkHours_16_18,
} WorkHours;

@interface Hospital : NSObject

- (BOOL)canAddNoteForDay:(WeekDay)day
                  atTime:(WorkHours)time;

- (void)addNote:(NSString *)note
         forDay:(WeekDay)day
         atTime:(WorkHours)time;

@end
