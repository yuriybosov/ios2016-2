//
//  Hospital.m
//  lesson 10_2_example_hospital
//
//  Created by Yuriy Bosov on 9/1/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "Hospital.h"

@interface Hospital ()
{
    NSMutableDictionary *weekTableTime;
}

- (void)setupTimeForWeekDay:(WeekDay)weekDay;

@end


@implementation Hospital

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        weekTableTime = [NSMutableDictionary new];
        
        [self setupTimeForWeekDay:WeekDayMon];
        [self setupTimeForWeekDay:WeekDayTue];
        [self setupTimeForWeekDay:WeekDayWen];
        [self setupTimeForWeekDay:WeekDayThu];
        [self setupTimeForWeekDay:WeekDayFri];
    }
    return self;
}

- (void)setupTimeForWeekDay:(WeekDay)weekDay {
    
    NSMutableDictionary *dict = [NSMutableDictionary new];
    
    [dict setObject:@"" forKey:@(WorkHours_9_11)];
    [dict setObject:@"" forKey:@(WorkHours_11_13)];
    [dict setObject:@"" forKey:@(WorkHours_14_16)];
    [dict setObject:@"" forKey:@(WorkHours_16_18)];
    
    [weekTableTime setObject:dict forKey:@(weekDay)];
}

- (BOOL)canAddNoteForDay:(WeekDay)day
                  atTime:(WorkHours)time {
    
    NSDictionary *dayTime = [weekTableTime objectForKey:@(day)];
    NSString* note = [dayTime objectForKey:@(time)];
    
    return note.length == 0 ? YES : NO;
}

- (void)addNote:(NSString *)note
         forDay:(WeekDay)day
         atTime:(WorkHours)time {
    
    if ([self canAddNoteForDay:day atTime:time]) {
        
        NSMutableDictionary *dayTime = [weekTableTime objectForKey:@(day)];
        [dayTime setObject:note forKey:@(time)];
        
    } else{
        NSLog(@"Запись добавить не возможно, так как она уже существует!");
    }
}

@end
