//
//  ViewController.m
//  lesson_35_1_DatePicker
//
//  Created by Yurii Bosov on 12/4/16.
//  Copyright © 2016 comfy. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {
    IBOutlet UILabel *label1;
    IBOutlet UILabel *label2;
    IBOutlet UILabel *label3;
    
    IBOutlet UIDatePicker *datePicke;
    
    NSDateFormatter *dateFormatter1;
    NSDateFormatter *dateFormatter2;
    NSDateFormatter *dateFormatter3;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    dateFormatter1 = [NSDateFormatter new];
    dateFormatter1.dateFormat = @"HH:mm:ss";
    
    dateFormatter2 = [NSDateFormatter new];
    dateFormatter2.dateFormat = @"dd.MM.yyyy";
    
    dateFormatter3 = [NSDateFormatter new];
    dateFormatter3.dateFormat = @"EEEE dd MMMM";
    
}

- (IBAction)changeDateValue:(id)sender {
    NSDate *selectedDate = datePicke.date;
    label1.text = [dateFormatter1 stringFromDate:selectedDate];
    label2.text = [dateFormatter2 stringFromDate:selectedDate];
    label3.text = [dateFormatter3 stringFromDate:selectedDate];
}

@end
