//
//  ViewController.m
//  lesson_60_MapKit
//
//  Created by Yurii Bosov on 6/1/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"
#import <MapKit/MapKit.h>

@import GooglePlaces;

@interface ViewController () <MKMapViewDelegate> {
    CLLocationManager *manager;
    
    GMSPlacesClient *placesClient;
}

@property (nonatomic, weak) IBOutlet MKMapView *mapView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    manager = [CLLocationManager new];
    [manager requestWhenInUseAuthorization];
    
    MKPointAnnotation *point = [MKPointAnnotation new];
    point.coordinate = CLLocationCoordinate2DMake(48.4638695, 35.04795);
    point.title = @"Dnipro";
    point.subtitle = @"my city";
    
    [self.mapView addAnnotation:point];
    
    placesClient = [GMSPlacesClient sharedClient];
}

#pragma mark - Actions

- (IBAction)cityButtonClicked:(id)sender {
    // show Dnipro 48.4638695, 35.04795
    MKCoordinateRegion region;
    region.center = CLLocationCoordinate2DMake(48.4638695, 35.04795);
    region.span = MKCoordinateSpanMake(0.1, 0.1);
    [self.mapView setRegion:region animated:YES];
}

- (IBAction)longTap:(UILongPressGestureRecognizer *)sender {
    if (sender.state == UIGestureRecognizerStateBegan) {
        CGPoint point = [sender locationInView:self.mapView];
        
        CLLocationCoordinate2D coordinate = [self.mapView convertPoint:point toCoordinateFromView:self.mapView];
        
        NSLog(@"%f, %f", coordinate.latitude, coordinate.longitude);
        
        
//        GMSPlacePicker 
        
    }
}

#pragma mark - MKMapViewDelegate

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        MKCoordinateRegion region;
        region.center = userLocation.location.coordinate;
        region.span = MKCoordinateSpanMake(0.05, 0.05);
        
        [self.mapView setRegion:region animated:YES];
    });
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    
    MKAnnotationView *view = nil;
    
    if (![annotation isKindOfClass:[MKUserLocation class]]) {
        view =[[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"custom_anotation"];
        
        UIImage *pinImage = [UIImage imageNamed:@"mapPin"];
        view.image = pinImage;
        view.centerOffset = CGPointMake(0, -pinImage.size.height/2);
    }
    
    return view;
}

@end
