//
//  MyClass.h
//  lesson 8_2 Property
//
//  Created by Yuriy Bosov on 8/28/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyClass : NSObject

// assign - применяется для простых типов, доступно запись\чтение (assign указан по умолчанию)
@property (nonatomic, assign) CGFloat value1;
@property (nonatomic, assign) NSInteger value2;
@property (nonatomic, assign) BOOL value3;

// readonly - применяется для простых и сложных типов, доступно только чтение
@property (nonatomic, readonly) BOOL value4;
@property (nonatomic, readonly) NSString *value5;

// weak (слабая ссылка) - применяется только для сложных типов, доступно чтение\запись.
// важно(!) - не ретейнит объект (т.е. не увеличивате ему счетчик ссылок)
// важно(!) - при удалении объекта weak свойство становится равным nil
@property (nonatomic, weak) NSString *value6;
@property (nonatomic, weak) NSDate *value7;

// strong (сильная ссылка) - применяется только для сложных типов, доступно чтение\запись.
// важно(!) - ретейнит объект (т.е. увеличивате ему счетчик ссылок)
// важно(!) - это свойство удалится либо при удаление объкта, в котором это свойство указано, либо при "ручном" занулении свойств (self.value8 = nil;)
@property (nonatomic, strong) NSString *value8;

@end
