//
//  ModalViewController.m
//  lesson_41_2_SwipeRecognizer
//
//  Created by Yurii Bosov on 12/25/16.
//  Copyright © 2016 ios. All rights reserved.
//

#import "ModalViewController.h"

@interface ModalViewController ()

@end

@implementation ModalViewController

- (IBAction)swipeToBottom:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
