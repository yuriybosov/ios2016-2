//
//  ResultTableController.m
//  lesson_49_UISearchController
//
//  Created by Yurii Bosov on 12/22/16.
//  Copyright © 2016 ios. All rights reserved.
//

#import "ResultTableController.h"

@interface ResultTableController () {
    NSArray *dataSources;
}

@end

@implementation ResultTableController

#pragma mark - ShowSearchResultProtocol

- (void)setSearchResultData:(NSArray *)resultData {
    
    dataSources = resultData;
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataSources.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID" forIndexPath:indexPath];
    
    cell.textLabel.text = dataSources[indexPath.row];
    
    return cell;
}

@end
