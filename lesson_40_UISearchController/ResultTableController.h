//
//  ResultTableController.h
//  lesson_49_UISearchController
//
//  Created by Yurii Bosov on 12/22/16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShowSearchResultProtocol.h"

@interface ResultTableController : UITableViewController <ShowSearchResultProtocol>

@end
