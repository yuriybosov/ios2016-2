//
//  ViewController.m
//  lesson_49_UISearchController
//
//  Created by Yurii Bosov on 12/22/16.
//  Copyright © 2016 ios. All rights reserved.
//

#import "ViewController.h"
#import "ResultTableController.h"
#import "ResultCollectionController.h"

@interface ViewController () <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate> {
    
    IBOutlet UITableView *tableView;
    UISearchController *searchController;
    
    NSArray *dataSources;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Search";
    
    //
    dataSources = @[@"qwe",@"adsfa",@"asfd",@"few",@"fwew",@"gerwq",@"fwe",@"weqff",@"fwfg",@"fwfwef",@"wfewfew",@"hyt",@"ggre"];

    UIViewController *resultController = [self.storyboard instantiateViewControllerWithIdentifier:@"ResultTableController"];
    
    // создали UISearchController
    searchController = [[UISearchController alloc] initWithSearchResultsController:resultController];
//    searchController.hidesNavigationBarDuringPresentation = NO;
    //
    tableView.tableHeaderView = searchController.searchBar;
    
    //
    searchController.searchBar.delegate = self;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataSources.count;
}

- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID"];
    cell.textLabel.text = dataSources[indexPath.row];
    return cell;
}

#pragma mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    // фильтруем массив строк, используя предикат
    NSPredicate *preidcate = [NSPredicate predicateWithFormat:@"self contains[cd] %@", searchText];
    NSArray *searchResult = [dataSources filteredArrayUsingPredicate:preidcate];
    
    // отобразить searchResult в экране результат поиска
    if([searchController.searchResultsController conformsToProtocol:@protocol(ShowSearchResultProtocol)]) {
        
        // или так вызываем
//        [searchController.searchResultsController performSelector:@selector(setSearchResultData:) withObject:searchResult];
        
        // или так вызываем
        [(id<ShowSearchResultProtocol>)searchController.searchResultsController setSearchResultData:searchResult];
    }
}

@end
