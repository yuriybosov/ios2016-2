//
//  ShowSearchResultProtocol.h
//  lesson_49_UISearchController
//
//  Created by Yurii Bosov on 12/22/16.
//  Copyright © 2016 ios. All rights reserved.
//

#ifndef ShowSearchResultProtocol_h
#define ShowSearchResultProtocol_h

#import <UIKit/UIKit.h>

@protocol ShowSearchResultProtocol <NSObject>

- (void)setSearchResultData:(NSArray *)resultData;

@end

#endif /* ShowSearchResultProtocol_h */
