//
//  ViewController.m
//  lesson_61_GoogleMap
//
//  Created by Yurii Bosov on 6/5/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import <AFNetworking.h>

@import GoogleMaps;

@interface ViewController () <GMSMapViewDelegate> {
    NSMutableArray <GMSPolyline *> *polylines;
}

@property (nonatomic, weak) IBOutlet GMSMapView *mapView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    polylines = [NSMutableArray new];
    
    self.mapView.myLocationEnabled = YES;
    self.mapView.settings.compassButton = YES;
    self.mapView.settings.myLocationButton = YES;
    
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake(48.4638695, 35.04795);
    marker.title = @"Dnipto";
    marker.snippet = @"My city";
    marker.icon = [UIImage imageNamed:@"mapPin"];
    marker.map = self.mapView;
}

#pragma mark - Actions

- (IBAction)showDniproCity:(id)sender{
    //
    GMSCameraPosition *positon = [GMSCameraPosition cameraWithTarget:CLLocationCoordinate2DMake(48.4638695, 35.04795) zoom:15];
//    [self.mapView setCamera:positon];
    [self.mapView animateToCameraPosition:positon];
}

#pragma mark - GMSMapViewDelegate

- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate {
    
    // проверка что локация пользователя доступна, так как маршрут мы строим от нее
    if (self.mapView.myLocation) {
        
        // показали HUD
        [SVProgressHUD showWithStatus:@"..."];
        
        // параметры для запроса
        NSString *origin = [NSString stringWithFormat:@"%f,%f", self.mapView.myLocation.coordinate.latitude,self.mapView.myLocation.coordinate.longitude];
        NSString *destination = [NSString stringWithFormat:@"%f,%f", coordinate.latitude, coordinate.longitude];
        NSString *urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/directions/json?origin=%@&destination=%@&key=%@&alternatives=true", origin, destination, @"AIzaSyD1nF3rJ-3SUUTWva-ujVOa2aqp3wSy0eg"];
        
        // выполняем запрос
        [[AFHTTPSessionManager manager] GET:urlString parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            [self directionsCallbackWithResponseObject:responseObject error:nil];
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [self directionsCallbackWithResponseObject:nil
                                                 error:error];
        }];
    }
}

- (void)mapView:(GMSMapView *)mapView didTapOverlay:(GMSOverlay *)overlay {
    if ([polylines containsObject:(GMSPolyline *)overlay]) {
        [self selectPolyline:(GMSPolyline *)overlay];
    }
}

#pragma mark - Responce

- (void)directionsCallbackWithResponseObject:(id)responseObject error:(NSError *)error {
    
    [self removePolylines];
    
    if (responseObject) {
        // построим маршрут
        // routes
        NSArray *array = [responseObject objectForKey:@"routes"];
        if ([array isKindOfClass:[NSArray class]] &&
            array.count > 0) {
            
            [SVProgressHUD dismiss];
            
            for (NSDictionary *routData in array) {
                NSString *points = [routData valueForKeyPath:@"overview_polyline.points"];
                GMSPath *path = [GMSPath pathFromEncodedPath:points];
                
                GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
                polyline.tappable = YES;
                polyline.map = self.mapView;
                polyline.strokeWidth = 5;
//                polyline.strokeColor = [UIColor colorWithRed:arc4random() % 256 / 255.f green:arc4random() % 256 / 255.f blue:arc4random() % 256 / 255.f alpha:1];
                
                [polylines addObject:polyline];
            }
            [self selectPolyline:polylines.firstObject];
            
        } else {
            //
            [SVProgressHUD showErrorWithStatus:@"Не удалось построить маршрут"];
            [SVProgressHUD dismissWithDelay:4];
        }
        
    }else{
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        [SVProgressHUD dismissWithDelay:4];
    }
}

- (void)removePolylines {
    for (GMSPolyline *polyline in polylines) {
        polyline.map = nil;
    }
    [polylines removeAllObjects];
}

- (void)selectPolyline:(GMSPolyline *)aPolyline {
    for (GMSPolyline *polyline in polylines) {
        if (polyline == aPolyline) {
            polyline.strokeColor = [UIColor blueColor];
            polyline.zIndex = 2;
        } else {
            polyline.strokeColor = [[UIColor blueColor] colorWithAlphaComponent:0.3];
            polyline.zIndex = 1;
        }
    }
}

@end
