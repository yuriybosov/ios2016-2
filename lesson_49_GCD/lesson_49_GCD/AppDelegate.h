//
//  AppDelegate.h
//  lesson_49_GCD
//
//  Created by Yurii Bosov on 3/2/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

