//
//  AppDelegate.m
//  lesson_49_GCD
//
//  Created by Yurii Bosov on 3/2/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    //1. c помощью performSelectorInBackground
//    [self performSelectorInBackground:@selector(test) withObject:nil];
    
    //2. создать поток
//    NSThread *thread = [[NSThread alloc] initWithTarget:self selector:@selector(test) object:nil];
//    [thread start];
    
    //3. с помощью CGD
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
//        [self testWithTitle:@"DISPATCH_QUEUE_PRIORITY_LOW"];
//    });
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
//        [self testWithTitle:@"DISPATCH_QUEUE_PRIORITY_BACKGROUND"];
//    });
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        [self testWithTitle:@"DISPATCH_QUEUE_PRIORITY_DEFAULT"];
//    });
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
//        [self testWithTitle:@"DISPATCH_QUEUE_PRIORITY_HIGH"];
//    });
//    
    return YES;
}

- (void)test {
    CFTimeInterval start = CACurrentMediaTime();
    for (NSInteger i = 0; i < 20000; i++) {
        NSLog(@"i = %li", i);
    }
    CFTimeInterval end = CACurrentMediaTime();
    NSLog(@"%f", end - start);
}

- (void)testWithTitle:(NSString *)title {
    CFTimeInterval start = CACurrentMediaTime();
    for (NSInteger i = 0; i < 20000000000; i++) {
        
    }
    CFTimeInterval end = CACurrentMediaTime();
    NSLog(@"%@, %f", title, end - start);
}


@end
