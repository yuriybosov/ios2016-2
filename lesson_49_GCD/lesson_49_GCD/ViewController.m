//
//  ViewController.m
//  lesson_49_GCD
//
//  Created by Yurii Bosov on 3/2/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

#pragma mark - Test Methods

- (void)testWithText:(NSString *)text {
    
    CFTimeInterval start = CACurrentMediaTime();
    for (NSInteger i = 0; i < 1000000000; i++) {
        
    }
    CFTimeInterval end = CACurrentMediaTime();
    NSLog(@"%@ - %f", text, end - start);
    
    if ([NSThread isMainThread]) {
        [self testEnd];
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self testEnd];
        });
    }
}

- (void)testEnd {
    self.view.backgroundColor = [UIColor colorWithHue:drand48() saturation:1.0 brightness:1.0 alpha:1.0];
}

#pragma mark - Button Actions

- (IBAction)buttin1Clicked{
    // выполнить метод в главном потоке
    // dispatch_get_main_queue() - возвращает очередь из главного потока
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self testWithText:@"in main"];
    });
}

- (IBAction)buttin2Clicked{
    // выполнить метод в бекграунд потоке
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        [self testWithText:@"in background"];
    });
}

- (IBAction)buttin3Clicked{
    // выполнить метод один раз
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [self testWithText:@"once"];
    });
}

- (IBAction)buttin4Clicked{
    // выполнить метод в кастомной очереди
    // 1. создаем очередь
    // DISPATCH_QUEUE_SERIAL - последовательное выполнение
    // DISPATCH_QUEUE_CONCURRENT - паралельное выполнение
    dispatch_queue_t qu = dispatch_queue_create("com.company_name.app_name.thread_name",DISPATCH_QUEUE_CONCURRENT);
    
    dispatch_async(qu, ^{
        [self testWithText:@"1"];
    });
    dispatch_async(qu, ^{
        [self testWithText:@"2"];
    });
    dispatch_async(qu, ^{
        [self testWithText:@"3"];
    });
    dispatch_async(qu, ^{
        [self testWithText:@"4"];
    });
    dispatch_async(qu, ^{
        [self testWithText:@"5"];
    });
    dispatch_async(qu, ^{
        [self testWithText:@"6"];
    });
    dispatch_async(qu, ^{
        [self testWithText:@"7"];
    });
    dispatch_async(qu, ^{
        [self testWithText:@"8"];
    });
    dispatch_async(qu, ^{
        [self testWithText:@"9"];
    });
}

- (IBAction)buttin5Clicked{
    // выполнить метод спустя какое то время
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            [self testWithText:@"after"];
        });
        
    });
}

@end
