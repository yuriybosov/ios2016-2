//
//  main.m
//  lesson 12_1_NSNotificationCenter_Example
//
//  Created by Yuriy Bosov on 9/11/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "Human.h"
#import "Printer.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        Printer *p = [[Printer alloc] init];
        p.currentPageCount = 5;
        
        Human *h = [[Human alloc] init];
        
        [p printPageCount:10];
        
        [p printPageCount:2];
    }
    return 0;
}
