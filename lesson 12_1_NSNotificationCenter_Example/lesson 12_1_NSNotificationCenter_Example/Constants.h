//
//  Constants.h
//  lesson 12_1_NSNotificationCenter_Example
//
//  Created by Yuriy Bosov on 9/11/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

static NSString *const kPrinterDidStart = @"kPrinterDidStart";
static NSString *const kPrinterInProcess = @"kPrinterInProcess";
static NSString *const kPrinterDidEnd = @"kPrinterDidEnd";
static NSString *const kPrinterDidEndWithFialed = @"kPrinterDidEndWithFialed";

#endif /* Constants_h */
