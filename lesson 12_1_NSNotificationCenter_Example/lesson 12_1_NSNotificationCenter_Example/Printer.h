//
//  Printer.h
//  lesson 12_1_NSNotificationCenter_Example
//
//  Created by Yuriy Bosov on 9/11/16.
//  Copyright © 2016 iOS. All rights reserved.
//

@interface Printer : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) NSUInteger currentPageCount;

- (void)printPageCount:(NSUInteger)cout;

@end
