//
//  Printer.m
//  lesson 12_1_NSNotificationCenter_Example
//
//  Created by Yuriy Bosov on 9/11/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "Printer.h"

@implementation Printer

- (void)printPageCount:(NSUInteger)count {
    
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    
    if (_currentPageCount > 0) {
        // prit start
        [nc postNotificationName:kPrinterDidStart
                          object:self
                        userInfo:@{@"message":@"print start"}];
        
        // prit in process
        NSInteger i = 1;
        for (; i <= count; i++) {
            
            if (_currentPageCount > 0) {
                //
                _currentPageCount--;
                
                NSString *str = [NSString stringWithFormat:@"print from %lu to %lu pages", i, count];
                NSDictionary *info = @{@"message":str};
                
                [nc postNotificationName:kPrinterInProcess
                                  object:self
                                userInfo:info];
                
            } else {
                // не хватило бумаги, Ошибка!!!
                [nc postNotificationName:kPrinterDidEndWithFialed
                                  object:self
                                userInfo:@{@"message":@"papir ended"}];
                break;
            }
            
            // end
            if (i == count) {
                [nc postNotificationName:kPrinterDidEnd
                                  object:self
                                userInfo:@{@"message":@"prin end"}];
            }
        }
        
    } else{
        
        [nc postNotificationName:kPrinterDidEndWithFialed object:self userInfo:@{@"message":@"not papir"}];
    }
}

@end
