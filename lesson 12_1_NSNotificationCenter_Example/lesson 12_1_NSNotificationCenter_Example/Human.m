//
//  Human.m
//  lesson 12_1_NSNotificationCenter_Example
//
//  Created by Yuriy Bosov on 9/11/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "Human.h"

@implementation Human

- (instancetype)init
{
    self = [super init];
    if (self) {
        NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
        
        [nc addObserver:self
               selector:@selector(printerDidStart:)
                   name:kPrinterDidStart
                 object:nil];
        
        [nc addObserver:self
               selector:@selector(printeInProcess:)
                   name:kPrinterInProcess
                 object:nil];
        
        [nc addObserver:self
               selector:@selector(printerDidEnd:)
                   name:kPrinterDidEnd
                 object:nil];
        
        [nc addObserver:self
               selector:@selector(printerDidEndWithFialed:)
                   name:kPrinterDidEndWithFialed
                 object:nil];
    }
    return self;
}

#pragma mark - Notifications

- (void)printerDidStart:(NSNotification *)notification {
    NSLog(@"printerDidStart %@", notification);
}

- (void)printeInProcess:(NSNotification *)notification {
    NSLog(@"printeInProcess %@", notification);
}

- (void)printerDidEnd:(NSNotification *)notification {
    NSLog(@"printerDidEnd %@", notification);
}

- (void)printerDidEndWithFialed:(NSNotification *)notification {
    NSLog(@"printerDidEndWithFialed %@", notification);
}

@end
