//
//  ViewController.m
//  UI_Lesson_36_RegularExpressions
//
//  Created by Kudlay Alex on 08.12.16.
//  Copyright © 2016 Kudlay Alex. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UITextFieldDelegate>
{
    IBOutlet UITextField *tfOnlyNumbers;
    IBOutlet UITextField *tfLatinSymbol;
    IBOutlet UITextField *tfCardNumber;
}
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    tfOnlyNumbers.placeholder = @"Введите только числа";
    tfLatinSymbol.placeholder = @"Введите латинские символы";
    tfCardNumber.placeholder = @"0000 0000 0000 0000";
}

#pragma mark Action

-(IBAction)hideKeyboard:(id)sender{
    [self.view endEditing:YES];
}

-(IBAction)validateButtonClicked:(id)sender{
    
}

#pragma mark UITextFieldDelegate

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    BOOL result = YES;
    if (string.length) {
        
        //обрезаем строку с помощью NSCharacterSet (убираем лишние пробелы)
        NSString *trinString = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        //проверяем вводимую строку для поля ввода tfOnlyNumbers
        if (textField == tfOnlyNumbers) {
            
            //проверяем на ввод чисел с помощью регулярного выражения
            NSRegularExpression *regExpr = [[NSRegularExpression alloc] initWithPattern:@"^[0-9]+$" options:NSRegularExpressionCaseInsensitive error:nil];
            NSUInteger count = [regExpr numberOfMatchesInString:trinString options:0 range:NSMakeRange(0, trinString.length)];
            if (count != 1) {
                result = NO;
            }
        } else if (textField == tfLatinSymbol){
            //проверяем на ввод только латинский символов
            //проверяем на ввод чисел с помощью регулярного выражения
            NSRegularExpression *regExpr = [[NSRegularExpression alloc] initWithPattern:@"^[A-Za-z]+$" options:NSRegularExpressionCaseInsensitive error:nil];
            NSUInteger count = [regExpr numberOfMatchesInString:trinString options:0 range:NSMakeRange(0, trinString.length)];
            if (count != 1) {
                result = NO;
            }
        } else if (textField == tfCardNumber){
            
            NSRegularExpression *regExpr = [[NSRegularExpression alloc] initWithPattern:@"^[0-9]+$" options:NSRegularExpressionCaseInsensitive error:nil];
            NSUInteger count = [regExpr numberOfMatchesInString:trinString options:0 range:NSMakeRange(0, trinString.length)];
            
            if (count == 1) {
                // получили текущую строку,
                // она может быть в виде: "4444 4444 4444"
                // убираем все пробелы
                NSMutableString * currentCardText = [NSMutableString stringWithString:[tfCardNumber.text stringByReplacingOccurrencesOfString:@" " withString:@""]];
                // проверяем что текущая строка и новая меньше или равно 16-ти символов
                if (currentCardText.length + trinString.length <= 16) {
                    
                    [currentCardText appendString:trinString];
                    
                    // разбиваем новую строку по 4 символа + пробел
                    for (NSInteger i = 0; i < currentCardText.length; i+=5) {
                        [currentCardText insertString:@" " atIndex:i];
                    }
                    tfCardNumber.text = currentCardText;
                }
            }
            result = NO;
        }
    }
    NSLog(@"changeCharactersInRange %@", NSStringFromRange(range));
    NSLog(@"replacementStrin %@",string);
    return result;
}

@end
