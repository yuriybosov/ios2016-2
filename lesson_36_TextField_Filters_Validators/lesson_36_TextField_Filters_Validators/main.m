//
//  main.m
//  lesson_36_TextField_Filters_Validators
//
//  Created by Yurii Bosov on 12/8/16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
