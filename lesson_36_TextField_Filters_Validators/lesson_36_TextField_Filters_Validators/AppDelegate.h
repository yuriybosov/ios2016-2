//
//  AppDelegate.h
//  lesson_36_TextField_Filters_Validators
//
//  Created by Yurii Bosov on 12/8/16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

