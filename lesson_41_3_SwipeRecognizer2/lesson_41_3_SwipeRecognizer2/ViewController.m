//
//  ViewController.m
//  lesson_41_3_SwipeRecognizer2
//
//  Created by Yurii Bosov on 12/25/16.
//  Copyright © 2016 ios. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {
    IBOutlet UIView *rect;
}

@end

@implementation ViewController

- (IBAction)didSwipe:(UISwipeGestureRecognizer *)sender {
    
    CGPoint point = rect.center;
    
    if (sender.direction == UISwipeGestureRecognizerDirectionUp){
    
        point.y = rect.frame.size.height/2 + 20;
        
    } else if (sender.direction == UISwipeGestureRecognizerDirectionDown){
        
        point.y = self.view.frame.size.height - rect.frame.size.height/2;
        
    } else if (sender.direction == UISwipeGestureRecognizerDirectionRight){
        
        point.x = self.view.frame.size.width - rect.frame.size.width/2;
        
    } else {
        
        point.x = rect.frame.size.width/2;
    }
    
    [UIView animateWithDuration:2 animations:^{
        rect.center = point;
    }];
    
//    rect.center = point;
}

@end
