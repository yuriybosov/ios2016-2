//
//  ViewController.m
//  lesson_43_UIFonts
//
//  Created by Yurii Bosov on 1/12/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {
    IBOutlet UILabel *lb1;
    IBOutlet UILabel *lb2;
    IBOutlet UILabel *lb3;
    IBOutlet UILabel *lb4;
    IBOutlet UILabel *lb5;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // set font
    // 1. системный шрифт
    lb2.font = [UIFont systemFontOfSize:18];
    lb2.font = [UIFont italicSystemFontOfSize:18];
    lb2.font = [UIFont boldSystemFontOfSize:22];
    
    // 2. любой шрифт, который есть в системе (используем font-fameli, смотри сайт http://iosfonts.com/)
    lb3.font = [UIFont fontWithName:@"Baskerville-Bold" size:12];
    
    // 3.1 кастомный шрифт (ttf или otf)
    lb4.font = [UIFont fontWithName:@"Pacifico" size:28];
    
    // 3.2 кастомный шрифт (ttf или otf)
    lb5.font = [UIFont fontWithName:@"RofiTaste" size:15];
    
    // set text
    lb1.text = @"Called after the view has been loaded. For view controllers created in code, this is after -loadView";
    lb2.text = @"Called after the view has been loaded. For view controllers created in code, this is after -loadView";
    lb3.text = @"Called after the view has been loaded. For view controllers created in code, this is after -loadView";
    lb4.text = @"Called after the view has been loaded. For view controllers created in code, this is after -loadView.";
    lb5.text = @"Called after the view has been loaded. For view controllers created in code, this is after -loadView.";
}

@end
