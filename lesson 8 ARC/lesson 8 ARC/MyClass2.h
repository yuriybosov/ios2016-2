//
//  MyClass2.h
//  lesson 8 ARC
//
//  Created by Yuriy Bosov on 8/28/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MyClass;

@interface MyClass2 : NSObject

@property (nonatomic, strong) MyClass *pr1;

@end
