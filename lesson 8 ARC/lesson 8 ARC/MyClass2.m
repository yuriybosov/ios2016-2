//
//  MyClass2.m
//  lesson 8 ARC
//
//  Created by Yuriy Bosov on 8/28/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "MyClass2.h"
#import "MyClass.h"

@implementation MyClass2

- (void)dealloc {
    NSLog(@"%@ dealloc", NSStringFromClass([self class]));
}

@end
