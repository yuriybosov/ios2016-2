//
//  MyClass.h
//  lesson 8 ARC
//
//  Created by Yuriy Bosov on 8/28/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MyClass2;

@interface MyClass : NSObject

@property (nonatomic, weak) MyClass2 *pr1;

@end
