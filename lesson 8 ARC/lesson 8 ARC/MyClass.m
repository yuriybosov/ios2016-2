//
//  MyClass.m
//  lesson 8 ARC
//
//  Created by Yuriy Bosov on 8/28/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "MyClass.h"
#import "MyClass2.h"

@implementation MyClass

- (void)dealloc {
    NSLog(@"%@ dealloc", NSStringFromClass([self class]));
}

@end
