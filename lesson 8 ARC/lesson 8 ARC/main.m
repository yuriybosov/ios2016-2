//
//  main.m
//  lesson 8 ARC
//
//  Created by Yuriy Bosov on 8/28/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyClass.h"
#import "MyClass2.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        MyClass *obj1 = [MyClass new];
        MyClass2 *obj2 = [MyClass2 new];
        
        obj1.pr1 = obj2;
        obj2.pr1 = obj1;
        
        NSLog(@"%@", obj1);
        NSLog(@"%@", obj2);
    }
    return 0;
}
