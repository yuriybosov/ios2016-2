//
//  ModelCell.swift
//  lesson_69_Swift_TableController
//
//  Created by Yurii Bosov on 6/30/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

import UIKit

class ModelCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var text1Label: UILabel?
    
    private var _model: Model?
    var model: Model? {
        set{
            self._model = newValue
            self.titleLabel?.text = self._model?.title
            self.text1Label?.text = self._model?.text1
        }
        get{
            return self._model
        }}
}
