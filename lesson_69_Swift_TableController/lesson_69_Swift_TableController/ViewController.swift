//
//  ViewController.swift
//  lesson_69_Swift_TableController
//
//  Created by Yurii Bosov on 6/30/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var dataSource = [Model]()
    
    @IBOutlet weak var tableView: UITableView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource.append(contentsOf: Model.testData())
        self.tableView?.reloadData()
    }
    
    //MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let controller = segue.destination as? ModelDetailedController else {
            return
        }
        
        guard let cell = sender as? ModelCell else {
            return
        }
        
        controller.model = cell.model        
    }
}

extension ViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellID", for: indexPath)

        if let modelCell = cell as? ModelCell {
            modelCell.model = dataSource[indexPath.row]
        }
        
        return cell
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
}

