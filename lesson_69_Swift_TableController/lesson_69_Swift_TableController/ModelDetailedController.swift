//
//  ModelDetailedController.swift
//  lesson_69_Swift_TableController
//
//  Created by Yurii Bosov on 6/30/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

import UIKit

class ModelDetailedController: UIViewController {

    var rootStackView = UIStackView()
    var text1 = UILabel()
    var text2 = UILabel()
    var text3 = UILabel()
    
    var model: Model?
    
    override func loadView() {
        self.view = UIScrollView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = model?.title
        self.view.backgroundColor = .white
        self.view.addSubview(rootStackView)
        // top
        let top = NSLayoutConstraint(item: rootStackView, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1, constant: 0)
        
        let left = NSLayoutConstraint(item: rootStackView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0)
        
        let right = NSLayoutConstraint(item: rootStackView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0)
        
        let bottom = NSLayoutConstraint(item: rootStackView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottomMargin, multiplier: 1, constant: 0)
        
        let center = NSLayoutConstraint(item: rootStackView, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerXWithinMargins, multiplier: 1, constant: 0)
        
        top.isActive = true
        left.isActive = true
        right.isActive = true
        bottom.isActive = true
        center.isActive = true
        
        //
        rootStackView.translatesAutoresizingMaskIntoConstraints = false
        rootStackView.isLayoutMarginsRelativeArrangement = true
        rootStackView.layoutMargins = UIEdgeInsets.init(top: 20, left: 20, bottom: 20, right: 20)
        rootStackView.spacing = 20
        rootStackView.axis = .vertical
        rootStackView.alignment = .fill
        rootStackView.distribution = .fill
        
        //
        text1.backgroundColor = .red
        text2.backgroundColor = .yellow
        text3.backgroundColor = .green
        
        text1.numberOfLines = 0
        text2.numberOfLines = 0
        text3.numberOfLines = 0
        
        var labels = [UILabel]()
        
        if model?.text1 != nil {
            text1.text = model?.text1
            labels.append(text1)
        }
        
        if model?.text2 != nil {
            text2.text = model?.text2
            labels.append(text2)
        }
        
        if !labels.isEmpty {
            let stackView = UIStackView(arrangedSubviews: labels)
            stackView.axis = .horizontal
            stackView.spacing = 20
            stackView.alignment = .top
            stackView.distribution = .fillEqually
            rootStackView.addArrangedSubview(stackView)
        }
        
        if model?.text3 != nil {
            text3.text = model?.text3
            rootStackView.addArrangedSubview(text3)
        }
    }
}
