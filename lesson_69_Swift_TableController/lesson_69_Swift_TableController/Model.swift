//
//  Model.swift
//  lesson_69_Swift_TableController
//
//  Created by Yurii Bosov on 6/30/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

import UIKit

class Model: NSObject {
    
    let title: String
    let text1: String?
    let text2: String?
    let text3: String?

    init(title:String,
         text1: String?,
         text2: String?,
         text3: String?) {
        
        self.title = title
        self.text1 = text1
        self.text2 = text2
        self.text3 = text3
    }
    
    static func testData() -> [Model] {
        var array = [Model]()
        
        array.append(Model(title: "model 1 title",
                           text1: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet,",
                           text2: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.",
                           text3: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium"))
        
        array.append(Model(title: "model 2 title title title title title title title title title title title title title title title title title title title title title title title title title title title title",
                           text1: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.",
                           text2: nil,
                           text3: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium"))
        array.append(Model(title: "model 2 title title title title title title title title",
                           text1: nil,
                           text2: nil,
                           text3: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium"))
        
        return array
    }
}
