//
//  main.m
//  lesson 7_2 mutablearray_example
//
//  Created by Yuriy Bosov on 8/25/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Group.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        Group *iOSGrpoup = [[Group alloc] init];
        iOSGrpoup.name = @"iOS group 2016";
        
        Student *st1 = [Student studentWithName:@"Vasya" age:20 gender:GenderMale];
        Student *st2 = [Student studentWithName:@"Petya" age:32 gender:GenderMale];
        Student *st3 = [Student studentWithName:@"Misha" age:32 gender:GenderMale];
        
        [iOSGrpoup addStudent:st1];
        [iOSGrpoup addStudent:st2];
        [iOSGrpoup addStudent:st3];
        
        NSLog(@"%@", iOSGrpoup);
        
        NSLog(@"searchStudentsByGender %@", [iOSGrpoup searchStudentsByGender:GenderMale]);
        
        NSLog(@"searchStudentsByAgeRage %@", [iOSGrpoup searchStudentsByAgeRage:NSMakeRange(25, 5)]);
        
        NSLog(@"searchStudentsByPrefixName %@", [iOSGrpoup searchStudentsByPrefixName:@"a"]);
        
        NSLog(@"searchStudentsByStringName %@", [iOSGrpoup searchStudentsByStringName:@"Mi"]);
        
        // добавление оценок
        [st1 addMark:10 forSubject:@"Математика"];
        [st1 addMark:5 forSubject:@"Математика"];
        [st1 addMark:1 forSubject:@"Математика"];
        
        [st1 addMark:10 forSubject:@"Физика"];
        [st1 addMark:10 forSubject:@"Литература"];
        
        [st2 addMark:12 forSubject:@"Физика"];
        [st2 addMark:4 forSubject:@"Литература"];
        
        [st1 showLogbook];
        [st2 showLogbook];
        
        [iOSGrpoup showStudentsSortedByAZ];
        [iOSGrpoup showStudentsSortedByAge];
    }
    return 0;
}
