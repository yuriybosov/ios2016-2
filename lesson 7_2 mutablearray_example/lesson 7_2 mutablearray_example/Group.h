//
//  Group.h
//  lesson 7_2 mutablearray_example
//
//  Created by Yuriy Bosov on 8/25/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Student.h"

@interface Group : NSObject

@property (nonatomic, strong) NSString *name;

- (void)addStudent:(Student *)student;
- (void)removeStudent:(Student *)student;

- (void)showStudentsSortedByAZ;
- (void)showStudentsSortedByAge;

- (NSArray *)searchStudentsByGender:(Gender)gender;
- (NSArray *)searchStudentsByAgeRage:(NSRange)range;
- (NSArray *)searchStudentsByPrefixName:(NSString *)prefix;
- (NSArray *)searchStudentsByStringName:(NSString *)string;

@end
