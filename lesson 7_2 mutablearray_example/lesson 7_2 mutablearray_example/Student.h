
#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    GenderNone,
    GenderMale,
    GenderFemale,
} Gender;

@interface Student : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) NSUInteger age;
@property (nonatomic, assign) Gender gender;

+ (Student *)studentWithName:(NSString *)name
                         age:(NSUInteger)age
                      gender:(Gender)gender;

// метод добавления оценки mark по предмету subject
- (void)addMark:(NSUInteger)mark
     forSubject:(NSString *)subject;

- (void)showLogbook;

@end
