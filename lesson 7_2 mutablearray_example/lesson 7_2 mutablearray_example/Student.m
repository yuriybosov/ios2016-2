

#import "Student.h"

@interface Student ()
{
    NSMutableDictionary *logbook;
}

@end


@implementation Student

+ (Student *)studentWithName:(NSString *)name
                         age:(NSUInteger)age
                      gender:(Gender)gender
{
    Student *student = [[Student alloc] init];
    
    student.name = name;
    student.age = age;
    student.gender = gender;
    
    return student;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        logbook = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"name = %@, age = %lu, gender = %li", _name, _age, _gender];
}

- (void)addMark:(NSUInteger)mark forSubject:(NSString *)subject {
    
//    if (!logbook[subject])
//        logbook[subject] = [NSMutableArray new];
//    [logbook[subject] addObject:@(mark)];
    
    NSMutableArray *marksArray = [logbook objectForKey:subject];
    
    // если оценок нет по предмету, то массив будет равен nill
    if (marksArray == nil) {
        marksArray = [[NSMutableArray alloc] init];
        [logbook setObject:marksArray forKey:subject];
    }
    
    [marksArray addObject:@(mark)];
}

- (void)showLogbook {
    
    for (NSString *key in [logbook allKeys]) {
        
        NSMutableArray *array = [logbook objectForKey:key];
        NSLog(@"%@: %@", key, [array componentsJoinedByString:@","]);
    }
}

@end
