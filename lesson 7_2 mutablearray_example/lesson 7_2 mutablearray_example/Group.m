//
//  Group.m
//  lesson 7_2 mutablearray_example
//
//  Created by Yuriy Bosov on 8/25/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "Group.h"

@interface Group ()
{
    NSMutableArray *students;
}

@end

@implementation Group

- (instancetype)init
{
    self = [super init];
    if (self) {
        students = [[NSMutableArray alloc] init];
    }
    return self;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"group name = %@", _name];
}

- (void)addStudent:(Student *)student {
    [students addObject:student];
}

- (void)removeStudent:(Student *)student {
    [students removeObject:student];
}

- (NSArray *)searchStudentsByGender:(Gender)gender {
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    for (Student *st in students) {
        if (st.gender == gender) {
            [result addObject:st];
        }
    }
    
    return result;
}

- (NSArray *)searchStudentsByAgeRage:(NSRange)range {
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    for (Student *st in students) {
        if (st.age >= range.location &&
            st.age <= range.location + range.length) {
            [result addObject:st];
        }
    }
    
    return result;
}

- (NSArray *)searchStudentsByPrefixName:(NSString *)prefix {
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    for (Student *st in students) {
        if ([[st.name lowercaseString] hasPrefix:prefix]) {
            [result addObject:st];
        }
    }
    
    return result;
}

- (NSArray *)searchStudentsByStringName:(NSString *)string {
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    NSRange range;
    
    for (Student *st in students) {
        
        range = [st.name rangeOfString:string
                               options:NSCaseInsensitiveSearch];
        
        if (range.location != NSNotFound) {
            [result addObject:st];
        }
    }
    
    return result;
}

- (void)showStudentsSortedByAZ {
    
    //NSSortDescriptor - описывает условие сортировки
    NSSortDescriptor *sortDescriptorByName = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    
    // sortedArray - отсортированный массив
    NSArray *sortedArray = [students sortedArrayUsingDescriptors:@[sortDescriptorByName]];
    
    NSLog(@"sorted by name %@", sortedArray);
}

- (void)showStudentsSortedByAge {
    
    NSSortDescriptor *sortDescriptorByAge = [NSSortDescriptor sortDescriptorWithKey:@"age" ascending:YES];
    NSSortDescriptor *sortDescriptorByName = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    
    // sortedArray - отсортированный массив
    NSArray *sortedArray = [students sortedArrayUsingDescriptors:@[sortDescriptorByAge, sortDescriptorByName]];
    
    NSLog(@"sorted by age %@", sortedArray);
}


@end
