//
//  Exchangerate.h
//  lesson_50_HTTP_Request
//
//  Created by Yurii Bosov on 3/5/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Exchangerate : NSObject

@property (nonatomic, strong) NSString *ccy;
@property (nonatomic, strong) NSString *baseCcy;
@property (nonatomic, strong) NSString *buy;
@property (nonatomic, strong) NSString *sale;

@end
