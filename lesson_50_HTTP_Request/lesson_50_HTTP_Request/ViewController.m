//
//  ViewController.m
//  lesson_50_HTTP_Request
//
//  Created by Yurii Bosov on 3/5/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"
#import "Exchangerate.h"

#define BASE_URL @"https://api.privatbank.ua"
#define PATH @"p24api/pubinfo"

@interface ViewController () <UITableViewDataSource> {
    IBOutlet UITableView *myTableView;
    IBOutlet UIActivityIndicatorView *activityIndicator;
    
    NSMutableArray *dataSources;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Курсы валют :(";
    dataSources = [NSMutableArray new];
    
    // "запустить крутилочку :)"
    [activityIndicator startAnimating];
    
    
    // 1. создаем урл строку
    NSString *urlStr = [NSString stringWithFormat:@"%@/%@?json&exchange&coursid=%i", BASE_URL, PATH, 5];
    // 2. создаем урл на основе урл строки
    NSURL *url = [NSURL URLWithString:urlStr];
    // 3. создаем реквест на основе урл
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    // 4. создаем сессию
    NSURLSession *session = [NSURLSession sharedSession];
    // 5. создаем таску, указывая реквест и указывая комплишен блок
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        // этот блок вызовется сразу же после получения ответа от сервера
        
        // "остановить крутилочку :)", выполняем в главном потоке
        dispatch_async(dispatch_get_main_queue(), ^{
            [activityIndicator stopAnimating];
        });
        
        // если пришла data значит нужно ее распарсить
        if (data.length) {
            
            NSError *error = nil;
            id object = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
            if (error) {
                // заменить на Alert
                NSLog(@"не смогли распарсить JSON");
            } else {
                // создать модели, выполнить reload tableview
                NSLog(@"%@",object);
                
                if ([object isKindOfClass:[NSArray class]]) {
                    for (NSDictionary *modelData in object) {
                        if ([modelData isKindOfClass:[NSDictionary class]]) {
                            Exchangerate *model = [Exchangerate new];
                            model.ccy = modelData[@"ccy"];
                            model.baseCcy = modelData[@"base_ccy"];
                            model.sale = modelData[@"sale"];
                            model.buy = modelData[@"buy"];
                            
                            [dataSources addObject:model];
                        }
                    }
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (dataSources.count == 0) {
                        // показать alert что данные отсутствуют
                    }
                    [myTableView reloadData];
                });
            }
            
        } else if (error){
            // заменить на Alert
            NSLog(@"%@", error.localizedDescription);
        } else {
            // заменить на Alert
            NSLog(@"Все плохо!!!");
        }
    }];
    
    // 6. выполнить таску
    [task resume];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataSources.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID"];
    
    Exchangerate *model = dataSources[indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"%@ / %@", model.baseCcy, model.ccy];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"покупка %@, продажа %@", model.buy, model.sale];
    
    return cell;
}

#pragma mark - Alert

- (void)showAlertWithTitle:(NSString *)title message:(NSString *)
message {
    dispatch_async(dispatch_get_main_queue(), ^{
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil]];
        
        [self presentViewController:alert animated:YES completion:nil];
    });
}

@end
