//
//  MyView.m
//  lesson_44_CoreGraphics
//
//  Created by Yurii Bosov on 1/15/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "MyView.h"

@implementation MyView


// создавать графические примитивы (рисовать что либо) можно только в этой функии
// 1. drawRect: вызывается системой в определенный случаях. Вызывать этот метод в коде безсмысленно
// 2. если есть необходимость выполнить перерисовку (т.е. вызвать метод drawRect:), то нужно вызвать метод setNeedsDisplay или setNeedsDisplayInRect: ()
- (void)drawRect:(CGRect)rect {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // для отрисовки будем использовать контекст
    // 1. создали контекст
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // 2. настраиваем контекст
    // 2.1 задаем цвет заливки
    CGContextSetRGBFillColor(context, 1, 0, 0, 1);
    
    // 2.2 задаем цвет контура
    CGContextSetRGBStrokeColor(context, 0, 1, 0, 1);
    
    // 2.3 толщина контура
    CGContextSetLineWidth(context, 4);
    
    // 2.4 прозрачность (от 0 до 1)
    CGContextSetAlpha(context, 1);
    
    // 3.1 квадрат с заливкой
    CGContextFillRect(context, CGRectMake(10, 10, 50, 50));
    
    // 3.2 квадрат с контуром
    CGContextStrokeRect(context, CGRectMake(70, 10, 50, 50));
    
    // 3.3. квадрат с заливкой и контуром
    CGContextFillRect(context, CGRectMake(130, 10, 50, 50));
    CGContextStrokeRect(context, CGRectMake(130, 10, 50, 50));
    
    // 4.1 круг (елипс) с заливкой
    CGContextFillEllipseInRect(context, CGRectMake(10, 70, 50, 50));
    
    // 4.2 круг (елипс) с контуром
    CGContextStrokeEllipseInRect(context, CGRectMake(70, 70, 50, 50));
    
    // 4.3. круг (елипс) с заливкой и контуром
    CGContextFillEllipseInRect(context, CGRectMake(130, 70, 50, 50));
    CGContextStrokeEllipseInRect(context, CGRectMake(130, 70, 50, 50));
    
    // 5. линия
    // для отрисовки линии нужно создать массив из 2-х точек
    CGPoint pointsToLine[2] = {CGPointMake(10, 130), CGPointMake(185, 130)};
    CGContextStrokeLineSegments(context, pointsToLine, 2);
    
    // 6. треугольник (многоугольник)
    // 6.1 треугольник c контуром
    CGPoint pointsToTriangle[6] = {CGPointMake(10, 200), CGPointMake(35, 150), CGPointMake(35, 150), CGPointMake(60, 200), CGPointMake(10, 200), CGPointMake(60, 200)};
    CGContextAddLines(context, pointsToTriangle, 6);
    CGContextClosePath(context);
    CGContextStrokePath(context);
    
    // 6.2 треугольник c заливкой
    CGPoint pointsToTriangle2[6] = {CGPointMake(110, 200), CGPointMake(135, 150), CGPointMake(135, 150), CGPointMake(160, 200), CGPointMake(110, 200), CGPointMake(160, 200)};
    CGContextAddLines(context, pointsToTriangle2, 6);
    CGContextFillPath(context);
    
    // 7. сплайны
    // Квадратичная кривая Безье, см википедию
    CGPoint start = CGPointMake(10, 300);
    CGPoint end = CGPointMake(310, 300);
    CGPoint help = CGPointMake(160, 220);
    
    CGContextBeginPath(context);
    CGContextMoveToPoint(context, start.x, start.y);
    CGContextAddQuadCurveToPoint(context, help.x, help.y, end.x, end.y);
    CGContextClosePath(context);
    CGContextDrawPath(context, kCGPathFillStroke);
    
    // Кубическая кривая Безье
    CGPoint start2 = CGPointMake(10, 400);
    CGPoint end2 = CGPointMake(310, 400);
    CGPoint help1 = CGPointMake(110, 300);
    CGPoint help2 = CGPointMake(210, 500);
    
    CGContextBeginPath(context);
    CGContextMoveToPoint(context, start2.x, start2.y);
    CGContextAddCurveToPoint(context, help1.x, help1.y, help2.x, help2.y, end2.x, end2.y);
    CGContextDrawPath(context, kCGPathStroke);
}

@end
