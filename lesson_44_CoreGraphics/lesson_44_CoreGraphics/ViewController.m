//
//  ViewController.m
//  lesson_44_CoreGraphics
//
//  Created by Yurii Bosov on 1/15/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (IBAction)drawViewButtonCliecked:(id)sender {
    //
    [self.view setNeedsDisplay];
}

@end
