//
//  CustomClass.m
//  lesson 13_Protocol
//
//  Created by Yuriy Bosov on 9/11/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "CustomClass.h"

@implementation CustomClass

- (void)method1 {
    NSLog(@"method1");
}

#pragma mark - CustomProtocol

- (void)protocolMethod1 {
    // NSStringFromSelector(_cmd) - метод вернет название метода (селектора), в котором она написана
    NSLog(@"%@", NSStringFromSelector(_cmd));
}

- (NSInteger)protocolMethod2 {
    NSLog(@"%@", NSStringFromSelector(_cmd));
    return 10;
}

- (BOOL)protocolMethod3:(NSInteger)value {
    NSLog(@"%@", NSStringFromSelector(_cmd));
    return (value % 2 == 0);
}

- (void)protocolOptionalMethod1 {
    NSLog(@"%@", NSStringFromSelector(_cmd));
}

@end
