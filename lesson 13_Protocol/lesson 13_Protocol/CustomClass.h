//
//  CustomClass.h
//  lesson 13_Protocol
//
//  Created by Yuriy Bosov on 9/11/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CustomProtocol.h"

@interface CustomClass : NSObject <CustomProtocol>

- (void)method1;

@end
