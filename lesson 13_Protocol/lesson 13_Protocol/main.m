//
//  main.m
//  lesson 13_Protocol
//
//  Created by Yuriy Bosov on 9/11/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "CustomClass.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        CustomClass *obj = [[CustomClass alloc] init];
        
        // проверка на реализацию протокола
        if ([obj conformsToProtocol:@protocol(CustomProtocol)])
        {
            NSLog(@"класс %@ реализует протокол %@", NSStringFromClass([obj class]), NSStringFromProtocol(@protocol(CustomProtocol)));
        }
        
        [obj protocolMethod1];
        [obj protocolMethod2];
        [obj protocolMethod3:10];
        
        // для опциональных методов протокола лучше делать проверку на реализацию!!!
        if ([obj respondsToSelector:@selector(protocolOptionalMethod1)]) {
            [obj protocolOptionalMethod1];
        }
        //
        if  ([obj respondsToSelector:@selector(protocolOptionalMethod2)]){
            [obj protocolOptionalMethod2];
        }
    }
    return 0;
}
