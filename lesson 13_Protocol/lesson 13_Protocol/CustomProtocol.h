//
//  CustomProtocol.h
//  lesson 13_Protocol
//
//  Created by Yuriy Bosov on 9/11/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#ifndef CustomProtocol_h
#define CustomProtocol_h

#import <Foundation/Foundation.h>

// обьявление поротокола

@protocol CustomProtocol <NSObject>

@required // через @required обявляются обязательные методы протокола, по умолчанию все методы required
- (void)protocolMethod1;
- (NSInteger)protocolMethod2;
- (BOOL)protocolMethod3:(NSInteger)value;


@optional // через @optional объявляются не обязательные методы протокола. т.е. класс, который реализует данный протокол может не реализовывать эти методы
- (void)protocolOptionalMethod1;
- (void)protocolOptionalMethod2;

@end

#endif /* CustomProtocol_h */
