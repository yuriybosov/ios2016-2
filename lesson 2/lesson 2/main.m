//
//  main.m
//  lesson 2
//
//  Created by Yuriy Bosov on 7/10/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        // однострочный комент
        
        /*
         многострочный комент
         */
        
        // NSInteger - хранит целые числа
        NSInteger i = 30;
        
        // NSUInteger - беззнаковый тип, хранит целые положительные числа (от нуля и до ++++)
        NSUInteger a = 10;
        
        // CGFloat - хранит числа с дробной частью
        CGFloat f = 40.15;
        
        // BOOL - логический тип, принимает значениея YES \ NO
        BOOL b = YES; // NO
        
        // вывод в консоль
        NSLog(@"int i = %li", i);
        NSLog(@"uint u = %lu", a);
        NSLog(@"float f = %f", f);
        NSLog(@"bool b = %i", b);
        
        // ограничение кол-во символов после запятой при выводе float - между '%' и 'f' указываем число 0.2, тем самым ограничивая кол-во символов до 2-х
        NSLog(@"float f = %0.1f", f);
        NSLog(@"float f = %0.2f", f);
        NSLog(@"float f = %0.3f", f);
        
        // операторы
        // сложение
        NSInteger sum = i + a;
        NSLog(@"sum = %li", sum);
        
        // вычитание
        NSInteger min = a - i;
        NSLog(@"min = %li", min);
        
        // умножение
        CGFloat result = f * a;
        NSLog(@"result = %f", result);
        
        // деление
        result = f / a;
        NSLog(@"result = %f", result);
        
        // остаток от деления (пример: берем остаток от деления на 2, если остаок равен 1 - то число не четное, если равен 0 - то чисто четное)
        NSInteger ost = i % 2;
        NSLog(@"ost = %li", ost);
        
        // ++ - инкремент, увеличение на единицу
        i++;
        NSLog(@"i = %li", i);
        
        // -- - декремент, уменьшение на единицу
        i--;
        NSLog(@"i = %li", i);
        
        // сложение с присвоением
        i += a;
        NSLog(@"i = %li", i);
        
        // вычитание с присвоением
        i -= a;
        NSLog(@"i = %li", i);
        
        // извлечение корня
        result = sqrtf(f);
        NSLog(@"извлечение корня = %f", result);
        
        // возведение в степень, где 2 - степень
        result = powf(f,2);
        NSLog(@"возведение в степень = %", result);
    }
    return 0;
}
