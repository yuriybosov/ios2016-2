//
//  AppDelegate.h
//  lesson_57_UserLocation
//
//  Created by Yurii Bosov on 5/25/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

