//
//  ViewController.m
//  lesson_57_UserLocation
//
//  Created by Yurii Bosov on 5/25/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"
#import <CoreLocation/CoreLocation.h>

@interface ViewController () <CLLocationManagerDelegate> {
    IBOutlet UILabel *latitideLabel;
    IBOutlet UILabel *longitudeLabel;
    IBOutlet UILabel *speedLabel;
    
    CLLocationManager *locationManager;
}

@end

@implementation ViewController

#pragma mark - Actions
    
- (IBAction)myLocationButtonClicked:(id)sender {
    
    if (!locationManager) {
        locationManager = [CLLocationManager new];
        locationManager.delegate = self;
    }
    
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    
    switch (status) {
        case kCLAuthorizationStatusNotDetermined:
            NSLog(@"доступ не был еще запрошен");
            [locationManager requestWhenInUseAuthorization];
            break;
            
        case kCLAuthorizationStatusDenied:
        case kCLAuthorizationStatusRestricted:
            NSLog(@"доступ к координатам не дан");
            break;
    
        case kCLAuthorizationStatusAuthorizedWhenInUse:
        case kCLAuthorizationStatusAuthorizedAlways:
            NSLog(@"доступ разрешен");
            [locationManager startUpdatingLocation];
            break;
    }
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    
    CLLocation *location = locations.firstObject;
    latitideLabel.text = [NSString stringWithFormat:@"%f", location.coordinate.latitude];
    longitudeLabel.text = [NSString stringWithFormat:@"%f", location.coordinate.longitude];
    speedLabel.text = [NSString stringWithFormat:@"%f",location.speed];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    
    // прекращаем трекать локацию
    [locationManager stopUpdatingLocation];
    NSLog(@"%@", error);
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    
    if (status == kCLAuthorizationStatusAuthorizedAlways ||
        status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        [locationManager startUpdatingLocation];
    }
}
    
@end
