
//
//  Auto.m
//  lesson 4_class_auto
//
//  Created by Yuriy Bosov on 7/17/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "Auto.h"

@implementation Auto

- (NSString *)description
{
    return [NSString stringWithFormat:@"brand = %@, model = %@, mileage = %lu, reg.data = %@", _brand, _model, _mileage, [_regData description]];
}

- (BOOL)isCorrectRegistrationData {
    
    BOOL result = _regData.numberAuto && _regData.ownerName && _regData.placeRegistration && _regData.dateRegistration;
    
    return result;
}

@end
