//
//  RegData.m
//  lesson 4_class_auto
//
//  Created by Yuriy Bosov on 7/17/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "RegData.h"

@implementation RegData

+ (RegData *)regDataWithNumber:(NSString *)numberAuto
                     ownerName:(NSString *)ownerName
                          date:(NSString *)dateRegistration
                         place:(NSString *)placeRegistration {
    
    RegData *data = [[RegData alloc] init];
    
    data.numberAuto = numberAuto;
    data.ownerName = ownerName;
    data.dateRegistration = dateRegistration;
    data.placeRegistration = placeRegistration;
    
    return data;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"number = %@, owner = %@, date = %@, place = %@", _numberAuto, _ownerName, _dateRegistration, _placeRegistration];
}

@end
