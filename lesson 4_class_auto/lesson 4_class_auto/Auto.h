//
//  Auto.h
//  lesson 4_class_auto
//
//  Created by Yuriy Bosov on 7/17/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RegData.h"

@interface Auto : NSObject

@property (nonatomic, strong) NSString *brand;
@property (nonatomic, strong) NSString *model;
@property (nonatomic, assign) NSUInteger mileage; // пробег
@property (nonatomic, strong) RegData *regData;

- (BOOL)isCorrectRegistrationData;

@end
