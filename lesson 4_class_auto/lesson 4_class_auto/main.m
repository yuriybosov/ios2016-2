//
//  main.m
//  lesson 4_class_auto
//
//  Created by Yuriy Bosov on 7/17/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Auto.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        // auto
        Auto *auto1 = [[Auto alloc] init];
        auto1.brand = @"BMW";
        auto1.model = @"X5";
        auto1.mileage = 1000;
        
        // reg data
        RegData *regData = [RegData regDataWithNumber:@"AE 11-12 ED"
                                            ownerName:@"Yuriy"
                                                 date:@"2016.01.01"
                                                place:@"Dnepr"];
        
        //
        auto1.regData = regData;
        NSLog(@"Auto1 = %@", auto1);
        
        if ([auto1 isCorrectRegistrationData]) {
            NSLog(@"регистрационные данные корректны");
        } else {
            NSLog(@"регистрационные данные НЕ корректны");
        }
        
    }
    return 0;
}
