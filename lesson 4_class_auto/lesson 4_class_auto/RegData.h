//
//  RegData.h
//  lesson 4_class_auto
//
//  Created by Yuriy Bosov on 7/17/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RegData : NSObject

@property (nonatomic, strong) NSString *numberAuto;
@property (nonatomic, strong) NSString *ownerName;
@property (nonatomic, strong) NSString *dateRegistration;
@property (nonatomic, strong) NSString *placeRegistration;

+ (RegData *)regDataWithNumber:(NSString *)numberAuto
                     ownerName:(NSString *)ownerName
                          date:(NSString *)dateRegistration
                         place:(NSString *)placeRegistration;

@end
