//
//  ViewController.swift
//  lesson_72_Widget
//
//  Created by Yurii Bosov on 7/9/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

import UIKit
import API

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // call get random joke
        APIProvider.sharedInstance.getRandomJoke(1) { (data, error) in
            if let d = data {
                print("\(d)")
            }
            if let e = error {
                print("\(e)")
            }
        }
    }
}
