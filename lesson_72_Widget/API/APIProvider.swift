//
//  APIProvider.swift
//  lesson_72_Widget
//
//  Created by Yurii Bosov on 7/9/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

import UIKit

public class APIProvider: NSObject {
    public static let sharedInstance = APIProvider()
    
    public func getRandomJoke(_ count: Int, complition: @escaping( _ responce: [Model]?, _ error: String?) -> Void) {
     
        let urlString = "http://umorili.herokuapp.com/api/random?num=\(count)"
        if let url = URL(string: urlString) {
            
            let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, responce, error) in
                
                if data != nil {
                    do {
                        if let jokesData = try JSONSerialization.jsonObject(with: data!, options: []) as? [[String:Any]] {
                            
                            var models = [Model]()
                            for jokeData in jokesData {
                                models.append(Model(jokeData))
                            }
                            DispatchQueue.main.async {
                                complition(models, nil)
                            }
                            
                        } else {
                            DispatchQueue.main.async {
                                complition(nil, "invalid json")
                            }
                        }
                    } catch {
                        DispatchQueue.main.async {
                            complition(nil, "invalid json")
                        }
                    }
                } else {
                    DispatchQueue.main.async {
                        complition(nil, error?.localizedDescription ?? "ooops")
                    }
                }
            })
            task.resume()
            
        } else {
            complition(nil, "bad url")
        }
    }
}
