//
//  Model.swift
//  lesson_72_Widget
//
//  Created by Yurii Bosov on 7/9/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

import UIKit

public class Model: NSObject {
    
    public var site: String?
    public var name: String?
    public var desc: String?
    public var link: String?
    public var elementPureHtml: String?
    
    init(_ data:[String:Any]) {
        super.init()
        
        if let site = data["site"] as? String {
            self.site = site
        }
        if let name = data["name"] as? String {
            self.name = name
        }
        if let desc = data["desc"] as? String {
            self.desc = desc
        }
        if let link = data["link"] as? String {
            self.link = link
        }
        if let element = data["elementPureHtml"] as? String {
            self.elementPureHtml = element
        }
    }
}
