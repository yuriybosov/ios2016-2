//
//  TodayViewController.swift
//  Widget
//
//  Created by Yurii Bosov on 7/9/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

import UIKit
import NotificationCenter
import API

class TodayViewController: UIViewController, NCWidgetProviding {
    
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        self.extensionContext?.widgetLargestAvailableDisplayMode = .expanded
    }
    
    @IBAction func refreshButtonClicked() {
        fetshData()
    }
    
    private func fetshData() {
        
        self.textLabel.attributedText = nil
        self.indicatorView.startAnimating()
        
        APIProvider.sharedInstance.getRandomJoke(20) { [weak self] (data, error) in
            self?.indicatorView.stopAnimating()
            if let d = data {
                
                let randomIndex = Int(arc4random_uniform(UInt32(d.count - 1)))
                let str = d[randomIndex].elementPureHtml
                let data = str?.data(using:.utf8)
                let options: [String: Any] = [
                    NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                    NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue, NSFontAttributeName: UIFont.systemFont(ofSize: 16)
                ]
                
                guard let attributedString = try? NSAttributedString(data: data!, options: options, documentAttributes: nil) else {
                    return
                }
                
                self?.textLabel.attributedText = attributedString
                self?.resize()
            }
            if let e = error {
                self?.textLabel.text = e
            }
        }
    }
    
    func resize() {
        let size = self.view.systemLayoutSizeFitting(CGSize(width: self.view.frame.size.width, height: 110), withHorizontalFittingPriority: UILayoutPriorityRequired, verticalFittingPriority: UILayoutPriorityFittingSizeLevel)
        
        self.preferredContentSize = CGSize(width: size.width, height: size.height)
    }
 
    // MARK: NCWidgetProviding
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {
        completionHandler(NCUpdateResult.newData)
        fetshData()
    }
    
    func widgetActiveDisplayModeDidChange(_ activeDisplayMode: NCWidgetDisplayMode, withMaximumSize maxSize: CGSize) {
        if (activeDisplayMode == NCWidgetDisplayMode.compact) {
            self.preferredContentSize = CGSize(width: 0, height: 110)
        } else {
            
            let size = self.view.systemLayoutSizeFitting(CGSize(width: self.view.frame.size.width, height: 110), withHorizontalFittingPriority: UILayoutPriorityRequired, verticalFittingPriority: UILayoutPriorityFittingSizeLevel)
            
            self.preferredContentSize = CGSize(width: size.width, height: size.height)
        }
    }
}
