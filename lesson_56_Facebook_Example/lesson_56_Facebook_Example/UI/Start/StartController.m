//
//  StartController.m
//  lesson_56_Facebook_Example
//
//  Created by Yurii Bosov on 5/21/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "StartController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>

@implementation StartController

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    // проверка на залогиненого facebook юзера
    if ([FBSDKAccessToken currentAccessToken]) {
        // open tabbar
        [UIApplication sharedApplication].keyWindow.rootViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"tabbar"];
        
    } else {
        // open login
        [UIApplication sharedApplication].keyWindow.rootViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"loginNavigation"];
    }
}

@end
