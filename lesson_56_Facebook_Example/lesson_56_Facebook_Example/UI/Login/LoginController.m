//
//  LoginController.m
//  lesson_56_Facebook_Example
//
//  Created by Yurii Bosov on 5/21/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "LoginController.h"
#import <FBSDKLoginKit.h>
#import <MBProgressHUD/MBProgressHUD.h>

@implementation LoginController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Login";
}

#pragma mark - Actions

- (IBAction)loginButtonClicked:(id)sender {
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    
    [[FBSDKLoginManager new] logInWithReadPermissions:@[@"email",@"user_friends",@"public_profile"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        
        if (result.token) {
            // open tabbar
            [UIApplication sharedApplication].keyWindow.rootViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"tabbar"];

        } else if (error) {
            // show error
            hud.mode = MBProgressHUDModeText;
            hud.label.text = error.localizedDescription;
            [hud hideAnimated:YES afterDelay:3];
        } else {
            //
            [hud hideAnimated:YES];
        }
    }];
}

@end
