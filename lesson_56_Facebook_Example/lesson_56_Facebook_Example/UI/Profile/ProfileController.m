//
//  ProfileController.m
//  lesson_56_Facebook_Example
//
//  Created by Yurii Bosov on 5/21/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ProfileController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit.h>
#import <MBProgressHUD/MBProgressHUD.h>

@interface ProfileController ()

@end

@implementation ProfileController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Profile";
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    
    // get user profile info
    FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields":@"id,email,gender,first_name,last_name,picture.type(large)"}];
    
    [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
        if (error) {
            NSLog(@"error %@", error);
            hud.mode = MBProgressHUDModeText;
            hud.label.text = error.localizedDescription;
            hud.label.numberOfLines = 0;
            [hud hideAnimated:YES afterDelay:4];
            
        } else {
            [hud hideAnimated:YES];
            NSLog(@"result %@", result);
        }
    }];
}

#pragma mark - Actions

- (IBAction)logoutButtonClicked:(id)sender{
    
    [[FBSDKLoginManager new] logOut];
    
    [UIApplication sharedApplication].keyWindow.rootViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"loginNavigation"];
}

@end
