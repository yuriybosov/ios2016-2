//
//  FriednsController.m
//  lesson_56_Facebook_Example
//
//  Created by Yurii Bosov on 5/21/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "FriendsController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <MBProgressHUD/MBProgressHUD.h>

@interface FriendsController () {
    NSString *cursotAfter;
}

@end

@implementation FriendsController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Friends";
    
    // test
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setObject:@"id,name,picture.type(large)" forKey:@"fields"];
    if (cursotAfter.length > 0) {
        [params setObject:cursotAfter forKey:@"after"];
    }
    
    FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                  initWithGraphPath:@"me/taggable_friends"
                                  parameters:params
                                  HTTPMethod:@"GET"];
    [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                          id result,
                                          NSError *error) {
        if (error) {
            NSLog(@"error %@", error);
            hud.mode = MBProgressHUDModeText;
            hud.label.text = error.localizedDescription;
            hud.label.numberOfLines = 0;
            [hud hideAnimated:YES afterDelay:4];
            
        } else {
            [hud hideAnimated:YES];
            NSLog(@"result %@", result);
            
            cursotAfter = [result valueForKeyPath:@"paging.cursors.after"];
            NSLog(@"cursotAfter %@", cursotAfter);
        }
    }];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellID" forIndexPath:indexPath];
    return cell;
}

@end
