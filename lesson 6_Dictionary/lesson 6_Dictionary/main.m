//
//  main.m
//  lesson 6_Dictionary
//
//  Created by Yuriy Bosov on 7/24/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        // методы создание и инициализации
        NSDictionary *dict1 = [NSDictionary dictionaryWithObjectsAndKeys:@"value1",@"key1",@"value2",@"key2",@"value3",@"key3", nil];
        NSLog(@"dict1 = %@", dict1);
        
        NSDictionary *dict2 = [NSDictionary dictionaryWithObjects:@[@"valu1",@"valu2",@"valu3"] forKeys:@[@"key1",@"key2",@"key3"]];
        NSLog(@"dict2 = %@", dict2);
        
        NSDictionary *dict3 = [NSDictionary dictionaryWithObject:@"value" forKey:@"key"];
        NSLog(@"dict3 = %@", dict3);
        
        // сокращенный метод создания и инициализации словаря
        NSDictionary *dict4 = @{@"key1": @"valu1",
                                @"key2": @"value2",
                                @"key3": @"value3"};
        NSLog(@"dict4 = %@", dict4);
        
        // методы:
        // получить массив всех ключей
        NSArray *keysArray = [dict4 allKeys];
        
        // получить массив всех значений
        NSArray *values = [dict4 allValues];
        NSLog(@"keys = %@, values = %@", keysArray,values);
        
        // получение значение по ключу
        id value = [dict4 objectForKey:@"key1"];
        // этот же метод в сокращенной форме
        value = dict4[@"key1"];
        NSLog(@"value = %@", value);
        
        // пример, как "обойти" все элементы словаря с помощью цикла
        for(NSString *key in [dict4 allKeys]) {
            NSLog(@"key = %@, value = %@",key, [dict4 objectForKey:key]);
        }
        
            
    }
    return 0;
}
