//
//  ViewController.m
//  lesson_46_Layar_Mask
//
//  Created by Yurii Bosov on 1/22/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"
#import "MyView.h"

@interface ViewController () {
    IBOutlet UIScrollView *scrollView;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    MyView *view = nil;
    for (NSInteger i = 0; i < 10; i++) {
        
        view = [[MyView alloc] init];
        view.frame = CGRectMake(0,
                                i * (H + 2 - h) - h,
                                self.view.frame.size.width,
                                H);
        view.backgroundColor = [UIColor greenColor];
        view.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        view.tag = i;
        view.contentMode = UIViewContentModeRedraw;
        
        [scrollView addSubview:view];
    }
    
    scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, view.frame.origin.y + view.frame.size.height - h);
    
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTaped:)];
    [scrollView addGestureRecognizer:recognizer];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (void)didTaped:(UITapGestureRecognizer *)sender {

    CGPoint point = [sender locationInView:scrollView];
    for (MyView *view in scrollView.subviews) {
        if ([view isKindOfClass:[MyView class]]) {
            
            CGPoint convertPoint = [scrollView convertPoint:point toView:view];
            
            if ([view.path containsPoint:convertPoint]) {
                NSLog(@"%li", view.tag);
                break;
            }
        }
    }
}

@end
