//
//  MyView.h
//  lesson_46_Layar_Mask
//
//  Created by Yurii Bosov on 1/26/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyView : UIView

@property (nonatomic, readonly) UIBezierPath *path;

@end
