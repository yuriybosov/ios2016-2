//
//  MyView.m
//  lesson_46_Layar_Mask
//
//  Created by Yurii Bosov on 1/26/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "MyView.h"

@implementation MyView

- (void)drawRect:(CGRect)rect {
    // mask
    // 1. создаем UIBezierPath
    _path = [UIBezierPath bezierPath];
    [_path moveToPoint:CGPointMake(0,
                                  H)];
    [_path addLineToPoint:CGPointMake(0,
                                     h)];
    [_path addLineToPoint:CGPointMake(self.frame.size.width,
                                     0)];
    [_path addLineToPoint:CGPointMake(self.frame.size.width,
                                     H - h)];
    _path.lineJoinStyle = kCGLineJoinRound;
    [_path closePath];
    // 2. создаем layer
    CAShapeLayer *layer = [CAShapeLayer layer];
    layer.path = _path.CGPath;
    // 3. CAShapeLayer применяем как маску для леера view
    self.layer.mask = layer;
}

@end
