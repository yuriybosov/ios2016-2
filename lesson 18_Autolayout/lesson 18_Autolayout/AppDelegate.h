//
//  AppDelegate.h
//  lesson 18_Autolayout
//
//  Created by Yuriy Bosov on 9/29/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

