//
//  ViewController.m
//  lesson_38_UIAlertController
//
//  Created by Yurii Bosov on 12/15/16.
//  Copyright © 2016 ios. All rights reserved.
//

#import "ViewController.h"

void showAlert(NSString *title, NSString *message){
    
    // 1.
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    // 2.
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
    
    // 3.
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertController animated:YES completion:nil];
}


@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

#pragma mark - Actions

- (IBAction)showAlert:(id)sender {
    
    // 1. создаем alet controller использую статичесикй метод, в котором передаем заголовок, сообщение и стиль отображение алерта
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Предупреждение" message:@"Вы действительно хотите выполнить это действие" preferredStyle:UIAlertControllerStyleAlert];
    
    // 2. добавляем кнопки и обработчики на них. это можно выполнить использую UIAlerAction
    // 2.1. создаем UIAlertAction, указав заголовок (он будет отображен на кнопке) и добавляем(по желанию) обработчик нажатия на кнопку в виде handler-блока
    UIAlertAction *actionYES = [UIAlertAction actionWithTitle:@"Да" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"пользователь выбрал вариат \"Да\"");
    }];
    // 2.2  добавляем UIAlertAction в UIAlertController (только после этого появится кнопка)
//    [alertController addAction:actionYES];
    
    // 2.3 для создание второй кпонки выполняем п2.1 и п.2.2
    UIAlertAction *actionNo = [UIAlertAction actionWithTitle:@"Нет" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"пользователь выбрал вариат \"Нет\"");
    }];
    [alertController addAction:actionYES];
    [alertController addAction:actionNo];

    
    // отоброжаем алер (
    [self presentViewController:alertController animated:YES completion:^{
    }];
}

- (IBAction)showActionSheet:(UIButton *)sender {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Предупреждение" message:@"Вы действительно хотите выполнить это действие" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Возможно" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        NSLog(@"пользователь выбрал вариат \"Возможно\"");
        
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Подумаю" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        NSLog(@"пользователь выбрал вариат \"Подумаю\"");
        
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Напомни позже" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        NSLog(@"пользователь выбрал вариат \"Напомни позже\"");
        
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Нет" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
        NSLog(@"пользователь выбрал вариат \"Нет\"");
        
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Да" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        
        NSLog(@"пользователь выбрал вариат \"Да\"");
        
    }]];
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        [self presentViewController:alertController animated:YES completion:nil];
    } else {
        // для отображение ActionSheet для iPad будем использовать UIPopoverPresentaionController
        alertController.modalPresentationStyle = UIModalPresentationPopover;
        UIPopoverPresentationController *popoverController = alertController.popoverPresentationController;
        
        popoverController.sourceView = sender;
        popoverController.sourceRect = sender.bounds;
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

- (IBAction)testShowCiAlert:(id)sender {
    showAlert(@"Title", @"Message");
}

@end
