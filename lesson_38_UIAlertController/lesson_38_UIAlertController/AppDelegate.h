//
//  AppDelegate.h
//  lesson_38_UIAlertController
//
//  Created by Yurii Bosov on 12/15/16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

