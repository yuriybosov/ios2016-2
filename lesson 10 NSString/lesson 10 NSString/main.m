//
//  main.m
//  lesson 10 NSString
//
//  Created by Yuriy Bosov on 9/1/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        // NSString
        NSString *str1 = @"Test";
        NSString *str2 = @"test";
        
        // 1. Сравнение строк
        // 1.1 isEqualToString (!регистрочуствителен)
        if ([str1 isEqualToString:str2]) {
            NSLog(@"%@ == %@", str1, str2);
        } else {
            NSLog(@"%@ != %@", str1, str2);
        }
        
        // 1.2 == (c помощью операта сравнения, !регистрочуствителен)
        if (str1 == str2) {
            NSLog(@"%@ == %@", str1, str2);
        } else {
            NSLog(@"%@ != %@", str1, str2);
        }
        
        // 1.3 compare:options: c помощью опции NSCaseInsensitiveSearch можем убрать регистрочуствительность
        if ([str1 compare:str2 options:NSCaseInsensitiveSearch] ==
            NSOrderedSame){
            NSLog(@"%@ == %@", str1, str2);
        } else {
            NSLog(@"%@ != %@", str1, str2);
        }
        
        // 2. Поиск строки
        // 2.1 совпадение(поиск) в начале строки
        BOOL result = [str1 hasPrefix:@"E"];
        
        // 2.2 совпадение(поиск) в конце строки
        result = [str1 hasSuffix:@"E"];
        
        // 2.3 поиск подстроки в строке с помощью rangeOfString (!регистрочуствителен)
        NSRange range = [str1 rangeOfString:str2];
        if (range.location != NSNotFound) {
            NSLog(@"%@ содержится в %@", str2, str1);
        } else{
            NSLog(@"%@ НЕ содержится в %@", str2, str1);
        }
        
        // 2.4 поиск подстроки в строке с помощью rangeOfString:options:
        // можно убрать регистрочуствительность
        range = [str1 rangeOfString:str2 options:NSCaseInsensitiveSearch];
        if (range.location != NSNotFound) {
            NSLog(@"%@ содержится в %@", str2, str1);
        } else{
            NSLog(@"%@ НЕ содержится в %@", str2, str1);
        }
        
        // 3. Объединение строк
        // 3.1 stringByAppendingString
        str1 = [str1 stringByAppendingString:str2];
        NSLog(@"str1 %@", str1);
        
        // 3.2
        str1 = [str1 stringByAppendingFormat:@" <%@>", str2];
        NSLog(@"str1 %@", str1);
        
        // 4. Подстроки
        // 4.1 substringFromIndex: - требует проверки на выход за границы строки
        str2 = [str1 substringFromIndex:3];
        NSLog(@"str2 = %@", str2);
        
        //4.2 substringToIndex: - требует проверки на выход за границы строки
        str2 = [str1 substringToIndex:3];
        NSLog(@"str2 = %@", str2);
        
        // 4.3 substringWithRange: - требует проверки на выход за границы строки
        range = NSMakeRange(3, 3);
        str2 = [str1 substringWithRange:range];
        NSLog(@"str2 = %@", str2);
        
        // 5 Перевод в нижний \ верхний регистр
        str2 = [str1 lowercaseString];
        NSLog(@"lowercase str2 = %@", str2);
        
        str2 = [str1 uppercaseString];
        NSLog(@"uppercase str2 = %@", str2);
        
        str2 = [str1 capitalizedString];
        NSLog(@"capitalized str2 = %@", str2);
        
        // 6 str to ...
        // 6.1 to int
        str2 = @"Testtest <Test>";
        NSLog(@"%li", [str2 integerValue]);
        
        // 6.2 to bool
        NSLog(@"%i", [str2 boolValue]);
        
        // 6.3 to float
        NSLog(@"%f", [str2 floatValue]);
    }
    return 0;
}
