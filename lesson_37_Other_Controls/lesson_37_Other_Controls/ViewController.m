//
//  ViewController.m
//  lesson_37_Other_Controls
//
//  Created by Yurii Bosov on 12/11/16.
//  Copyright © 2016 ios. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {
    IBOutlet UISegmentedControl *segmentedControl;
    IBOutlet UIStepper *stepper;
    IBOutlet UISwitch *mySwitch;
    IBOutlet UISlider *slider;
    
    IBOutlet UILabel *lbSteppetValue;
    IBOutlet UILabel *lbSwitchValue;
    IBOutlet UILabel *lbSlideValue;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // добавляем программно экшены на контролы
    // добавляем медот(экшен) segmentedControlChangeValue:, который вызовется у self когда у segmentedControl произойдет событие UIControlEventValueChanged
    [segmentedControl addTarget:self
                         action:@selector(segmentedControlChangeValue:)
               forControlEvents:UIControlEventValueChanged];
    
    [stepper addTarget:self
                action:@selector(stepperChangeValue:)
      forControlEvents:UIControlEventValueChanged];
    
    [mySwitch addTarget:self
                 action:@selector(switchChangeValue:)
       forControlEvents:UIControlEventValueChanged];
    
    [slider addTarget:self
               action:@selector(sliderChangeValue:)
     forControlEvents:UIControlEventValueChanged];
    
    // segmentedControl
    [segmentedControl setTitle:@"1" forSegmentAtIndex:0];
    [segmentedControl setTitle:@"2" forSegmentAtIndex:1];
    [segmentedControl insertSegmentWithTitle:@"3" atIndex:2 animated:NO];
    
    segmentedControl.selectedSegmentIndex = 1;
    
    // stepper
    stepper.minimumValue = 0;
    stepper.maximumValue = 10;
    stepper.stepValue = 1;
    stepper.value = 3;
    
    // switch
    mySwitch.on = NO;
    
    // slider
    slider.minimumValue = 0;
    slider.maximumValue = 100;
    slider.value = slider.maximumValue/2;
    
    //
    [self stepperChangeValue:stepper];
    [self switchChangeValue:mySwitch];
    [self sliderChangeValue:slider];
}

#pragma mark - Actions

- (void)segmentedControlChangeValue:(UISegmentedControl *)controll{
    NSLog(@"selected index = %li", controll.selectedSegmentIndex);
}

- (void)stepperChangeValue:(UIStepper *)controll{
    lbSteppetValue.text = [NSString stringWithFormat:@"%.0f", controll.value];
}

- (void)switchChangeValue:(UISwitch *)controll{
    lbSwitchValue.text = controll.on ? @"Вкл" : @"Выкл";
}

- (void)sliderChangeValue:(UISlider *)controll{
    lbSlideValue.text = [NSString stringWithFormat:@"%.0f", controll.value];
}

@end
