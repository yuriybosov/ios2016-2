//
//  AppDelegate.h
//  lesson 15_intro_ui
//
//  Created by Yuriy Bosov on 9/18/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

