//
//  ViewController.m
//  lesson_30_Cocoapods
//
//  Created by Yurii Bosov on 11/13/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"
#import <DCPathButton.h>

@interface ViewController () <DCPathButtonDelegate>

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    DCPathButton *centerButton = [[DCPathButton alloc]initWithCenterImage:[UIImage imageNamed:@"chooser-button-tab"]
                                                         highlightedImage:[UIImage imageNamed:@"chooser-button-tab-highlighted"]];
    [self.view addSubview:centerButton];
    centerButton.dcButtonCenter = CGPointMake(self.view.frame.size.width/2,
                                      self.view.frame.size.height/2);
    centerButton.delegate = self;
    
    DCPathItemButton *item = [[DCPathItemButton alloc]initWithImage:[UIImage imageNamed:@"chooser-moment-icon-camera"]
                                                   highlightedImage:[UIImage imageNamed:@"chooser-moment-icon-camera-highlighted"]
                                                    backgroundImage:[UIImage imageNamed:@"chooser-moment-button"]
                                         backgroundHighlightedImage:[UIImage imageNamed:@"chooser-moment-button-highlighted"]];
    [centerButton addPathItems:@[item]];
    
    
    //
    NSLayoutConstraint *c1 = [NSLayoutConstraint constraintWithItem:centerButton
                                 attribute:NSLayoutAttributeCenterX
                                 relatedBy:NSLayoutRelationEqual
                                    toItem:self.view
                                 attribute:NSLayoutAttributeCenterX
                                multiplier:1
                                  constant:0];
    
    NSLayoutConstraint *c2 = [NSLayoutConstraint constraintWithItem:centerButton
                                 attribute:NSLayoutAttributeCenterY
                                 relatedBy:NSLayoutRelationEqual
                                    toItem:self.view
                                 attribute:NSLayoutAttributeCenterY
                                multiplier:1
                                  constant:0];
    
//    [NSLayoutConstraint activateConstraints:@[c1,c2]];
    [self.view addConstraint:c1];
    [self.view addConstraint:c2];
}


- (void)pathButton:(DCPathButton *)dcPathButton clickItemButtonAtIndex:(NSUInteger)itemButtonIndex{
    ;
}

@end
