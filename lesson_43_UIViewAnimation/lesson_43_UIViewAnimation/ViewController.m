//
//  ViewController.m
//  lesson_43_UIViewAnimation
//
//  Created by Yurii Bosov on 1/12/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"

typedef enum : NSUInteger {
    DirectionTop,
    DirectionRight,
    DirectionBotton,
    DirectionLeft,
} Direction;


@interface ViewController () {
    IBOutlet UIView *rect;
}

@end

@implementation ViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // start animation
    [self moveToDirection:DirectionRight];
}

- (void)moveToDirection:(Direction)direction {
    
    CGPoint center;
    switch (direction) {
        case DirectionTop:
            center = CGPointMake(rect.frame.size.width/2,
                                 rect.frame.size.height/2);
            break;
            
        case DirectionRight:
            center = CGPointMake(self.view.frame.size.width - rect.frame.size.width/2,
                                 rect.frame.size.height/2);
            break;
            
        case DirectionBotton:
            center = CGPointMake(self.view.frame.size.width - rect.frame.size.width/2,
                                 self.view.frame.size.height - rect.frame.size.height/2);
            break;
            
        default: // DirectionLeft
            center = CGPointMake(rect.frame.size.width/2,
                                 self.view.frame.size.height - rect.frame.size.height/2);
            break;
    }
    
    [UIView animateWithDuration:1 animations:^{
        // тут пишем вводные параметры анимации
        rect.center = center;
        
    } completion:^(BOOL finished) {
        // completion блок вызывается после окончания анимации
        
        [self moveToDirection: (direction == DirectionLeft) ? DirectionTop : direction + 1];
        
    }];
}

@end
