//
//  AppDelegate.h
//  lesson_43_UIViewAnimation
//
//  Created by Yurii Bosov on 1/12/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

