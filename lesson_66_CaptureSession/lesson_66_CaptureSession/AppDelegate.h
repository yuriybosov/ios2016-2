//
//  AppDelegate.h
//  lesson_66_CaptureSession
//
//  Created by Yurii Bosov on 6/23/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

