//
//  ViewController.m
//  lesson_66_CaptureSession
//
//  Created by Yurii Bosov on 6/23/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "Helper.h"

@interface ViewController () <AVCaptureFileOutputRecordingDelegate> {
    IBOutlet UIButton *startButton;
    IBOutlet UIButton *stopButton;
    IBOutlet UIActivityIndicatorView *indicatorView;
    
    // сессия захвата
    AVCaptureSession *session;
    
    // старт \ стоп \ сохраниния выходяй информации
    AVCaptureMovieFileOutput *output;
    
    AVCaptureVideoPreviewLayer *previewLayer;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // создание сессии
    session = [[AVCaptureSession alloc] init];
    [session beginConfiguration];
    
    // создаем "девайсы", с которых будем получать данные
    AVCaptureDevice *camera = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    AVCaptureDevice *microphon = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeAudio];
    
    //
    NSError *error1 = nil;
    AVCaptureDeviceInput *videoInput = [AVCaptureDeviceInput deviceInputWithDevice:camera error:&error1];
    NSError *error2 = nil;
    AVCaptureDeviceInput *audioInput = [AVCaptureDeviceInput deviceInputWithDevice:microphon error:&error2];
    
    if (!error1 && !error2 && videoInput && audioInput) {
        
        // добавили для сессия входящие девайсы
        [session addInput:videoInput];
        [session addInput:audioInput];
        
        // добавили для сессия "менеджера" записи файла
        output = [[AVCaptureMovieFileOutput alloc] init];
        [session addOutput:output];
        
        // добавили для сессия вывод видео во вью
        previewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:session];
        [previewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
        [self.view.layer insertSublayer:previewLayer atIndex:0];
        previewLayer.frame = self.view.bounds;
        
        [session commitConfiguration];
        [session startRunning];
        
    } else {
        NSLog(@"error1 %@", error1);
        NSLog(@"error2 %@", error2);
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    previewLayer.frame = self.view.bounds;
}

#pragma makr - Actions

- (IBAction)startButtonClicked:(id)sender {
    // если нет записи - начать запись
    if (!output.isRecording) {
        
        NSString *folderPath = [Helper videoDirectoryPath];
        NSString *fileName = [NSString stringWithFormat:@"%0.f.mp4", [[NSDate date] timeIntervalSince1970]];
        NSString *fullPath = [folderPath stringByAppendingPathComponent:fileName];
        NSURL *url = [NSURL fileURLWithPath:fullPath];
        
        [output startRecordingToOutputFileURL:url
                            recordingDelegate:self];
    }
}

- (IBAction)stopButtonClicked:(id)sender {
    // если есть запись - сохранить запись
    if (output.isRecording) {
        [output stopRecording];
        [indicatorView startAnimating];
    }
}

#pragma mark - AVCaptureFileOutputRecordingDelegate

- (void)captureOutput:(AVCaptureFileOutput *)captureOutput didStartRecordingToOutputFileAtURL:(NSURL *)fileURL fromConnections:(NSArray *)connections{
    startButton.enabled = NO;
    stopButton.enabled = YES;
}

- (void)captureOutput:(AVCaptureFileOutput *)captureOutput didFinishRecordingToOutputFileAtURL:(NSURL *)outputFileURL fromConnections:(NSArray *)connections error:(NSError *)error {
    startButton.enabled = YES;
    stopButton.enabled = NO;
    [indicatorView stopAnimating];
    
    if (error) {
        
        if (outputFileURL) {
            [[NSFileManager defaultManager] removeItemAtURL:outputFileURL error:nil];
        }
        
        NSLog(@"DidFinishRecordingToOutputFileAtURL Error %@", error);
    } else {
        NSLog(@"DidFinishRecordingToOutputFileAtURL %@", outputFileURL);
    }
}

@end
