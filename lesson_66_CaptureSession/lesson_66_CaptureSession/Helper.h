//
//  Helper.h
//  lesson_66_CaptureSession
//
//  Created by Yurii Bosov on 6/23/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import <UIKit/UIKit.h>

@interface Helper : NSObject

+ (NSString *)videoDirectoryPath;
+ (UIImage *)thumbImageFromAsset:(AVURLAsset *)asset;
+ (NSString *)fileNameFromAsset:(AVURLAsset *)asset;

@end
