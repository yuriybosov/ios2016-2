//
//  VideoListController.m
//  lesson_66_CaptureSession
//
//  Created by Yurii Bosov on 6/23/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "VideoListController.h"
#import "Helper.h"
#import <AVFoundation/AVFoundation.h>

@interface VideoListController () {
    NSMutableArray <AVURLAsset *>*dataSource;
}

@end

@implementation VideoListController

- (void)viewDidLoad {
    [super viewDidLoad];
    dataSource = [NSMutableArray new];
    
    NSString *folderPath = [Helper videoDirectoryPath];
    NSArray *items = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:folderPath error:nil];
    for (NSString *fileName in items) {
        NSString *filePath = [folderPath stringByAppendingPathComponent:fileName];
        AVURLAsset *asset = [AVURLAsset assetWithURL:[NSURL URLWithString:filePath]];
        [dataSource addObject:asset];
    }
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID" forIndexPath:indexPath];
    AVURLAsset *asset = dataSource[indexPath.row];
    cell.textLabel.text = [Helper fileNameFromAsset:asset];
//    cell.detailTextLabel.text = asset.duration;
    cell.imageView.image = [Helper thumbImageFromAsset:asset];
    return cell;
}

@end
