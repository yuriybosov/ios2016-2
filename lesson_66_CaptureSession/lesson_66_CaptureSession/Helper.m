//
//  Helper.m
//  lesson_66_CaptureSession
//
//  Created by Yurii Bosov on 6/23/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "Helper.h"

@implementation Helper

+ (NSString *)videoDirectoryPath {
    NSString *cacheDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject];
    NSString *folderName = @"Video";
    NSString *fullPath = [cacheDirectory stringByAppendingPathComponent:folderName];
    
    //1. если такой папке нет, то ее нужно создать
    if (![[NSFileManager defaultManager] fileExistsAtPath:fullPath]) {
        
        NSError *error = nil;
        BOOL createFolderResult = [[NSFileManager defaultManager] createDirectoryAtPath:fullPath withIntermediateDirectories:YES attributes:nil error:&error];
        if (!createFolderResult || error) {
            fullPath = nil;
            NSLog(@"не смогли создать папку");
            abort();
        }
    }
    
    return fullPath;
}

+ (UIImage *)thumbImageFromAsset:(AVURLAsset *)asset1
{
//    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
//    imageGenerator.appliesPreferredTrackTransform = YES;
//    CMTime time = [asset duration];
//    time.value = 0;
//    CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
//    UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
//    CGImageRelease(imageRef);
    
    
    AVURLAsset *asset= [[AVURLAsset alloc] initWithURL:asset1.URL options:nil];
    AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    generator.appliesPreferredTrackTransform=TRUE;
    CMTime thumbTime = CMTimeMakeWithSeconds(0,30);
    
    AVAssetImageGeneratorCompletionHandler handler = ^(CMTime requestedTime, CGImageRef im, CMTime actualTime, AVAssetImageGeneratorResult result, NSError *error){
        if (result != AVAssetImageGeneratorSucceeded) {
            NSLog(@"couldn't generate thumbnail, error:%@", error);
        }
    };
    
    CGSize maxSize = CGSizeMake(320, 180);
    generator.maximumSize = maxSize;
    [generator generateCGImagesAsynchronouslyForTimes:[NSArray arrayWithObject:[NSValue valueWithCMTime:thumbTime]] completionHandler:handler];
    
    return nil;
}

+ (NSString *)fileNameFromAsset:(AVURLAsset *)asset {
    return asset.URL.absoluteString.lastPathComponent.stringByDeletingPathExtension;
}

@end
