//
//  ViewController.swift
//  lesson_74_1_localized
//
//  Created by Yurii Bosov on 7/25/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var button: UIButton?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "TEST"
        title = NSLocalizedString("TEST", comment: "")
        button?.setTitle(NSLocalizedString("button_title", comment: ""), for: .normal)
    }
}

