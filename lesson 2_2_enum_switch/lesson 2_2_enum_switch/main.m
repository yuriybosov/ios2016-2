//
//  main.m
//  lesson 2_2_enum_switch
//
//  Created by Yuriy Bosov on 7/10/16.
//  Copyright © 2016 iOS. All rights reserved.
//

typedef enum {
    Kvartal1 = 1,
    Kvartal2,
    Kvartal3,
    Kvartal4
}Kvartal;


#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        Kvartal kvartal = Kvartal1;
        
        switch (kvartal) {
            case Kvartal1:
                NSLog(@"Первый квартал");
                break;
                
            case Kvartal2:
                NSLog(@"Второй квартал");
                break;
                
            case Kvartal3:
                NSLog(@"Третий квартал");
                break;
                
            case Kvartal4:
                NSLog(@"Четвертый квартал");
                break;
            
            default:
                NSLog(@"Квартал указан не корретно");
                break;
        }
        
    }
    return 0;
}
