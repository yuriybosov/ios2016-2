
#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        NSMutableArray *arrya = [[NSMutableArray alloc] init];
        
        // добавление эл-тов в массив
        [arrya addObject:@"1"];
        
        // удаление эл-тов из массива
        [arrya removeObject:@"1"];
        
        // заполнение массива в цикле
        NSNumber *numb = nil;
        for (NSInteger i = 0; i < 100; i++)
        {
            numb = @(i);
            [arrya addObject:numb];
        }
        
        // вставка элемента
        [arrya insertObject:@(100) atIndex:0];
        
        // удаление элемента массива по индексу
        [arrya removeObjectAtIndex:0];
        
        // удаление объектов в диапазоне
        // удаляем первые три элемента
        NSRange range = NSMakeRange(0, 3);
        [arrya removeObjectsInRange:range];
        
        // удаляем последнии три элемента
        range = NSMakeRange(arrya.count - 3, 3);
        [arrya removeObjectsInRange:range];
        
        // очистка массива
        [arrya removeAllObjects];
        
        NSLog(@"array = %@", arrya);
        
        // добавление объектов из другого массива
        NSArray *array1 = @[@"str1", @"str2",@"str3"];
        [arrya addObjectsFromArray:array1];
        
        // замена объекта по индексу
        [arrya replaceObjectAtIndex:0 withObject:@"qqqqq"];
        
        NSLog(@"array = %@", arrya);
    }
    return 0;
}
