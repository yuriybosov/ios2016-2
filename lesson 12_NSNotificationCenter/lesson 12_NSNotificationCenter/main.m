//
//  main.m
//  lesson 12_NSNotificationCenter
//
//  Created by Yuriy Bosov on 9/11/16.
//  Copyright © 2016 iOS. All rights reserved.
//
#import "Sender.h"
#import "Reciever.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        Sender *sender = [[Sender alloc] init];
        Reciever *reciever = [[Reciever alloc] init];
        
        [sender postNotification1];
        [sender postNotification2WithUserInfo];
        [sender postNotification2WithUserInfoAndObject];
    }
    return 0;
}
