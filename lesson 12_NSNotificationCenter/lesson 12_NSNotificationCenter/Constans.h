//
//  Constans.h
//  lesson 12_NSNotificationCenter
//
//  Created by Yuriy Bosov on 9/11/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#ifndef Constans_h
#define Constans_h

#import <Foundation/Foundation.h>

// объявление статических строковых констант
static NSString *const kNotificationMessage1 = @"kNotificationMessage1";
static NSString *const kNotificationMessage2 = @"kNotificationMessage2";

#endif /* Constans_h */
