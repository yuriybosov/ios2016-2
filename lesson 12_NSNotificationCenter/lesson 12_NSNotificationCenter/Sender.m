//
//  Sender.m
//  lesson 12_NSNotificationCenter
//
//  Created by Yuriy Bosov on 9/11/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "Sender.h"

@implementation Sender

- (void)postNotification1 {
    // отправка уведомления без каких либо дополнительных параметров и объека
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationMessage1 object:nil];
}

- (void)postNotification2WithUserInfo {
    // отправка уведомления c дополнительными параметрами
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationMessage2 object:nil userInfo:@{@"key1":@"value1",
                                                                                                           @"key2":@"value2",}];
}

- (void)postNotification2WithUserInfoAndObject{
    // отправка уведомления c дополнительными параметрами и объектов
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationMessage2 object:self userInfo:@{@"key1":@"value1",
                                                                                                           @"key2":@"value2",}];
}

@end
