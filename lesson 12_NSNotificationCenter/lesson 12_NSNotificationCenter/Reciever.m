//
//  Reciever.m
//  lesson 12_NSNotificationCenter
//
//  Created by Yuriy Bosov on 9/11/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "Reciever.h"

@implementation Reciever

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        // подписыватемся на уведомления
        // при подписке мы указываем имя_уведомления и имя_селектора, который вызовется при получении уведомления
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(processingMessage1) name:kNotificationMessage1 object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(processingMessage2:) name:kNotificationMessage2 object:nil];
    }
    return self;
}

- (void)dealloc {
    
    NSLog(@"dealloc");
    // не забываем выполнить отписку от уведомлений!!!
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - NSNotication

- (void)processingMessage1 {
    
    NSLog(@"processingMessage1");
}

- (void)processingMessage2:(NSNotification *)notification {
    
    NSLog(@"processingMessage2 with notification %@", notification);
}

@end
