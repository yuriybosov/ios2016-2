//
//  Sender.h
//  lesson 12_NSNotificationCenter
//
//  Created by Yuriy Bosov on 9/11/16.
//  Copyright © 2016 iOS. All rights reserved.
//

@interface Sender : NSObject

- (void)postNotification1;
- (void)postNotification2WithUserInfo;
- (void)postNotification2WithUserInfoAndObject;

@end
