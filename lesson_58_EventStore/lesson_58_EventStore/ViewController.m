//
//  ViewController.m
//  lesson_58_EventStore
//
//  Created by Yurii Bosov on 5/25/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"
#import <EventKit/EventKit.h>

@interface ViewController () {
    EKEventStore *store;
}

@end

@implementation ViewController

- (void)dealloc {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    store = [EKEventStore new];
}

#pragma mark - Actions

- (IBAction)eventButtonClicked:(id)sender {
    
    EKAuthorizationStatus status = [EKEventStore authorizationStatusForEntityType:EKEntityTypeEvent];
    switch (status) {
        case EKAuthorizationStatusNotDetermined:
        {
            [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError * _Nullable error) {
                
                if (error) {
                    NSLog(@"error %@", error);
                } else {
                    // create event and add to calendar
                    [self createTextEvent];
                }
            }];
        }
            break;
        
        case EKAuthorizationStatusDenied:
        case EKAuthorizationStatusRestricted:
            NSLog(@"доступ к календарю запрещен");
            break;
        
        case EKAuthorizationStatusAuthorized:
            // create event and add to calendar
            [self createTextEvent];
            break;
    }
}

- (IBAction)remainderButtonClicked:(id)sender {
    
}

#pragma mark - Event

- (void)createTextEvent {
    
    EKEvent *event = [EKEvent eventWithEventStore:store];
    
    event.startDate = [NSDate dateWithTimeIntervalSince1970:1495962000]; // для GMT=0
    event.endDate = [NSDate dateWithTimeIntervalSince1970:1495972800];
    
    event.title = @"го в шаг, я создал!!!11!!!1";
    event.calendar = [store defaultCalendarForNewEvents];
    
    EKRecurrenceRule *rule = [[EKRecurrenceRule alloc] initRecurrenceWithFrequency:EKRecurrenceFrequencyWeekly interval:3 end:nil];
    
    [event addRecurrenceRule:rule];
    [event addAlarm:[EKAlarm alarmWithRelativeOffset:-300]];
    
    NSError *error = nil;
    BOOL saveResult = [store saveEvent:event span:EKSpanThisEvent error:&error];
    if (error) {
        NSLog(@"save event error %@", error);
    } else {
        NSLog(@"save result %i", saveResult);
    }
}

#pragma mark - Reminder

- (void)createTextReminder {
    
}

@end
