//
//  DataManager.h
//  lesson_51_1_NSUserDefault_Exemple
//
//  Created by Yurii Bosov on 3/9/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Office.h"

typedef void(^ComplitionBlock)(id data, NSError *error, BOOL cancel);

@interface DataManager : NSObject

+ (instancetype)sharedInstace;

- (NSURLSessionDataTask *)officeAtCity:(NSString *)city
                               address:(NSString *)address
                            complition:(ComplitionBlock)complition;

@end
