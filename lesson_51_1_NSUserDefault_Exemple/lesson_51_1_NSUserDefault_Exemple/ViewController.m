//
//  ViewController.m
//  lesson_51_1_NSUserDefault_Exemple
//
//  Created by Yurii Bosov on 3/9/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"
#import "DataManager.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self fetchData];
}

- (void)fetchData {
    NSURLSessionDataTask *task = [[DataManager sharedInstace] officeAtCity:@"Днепропетровск" address:nil complition:^(NSArray *data, NSError *error, BOOL cancel) {
        NSLog(@"data %@", [data componentsJoinedByString:@"\n"]);
        NSLog(@"error %@", error);
        NSLog(@"cancel %i", cancel);
    }];

    // тест на отмену запроса
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        [task cancel];
//    });
}

@end
