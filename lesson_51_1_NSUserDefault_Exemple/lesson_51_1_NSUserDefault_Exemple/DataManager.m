//
//  DataManager.m
//  lesson_51_1_NSUserDefault_Exemple
//
//  Created by Yurii Bosov on 3/9/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "DataManager.h"

@interface DataManager () {
    NSURLSession *session;
}

@end


@implementation DataManager

+ (instancetype)sharedInstace {
    static DataManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [DataManager new];
    });
    return manager;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        configuration.HTTPAdditionalHeaders = @{@"Content-Type":@"application/json; charset=UTF-8",
                                                @"Accept":@"application/json; charset=UTF-8"};
        session = [NSURLSession sessionWithConfiguration:configuration];
    }
    return self;
}

- (NSURLSessionDataTask *)officeAtCity:(NSString *)city
                               address:(NSString *)address
                            complition:(ComplitionBlock)complition {
    NSCharacterSet *characters = [NSCharacterSet URLFragmentAllowedCharacterSet];
    NSString *cityParam = city ? [city stringByAddingPercentEncodingWithAllowedCharacters:characters] : @"";
    NSString *addressParam = address ? [address stringByAddingPercentEncodingWithAllowedCharacters:characters] : @"";
    
    NSString *urlString = [NSString stringWithFormat:@"https://api.privatbank.ua/p24api/pboffice?json&city=%@&address=%@", cityParam, addressParam];
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"GET";
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        NSMutableArray *modelsArray = nil;
        NSError *jsonError = nil;
        
        // если нет ошибки и если есть data то парсим наш ответ, создаем модели, сохраняем модели в NSUserDefault
        if (data) {
            NSArray *dataArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
            if (!jsonError) {
                
                modelsArray = [NSMutableArray new];
                for (NSDictionary *dictData in dataArray) {
                    Office *model = [[Office alloc] initWithDictionary:dictData];
                    [modelsArray addObject:model];
                }
                
                // save to NSUserDefault
                NSData *saveData = [NSKeyedArchiver archivedDataWithRootObject:modelsArray];
                [[NSUserDefaults standardUserDefaults] setObject:saveData forKey:@"officeModels"];
            }
        }
        
         // если ошибка и не получили данных, то пытаемся получить данные из NSUserDefault
        if (!modelsArray) {
            NSData *loadData = [[NSUserDefaults standardUserDefaults] objectForKey:@"officeModels"];
            if (loadData && [loadData isKindOfClass:[NSData class]]) {
                NSArray *tempArray = [NSKeyedUnarchiver unarchiveObjectWithData:loadData];
                modelsArray = [[NSMutableArray alloc] initWithArray:tempArray];
            }
        }
        
        // вызов complition блока в главном потоке
        dispatch_async(dispatch_get_main_queue(), ^{
            if (complition) {
                complition (modelsArray,
                            jsonError ?: error,
                            error.code == NSURLErrorCancelled);
            }
        });
    }];
    
    [task resume];
    
    return task;
}

@end
