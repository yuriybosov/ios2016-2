//
//  Office.m
//  lesson_51_1_NSUserDefault_Exemple
//
//  Created by Yurii Bosov on 3/9/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "Office.h"
#import "NSDictionary+NSNull.h"

//name: "Отделение "ЦПОИК в г.Александрия" ПриватБанка",
//state: "Кировоградская",
//id: null,
//country: "Украина",
//city: "Кировоград",
//index: "28000",
//phone: "(8-05235) 4-12-47",
//email: "Elena.Muntjanova@pbank.com.ua",
//address: "ул Калинина 30"

@implementation Office

- (instancetype)initWithDictionary:(NSDictionary *)data {
    self = [super init];
    if (self) {
        
        self.name = [data objectForKeyNotNull:@"name"];
        self.state = [data objectForKeyNotNull:@"state"];
        self.ID = [data objectForKeyNotNull:@"id"];
        self.country = [data objectForKeyNotNull:@"country"];
        self.city = [data objectForKeyNotNull:@"city"];
        self.index = [data objectForKeyNotNull:@"index"];
        self.phone = [data objectForKeyNotNull:@"phone"];
        self.email = [data objectForKeyNotNull:@"email"];
        self.address = [data objectForKeyNotNull:@"address"];
        
    }
    return self;
}

#pragma mark - NSCoding

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        self.name = [aDecoder decodeObjectForKey:@"name"];
        self.state = [aDecoder decodeObjectForKey:@"state"];
        self.ID = [aDecoder decodeObjectForKey:@"ID"];
        self.country = [aDecoder decodeObjectForKey:@"country"];
        self.city = [aDecoder decodeObjectForKey:@"city"];
        self.index = [aDecoder decodeObjectForKey:@"index"];
        self.phone = [aDecoder decodeObjectForKey:@"phone"];
        self.email = [aDecoder decodeObjectForKey:@"email"];
        self.address = [aDecoder decodeObjectForKey:@"address"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.name forKey:@"name"];
    [aCoder encodeObject:self.state forKey:@"state"];
    [aCoder encodeObject:self.ID forKey:@"ID"];
    [aCoder encodeObject:self.country forKey:@"country"];
    [aCoder encodeObject:self.city forKey:@"city"];
    [aCoder encodeObject:self.index forKey:@"index"];
    [aCoder encodeObject:self.phone forKey:@"phone"];
    [aCoder encodeObject:self.email forKey:@"email"];
    [aCoder encodeObject:self.address forKey:@"address"];
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@, %@", _name, _address];
}

@end
