//
//  Office.h
//  lesson_51_1_NSUserDefault_Exemple
//
//  Created by Yurii Bosov on 3/9/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>

//name: "Отделение "ЦПОИК в г.Александрия" ПриватБанка",
//state: "Кировоградская",
//id: "2516",
//country: "Украина",
//city: "Кировоград",
//index: "28000",
//phone: "(8-05235) 4-12-47",
//email: "Elena.Muntjanova@pbank.com.ua",
//address: "ул Калинина 30"

@interface Office : NSObject <NSCoding>

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *state;
@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *country;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *index;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *address;

- (instancetype)initWithDictionary:(NSDictionary *)data;

@end
