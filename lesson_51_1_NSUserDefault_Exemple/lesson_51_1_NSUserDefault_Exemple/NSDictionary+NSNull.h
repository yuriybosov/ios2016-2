//
//  NSDictionary+NSNull.h
//  lesson_51_1_NSUserDefault_Exemple
//
//  Created by Yurii Bosov on 3/12/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (NSNull)

- (id)objectForKeyNotNull:(id)aKey;

@end
