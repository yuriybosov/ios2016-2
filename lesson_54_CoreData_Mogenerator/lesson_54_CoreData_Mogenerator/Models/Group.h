#import "_Group.h"
#import "SelectTableProtocol.h"

@interface Group : _Group <SelectTableCellProtocol>

+ (NSArray <Group *> *)allGroups;

- (NSArray *)allStudents;

@end
