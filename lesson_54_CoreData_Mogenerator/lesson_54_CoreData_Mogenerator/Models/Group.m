#import "Group.h"
#import "DataManager.h"
#import "Student.h"

@interface Group ()

@end

@implementation Group

+ (NSArray <Group *> *)allGroups {
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:[Group entityName]];
    
    NSSortDescriptor *sd = [NSSortDescriptor sortDescriptorWithKey:[GroupAttributes name] ascending:YES];
    
    request.sortDescriptors = @[sd];
    
    return [[DataManager sharedInstance].managedObjectContext executeFetchRequest:request error:nil];
}

- (NSArray *)allStudents {

    NSSortDescriptor *sd = [NSSortDescriptor sortDescriptorWithKey:StudentAttributes.name ascending:YES];
    
    return [self.students.allObjects sortedArrayUsingDescriptors:@[sd]];
}

#pragma mark - SelectTableCellProtocol

- (NSString *)selectTableCellTitle {
    return self.name;
}

@end
