#import "_Student.h"

@interface Student : _Student

+ (NSArray <Student *>*)allStudents;

@end
