// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Group.m instead.

#import "_Group.h"

@implementation GroupID
@end

@implementation _Group

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Group" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Group";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Group" inManagedObjectContext:moc_];
}

- (GroupID*)objectID {
	return (GroupID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic name;

@dynamic students;

- (NSMutableSet<Student*>*)studentsSet {
	[self willAccessValueForKey:@"students"];

	NSMutableSet<Student*> *result = (NSMutableSet<Student*>*)[self mutableSetValueForKey:@"students"];

	[self didAccessValueForKey:@"students"];
	return result;
}

@dynamic teacher;

@end

@implementation GroupAttributes 
+ (NSString *)name {
	return @"name";
}
@end

@implementation GroupRelationships 
+ (NSString *)students {
	return @"students";
}
+ (NSString *)teacher {
	return @"teacher";
}
@end

