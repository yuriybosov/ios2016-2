// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Teacher.m instead.

#import "_Teacher.h"

@implementation TeacherID
@end

@implementation _Teacher

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Teacher" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Teacher";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Teacher" inManagedObjectContext:moc_];
}

- (TeacherID*)objectID {
	return (TeacherID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic name;

@dynamic group;

@end

@implementation TeacherAttributes 
+ (NSString *)name {
	return @"name";
}
@end

@implementation TeacherRelationships 
+ (NSString *)group {
	return @"group";
}
@end

