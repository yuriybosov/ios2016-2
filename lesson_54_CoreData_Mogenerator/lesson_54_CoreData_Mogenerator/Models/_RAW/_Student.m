// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Student.m instead.

#import "_Student.h"

@implementation StudentID
@end

@implementation _Student

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Student" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Student";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Student" inManagedObjectContext:moc_];
}

- (StudentID*)objectID {
	return (StudentID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic dateOfBirdth;

@dynamic name;

@dynamic group;

@end

@implementation StudentAttributes 
+ (NSString *)dateOfBirdth {
	return @"dateOfBirdth";
}
+ (NSString *)name {
	return @"name";
}
@end

@implementation StudentRelationships 
+ (NSString *)group {
	return @"group";
}
@end

