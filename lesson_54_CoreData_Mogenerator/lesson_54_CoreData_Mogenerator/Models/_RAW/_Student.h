// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Student.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@class Group;

@interface StudentID : NSManagedObjectID {}
@end

@interface _Student : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (nullable NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) StudentID *objectID;

@property (nonatomic, strong, nullable) NSDate* dateOfBirdth;

@property (nonatomic, strong, nullable) NSString* name;

@property (nonatomic, strong, nullable) Group *group;

@end

@interface _Student (CoreDataGeneratedPrimitiveAccessors)

- (nullable NSDate*)primitiveDateOfBirdth;
- (void)setPrimitiveDateOfBirdth:(nullable NSDate*)value;

- (nullable NSString*)primitiveName;
- (void)setPrimitiveName:(nullable NSString*)value;

- (Group*)primitiveGroup;
- (void)setPrimitiveGroup:(Group*)value;

@end

@interface StudentAttributes: NSObject 
+ (NSString *)dateOfBirdth;
+ (NSString *)name;
@end

@interface StudentRelationships: NSObject
+ (NSString *)group;
@end

NS_ASSUME_NONNULL_END
