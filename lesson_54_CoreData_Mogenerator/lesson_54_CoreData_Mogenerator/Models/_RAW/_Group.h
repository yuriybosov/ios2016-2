// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Group.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@class Student;
@class Teacher;

@interface GroupID : NSManagedObjectID {}
@end

@interface _Group : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (nullable NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) GroupID *objectID;

@property (nonatomic, strong, nullable) NSString* name;

@property (nonatomic, strong, nullable) NSSet<Student*> *students;
- (nullable NSMutableSet<Student*>*)studentsSet;

@property (nonatomic, strong, nullable) Teacher *teacher;

@end

@interface _Group (StudentsCoreDataGeneratedAccessors)
- (void)addStudents:(NSSet<Student*>*)value_;
- (void)removeStudents:(NSSet<Student*>*)value_;
- (void)addStudentsObject:(Student*)value_;
- (void)removeStudentsObject:(Student*)value_;

@end

@interface _Group (CoreDataGeneratedPrimitiveAccessors)

- (nullable NSString*)primitiveName;
- (void)setPrimitiveName:(nullable NSString*)value;

- (NSMutableSet<Student*>*)primitiveStudents;
- (void)setPrimitiveStudents:(NSMutableSet<Student*>*)value;

- (Teacher*)primitiveTeacher;
- (void)setPrimitiveTeacher:(Teacher*)value;

@end

@interface GroupAttributes: NSObject 
+ (NSString *)name;
@end

@interface GroupRelationships: NSObject
+ (NSString *)students;
+ (NSString *)teacher;
@end

NS_ASSUME_NONNULL_END
