// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Teacher.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@class Group;

@interface TeacherID : NSManagedObjectID {}
@end

@interface _Teacher : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (nullable NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) TeacherID *objectID;

@property (nonatomic, strong, nullable) NSString* name;

@property (nonatomic, strong, nullable) Group *group;

@end

@interface _Teacher (CoreDataGeneratedPrimitiveAccessors)

- (nullable NSString*)primitiveName;
- (void)setPrimitiveName:(nullable NSString*)value;

- (Group*)primitiveGroup;
- (void)setPrimitiveGroup:(Group*)value;

@end

@interface TeacherAttributes: NSObject 
+ (NSString *)name;
@end

@interface TeacherRelationships: NSObject
+ (NSString *)group;
@end

NS_ASSUME_NONNULL_END
