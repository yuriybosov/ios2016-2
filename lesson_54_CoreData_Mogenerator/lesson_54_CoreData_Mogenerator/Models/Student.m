#import "Student.h"
#import "DataManager.h"

@interface Student ()

@end

@implementation Student

+ (NSArray <Student *>*)allStudents {
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:[Student entityName]];
    
    NSSortDescriptor *sd = [NSSortDescriptor sortDescriptorWithKey:StudentAttributes.name ascending:YES];
    
    request.sortDescriptors = @[sd];
    
    return [[DataManager sharedInstance].managedObjectContext executeFetchRequest:request error:nil];
}

@end
