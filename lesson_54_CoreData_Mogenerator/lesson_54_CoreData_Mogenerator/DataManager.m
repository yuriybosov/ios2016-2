//
//  DataManager.m
//  lesson_54_CoreData_Mogenerator
//
//  Created by Yurii Bosov on 4/13/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "DataManager.h"

@implementation DataManager

@synthesize managedObjectModel = _managedObjectModel;
@synthesize managedObjectContext = _managedObjectContext;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

+ (instancetype)sharedInstance {
    static DataManager *dataManager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dataManager = [DataManager new];
    });
    return dataManager;
}

- (NSManagedObjectModel *)managedObjectModel {
    
    if (_managedObjectModel == nil) {
        
        NSString *path = [[NSBundle mainBundle] pathForResource:@"MyDB" ofType:@"momd"];
        
        _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path]];
    }
    
    return _managedObjectModel;
}

- (NSManagedObjectContext *)managedObjectContext {
    if (_managedObjectContext == nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        
        [_managedObjectContext setPersistentStoreCoordinator:self.persistentStoreCoordinator];
    }
    return _managedObjectContext;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    
    if (_persistentStoreCoordinator == nil) {
        _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.managedObjectModel];
        
        // 1. получаем путь к папке 'Library/Application Support'
        NSURL *directoryURL = [[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask].lastObject;
        // 2. создаем урл на файл, который будем использовать как хранилище.
        NSURL *url = [NSURL URLWithString:@"mydb.sqlite" relativeToURL:directoryURL];
        NSError *error = nil;
        
        // для поддержки версионости базы данных нужно указать вот такие вот настройки политики миграции:
        NSDictionary *migratinPolicy = @{ NSMigratePersistentStoresAutomaticallyOption : @(YES),NSInferMappingModelAutomaticallyOption : @(YES)};
        
        // 3. задаем координатору файл хранилища
        NSPersistentStore *store = [_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:url options:migratinPolicy error:&error];
        // 4. проверяем что хранилище было корректно создано
        if (!store) {
            NSLog(@"Data Base Storage was created with error %@", error.localizedDescription);
            abort();
        } else {
            // 5. если файл хранилища успешно был создан, то пометим этот файл (урл к нему), как файл, который не нужно включать в бекап
            [url setResourceValue:@(NO) forKey: NSURLIsExcludedFromBackupKey error:nil];
        }
    }
    
    return _persistentStoreCoordinator;
}

- (void)save {
    
    NSError *error = nil;
    
    if ([self.managedObjectContext hasChanges]) {
        
        BOOL saveResult = [self.managedObjectContext save:&error];
        
        if (!saveResult) {
            NSLog(@"DB SAVE ERROR %@", error);
        }
    }
    
//    if ([self.managedObjectContext hasChanges] &&
//        ![self.managedObjectContext save:&error]) {
//        NSLog(@"DB SAVE ERROR %@", error);
//    }
}

@end
