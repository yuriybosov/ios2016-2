//
//  GroupEditController.m
//  lesson_54_CoreData_Mogenerator
//
//  Created by Yurii Bosov on 4/20/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "GroupEditController.h"
#import "DataManager.h"

@interface GroupEditController ()

@property (nonatomic, weak) IBOutlet UITextField *textFiledName;
@property (nonatomic, weak) IBOutlet UIButton *deleteButton;

@end

@implementation GroupEditController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.group) {
        self.title = @"Редактирование";
        self.textFiledName.text = self.group.name;
        self.deleteButton.hidden = NO;
    } else{
        self.title = @"Создание";
        self.deleteButton.hidden = YES;
    }
}

#pragma mark - Button Actions

- (IBAction)saveButtonClicked:(id)sender {
    
    // проверяем что поле ввода не пустое
    if (self.textFiledName.text.length > 0) {
        
        // провеяем есть ли у нас группа, если нет - то создаем
        if (_group == nil) {
            _group = [Group insertInManagedObjectContext:[DataManager sharedInstance].managedObjectContext];
        }
        _group.name = self.textFiledName.text;
        
        [[DataManager sharedInstance] save];
        
        [self.navigationController popViewControllerAnimated:YES];
        
    } else{
        // отобразить алерт или еще каким то способом что поле ввода не прошло валидацию
        NSLog(@"не прошли валидацию");
    }
}

- (IBAction)deleteButtonClicked:(id)sender {
    if (self.group) {
        [[[DataManager sharedInstance] managedObjectContext] deleteObject:self.group];
        [[DataManager sharedInstance] save];
        
        [self.navigationController popViewControllerAnimated:YES];
    }
}

@end
