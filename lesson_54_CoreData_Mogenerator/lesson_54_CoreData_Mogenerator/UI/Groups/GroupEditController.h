//
//  GroupEditController.h
//  lesson_54_CoreData_Mogenerator
//
//  Created by Yurii Bosov on 4/20/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Group.h"

@interface GroupEditController : UIViewController

@property (nonatomic, strong) Group *group;

@end
