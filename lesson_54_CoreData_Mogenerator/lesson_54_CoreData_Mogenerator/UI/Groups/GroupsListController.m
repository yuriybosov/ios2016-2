//
//  GroupsListController.m
//  lesson_54_CoreData_Mogenerator
//
//  Created by Yurii Bosov on 4/20/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "GroupsListController.h"
#import "Group.h"
#import "DataManager.h"
#import "GroupEditController.h"

@interface GroupsListController () <UITableViewDataSource, UITableViewDelegate> {

    NSMutableArray <Group *> *dataSource;
}

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end

@implementation GroupsListController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Группы";
    
    dataSource = [NSMutableArray new];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // выполним запрос в бд для получения списка всех групп
    [dataSource removeAllObjects];
    [dataSource addObjectsFromArray:[Group allGroups]];
    
    [self.tableView reloadData];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    cell.textLabel.text = dataSource[indexPath.row].name;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"колличество студентов %lu", dataSource[indexPath.row].students.count];
    
    return cell;
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(UITableViewCell *)sender {
    
    if ([sender isKindOfClass:[UITableViewCell class]] &&
        [segue.destinationViewController isKindOfClass:[GroupEditController class]]) {
        
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        ((GroupEditController *)segue.destinationViewController).group = dataSource[indexPath.row];
    }
}

@end
