//
//  StudentsListController.m
//  lesson_54_CoreData_Mogenerator
//
//  Created by Yurii Bosov on 4/20/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "StudentsListController.h"
#import "Student.h"

@interface StudentsListController () <UITableViewDataSource, UITableViewDelegate> {
    NSMutableArray *dataSource;
}

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end

@implementation StudentsListController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    dataSource = [NSMutableArray new];
    
    if (self.group) {
        self.title = self.group.name;
    } else {
        self.title = @"Студенты";
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [dataSource removeAllObjects];
    
    if (self.group) {
        [dataSource addObjectsFromArray:[self.group allStudents]];
    } else {
        // выполнить запрос в бд и отобразить всех студентов
        [dataSource addObjectsFromArray:[Student allStudents]];
    }
    
    [self.tableView reloadData];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    Student *student = dataSource[indexPath.row];
    
    cell.textLabel.text = student.name;
    
    // !!! так делать незя !!! см. предыдущий урок
    NSDateFormatter *df = [NSDateFormatter new];
    df.dateFormat = @"dd.MM.yy";
    
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@, %@", [df stringFromDate:student.dateOfBirdth], student.group.name];
    
    return cell;
}

@end
