//
//  StudentEditController.m
//  lesson_54_CoreData_Mogenerator
//
//  Created by Yurii Bosov on 4/20/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "StudentEditController.h"
#import "SelectTableController.h"
#import "Group.h"
#import "DataManager.h"

@interface StudentEditController () <UITextFieldDelegate, SelectTableProtocol> {
    IBOutlet UITextField *tfName;
    IBOutlet UITextField *tfGroupName;
    IBOutlet UIDatePicker *datePicker;
    
    Group *currentGroup;
}

@end

@implementation StudentEditController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    datePicker.maximumDate = [NSDate date];
    datePicker.minimumDate = [NSDate dateWithTimeIntervalSince1970:0];
    
    if (self.student) {
        self.title = @"Редактирование";
        tfName.text = self.student.name;
        tfGroupName.text = self.student.group.name;
        datePicker.date = self.student.dateOfBirdth;
        currentGroup = self.student.group;
    } else {
        self.title = @"Создание";
        datePicker.date = [NSDate date];
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    BOOL result = YES;
    if (textField == tfGroupName) {
        result = NO;
        // показать экран с выбором группы
        SelectTableController *controller = [SelectTableController new];
        controller.delegate = self;
        controller.dataSource = [Group allGroups];
        controller.currentItem = currentGroup;
        
        UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:controller];
        
        [self presentViewController:nc animated:YES completion:nil];
    }
    return result;
}

#pragma mark - Actions

- (IBAction)saveButtonClicked:(id)sender {
    // 1. валидация полей ввода
    if (tfName.text.length > 0 &&
        currentGroup) {
        
        // 2. сохранили данные
        if (!self.student) {
            self.student = [Student insertInManagedObjectContext:[DataManager sharedInstance].managedObjectContext];
        }
        self.student.name = tfName.text;
        self.student.dateOfBirdth = datePicker.date;
        self.student.group = currentGroup;
        
        [[DataManager sharedInstance] save];
        
        [self.navigationController popViewControllerAnimated:YES];
        
    } else {
        NSLog(@"не все данные введены");
    }
}

#pragma mark - SelectTableProtocol

- (void)selectTableControllerCancel:(SelectTableController *)controller {
    [controller dismissViewControllerAnimated:YES completion:nil];
}

- (void)selectTableController:(SelectTableController *)controller didSelectedItem:(id)item {
    [controller dismissViewControllerAnimated:YES completion:nil];
    
    currentGroup = item;
    tfGroupName.text = currentGroup.name;
}

@end
