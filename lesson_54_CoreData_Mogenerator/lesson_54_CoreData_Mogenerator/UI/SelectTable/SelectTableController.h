//
//  SelectTableController.h
//  lesson_54_CoreData_Mogenerator
//
//  Created by Yurii Bosov on 4/27/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelectTableProtocol.h"

@interface SelectTableController : UITableViewController

@property (nonatomic, weak) id<SelectTableProtocol>delegate;

@property (nonatomic, strong) NSArray<id<SelectTableCellProtocol>>*dataSource;
@property (nonatomic, strong) id<SelectTableCellProtocol>currentItem;

@end
