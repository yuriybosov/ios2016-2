//
//  SelectTableController.m
//  lesson_54_CoreData_Mogenerator
//
//  Created by Yurii Bosov on 4/27/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "SelectTableController.h"

@interface SelectTableController ()

@end

@implementation SelectTableController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Экран выбора";
    
    UIBarButtonItem *bbi = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelButtonClicked)];
    
    self.navigationItem.leftBarButtonItem = bbi;
}

#pragma mark - Action

- (void)cancelButtonClicked {
    [self.delegate selectTableControllerCancel:self];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID"];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellID"];
    }
    
    id<SelectTableCellProtocol> item = self.dataSource[indexPath.row];
    
    cell.textLabel.text = [item selectTableCellTitle];
    
    if (item == self.currentItem) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.delegate selectTableController:self didSelectedItem:self.dataSource[indexPath.row]];
}

@end
