//
//  SelectTableProtocol.h
//  lesson_54_CoreData_Mogenerator
//
//  Created by Yurii Bosov on 4/27/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef SelectTableProtocol_h
#define SelectTableProtocol_h

@class SelectTableController;

// для ячейки
@protocol SelectTableCellProtocol <NSObject>

- (NSString *)selectTableCellTitle;

@end

// для делегата
@protocol SelectTableProtocol <NSObject>

- (void)selectTableController:(SelectTableController *)controller didSelectedItem:(id)item;

- (void)selectTableControllerCancel:(SelectTableController *)controller;

@end

#endif /* SelectTableProtocol_h */
