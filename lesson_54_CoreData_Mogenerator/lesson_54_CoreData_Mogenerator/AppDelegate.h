//
//  AppDelegate.h
//  lesson_54_CoreData_Mogenerator
//
//  Created by Yurii Bosov on 4/13/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

