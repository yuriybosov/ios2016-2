//
//  KeyboardToolbar.h
//  lesson_34_1_UITextField_UIScrollView
//
//  Created by Yurii Bosov on 12/1/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KeyboardToolbar : UIToolbar

@property (nonatomic, strong) NSArray <UITextField *>*textFields;
@property (nonatomic, strong) UITextField *currentTextField;

+ (KeyboardToolbar *)create;

@end
