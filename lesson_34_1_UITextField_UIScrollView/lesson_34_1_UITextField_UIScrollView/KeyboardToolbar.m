//
//  KeyboardToolbar.m
//  lesson_34_1_UITextField_UIScrollView
//
//  Created by Yurii Bosov on 12/1/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import "KeyboardToolbar.h"

@interface KeyboardToolbar () {
    IBOutlet UIBarButtonItem *prevButton;
    IBOutlet UIBarButtonItem *nextButton;
}

@end

@implementation KeyboardToolbar

+ (KeyboardToolbar *)create {
    return [[NSBundle mainBundle] loadNibNamed:@"KeyboardToolbar" owner:nil options:nil].firstObject;
}

- (void)setTextFields:(NSArray<UITextField *> *)textFields {
    _textFields = textFields;
    for (UITextField *tf in _textFields) {
        
        tf.inputAccessoryView = self;
        
        if (tf == _textFields.lastObject) {
            tf.returnKeyType = UIReturnKeyDone;
        } else{
            tf.returnKeyType = UIReturnKeyNext;
        }
    }
}

- (void)setCurrentTextField:(UITextField *)currentTextField {
    _currentTextField = currentTextField;
    
    prevButton.enabled = self.textFields.firstObject != _currentTextField;
    nextButton.enabled = self.textFields.lastObject != _currentTextField;
}

#pragma mark Actions

- (IBAction)prevButtonClicked:(id)sender{
    NSUInteger index = [self.textFields indexOfObject:self.currentTextField];
    if (index != NSNotFound && index > 0) {
        [[self.textFields objectAtIndex:index - 1] becomeFirstResponder];
    }
}

- (IBAction)nextButtonClicked:(id)sender{
    NSUInteger index = [self.textFields indexOfObject:self.currentTextField];
    if (index != NSNotFound && index < self.textFields.count - 1) {
        [[self.textFields objectAtIndex:index + 1] becomeFirstResponder];
    }
}

- (IBAction)doneButtonClicked:(id)sender{
    [self.currentTextField resignFirstResponder];
}

@end
