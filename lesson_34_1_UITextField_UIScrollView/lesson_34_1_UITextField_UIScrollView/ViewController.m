//
//  ViewController.m
//  lesson_34_1_UITextField_UIScrollView
//
//  Created by Yurii Bosov on 11/27/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"
#import "KeyboardToolbar.h"

@interface ViewController () <UITextFieldDelegate> {
    IBOutlet UIScrollView *scrollView;
    IBOutletCollection(UITextField) NSArray *inputFields;
    
    KeyboardToolbar *toolbar;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    toolbar = [KeyboardToolbar create];
    toolbar.textFields = inputFields;
    
    for (UITextField *tf in inputFields) {
        tf.delegate = self;
    }
    
    // подписываемся на уведомления от клавиатуры
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHideNotification:) name:UIKeyboardWillHideNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChangeFrameNotification:) name:UIKeyboardWillChangeFrameNotification object:nil];
}

- (void)dealloc {
    // отписываемся от уведомлений
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Notification

- (void)keyboardWillHideNotification:(NSNotification *)notification {
    //2 set zero content inset
    scrollView.contentInset = UIEdgeInsetsZero;
}

- (void)keyboardWillChangeFrameNotification:(NSNotification *)notification {
    
    //1 get keyboard frame
    NSValue *value = [notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect frame = [value CGRectValue];
    
    //2 set content inset (only bottom)
    scrollView.contentInset = UIEdgeInsetsMake(0, 0, frame.size.height, 0);
}

#pragma mark - Action

- (IBAction)hideKeyboard:(id)sender {
    [self.view endEditing:YES];
}

#pragma makr - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    toolbar.currentTextField = textField;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSUInteger index = [inputFields indexOfObject:textField];
    if (index != NSNotFound) {
        if (index < inputFields.count - 1) {
            UITextField *nextTextField = [inputFields objectAtIndex:index + 1];
            [nextTextField becomeFirstResponder];
        } else {
            [textField resignFirstResponder];
        }
    }
    return YES;
}

@end
