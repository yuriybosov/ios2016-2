//
//  TCOModel.h
//  TCOList
//
//  Created by Yurii Bosov on 2/17/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef void(^ComplitionBlock)(id data, NSString *errorMessage, BOOL canceled);


@interface TCOModel : NSObject

@property (nonatomic, strong) NSString *fullAddressRu;
@property (nonatomic, strong) NSString *placeRu;
@property (nonatomic, strong) NSNumber *latitude;
@property (nonatomic, strong) NSNumber *longitude;

+ (void)loadTCOForCity:(NSString *)city
            complition:(ComplitionBlock)complition;

- (void)setupWithDictionary:(NSDictionary *)dictionary;

@end
