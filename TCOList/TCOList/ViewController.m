//
//  ViewController.m
//  TCOList
//
//  Created by Yurii Bosov on 2/17/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"
#import "TCOModel.h"
#import "TCOCell.h"

@interface ViewController () <UITableViewDataSource, UITableViewDelegate> {
    NSArray *dataSources;
    
    IBOutlet UITableView *myTableView;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [TCOModel loadTCOForCity:@"Dnipro" complition:^(id data, NSString *errorMessage, BOOL canceled) {
        
        if (errorMessage) {
//            NSLog(@"%@", errorMessage);
            UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"Ошибка" message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
            
            [controller addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil]];
            
            [self presentViewController:controller animated:YES completion:nil];
            
        } else {
            dataSources = data;
            [myTableView reloadData];
        }
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataSources.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TCOCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TCOCell"];
    TCOModel *model = dataSources[indexPath.row];
    
    cell.lbAddress.text = model.fullAddressRu;
    cell.lbPlace.text = model.placeRu;
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100;
}

@end
