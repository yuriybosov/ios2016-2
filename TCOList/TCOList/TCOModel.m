//
//  TCOModel.m
//  TCOList
//
//  Created by Yurii Bosov on 2/17/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "TCOModel.h"
#import <AFNetworking.h>

@implementation TCOModel

+ (void)loadTCOForCity:(NSString *)city
            complition:(ComplitionBlock)complition {
    
    NSURL *url = [NSURL URLWithString:@"https://api.privatbank.ua/p24api"];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:url];
    manager.responseSerializer = [[AFJSONResponseSerializer alloc] init];
    
    NSCharacterSet *allowedCharacters = [NSCharacterSet URLFragmentAllowedCharacterSet];
    NSString *encodedString = [city stringByAddingPercentEncodingWithAllowedCharacters:allowedCharacters];
    
    NSString *path = [NSString stringWithFormat:@"infrastructure?json&tso&city=%@", encodedString];
    
    [manager GET:path parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSMutableArray *array = [NSMutableArray array];
        for (NSDictionary *modelData in responseObject[@"devices"]) {
            TCOModel *model = [TCOModel new];
            [model setupWithDictionary:modelData];
            [array addObject:model];
        }
        
        if (array.count) {
            if (complition) {
                complition(array, nil, NO);
            }
        } else {
            if (complition) {
                complition(nil, @"Нет данных", NO);
            }
        }
    
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSString *message = error.localizedDescription;
        
        if (complition){
            complition(nil, message, NO);
        }
        
    }];
}

- (void)setupWithDictionary:(NSDictionary *)dictionary {

    id value = dictionary[@"fullAddressRu"];
    if (![value isKindOfClass:[NSNull class]]) {
        self.fullAddressRu = value;
    }
    
    value = dictionary[@"placeRu"];
    if (![value isKindOfClass:[NSNull class]]) {
        self.placeRu = value;
    }
    
    value = dictionary[@"latitude"];
    if (![value isKindOfClass:[NSNull class]]) {
        self.latitude = value;
    }
    
    value = dictionary[@"longitude"];
    if (![value isKindOfClass:[NSNull class]]) {
        self.longitude = value;
    }
}

@end
