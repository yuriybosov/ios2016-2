//
//  TCOCell.h
//  TCOList
//
//  Created by Yurii Bosov on 2/17/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TCOCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *lbAddress;
@property (nonatomic, weak) IBOutlet UILabel *lbPlace;

@end
