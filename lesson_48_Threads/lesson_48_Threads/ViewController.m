//
//  ViewController.m
//  lesson_48_Threads
//
//  Created by Yurii Bosov on 2/26/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {
    NSMutableArray *dataSources;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    dataSources = [NSMutableArray new];
}

- (IBAction)buttonClicked:(id)sender {
    
    [self performSelectorInBackground:@selector(testMethod) withObject:nil];
    NSLog(@"%@", dataSources);
}

- (IBAction)button2Clicked:(id)sender {
    [self performSelectorInBackground:@selector(testMethodWithParams:) withObject:@"X"];
    [self performSelectorInBackground:@selector(testMethodWithParams:) withObject:@"O"];
    [self performSelectorInBackground:@selector(testMethodWithParams:) withObject:@"Y"];
}

- (void)testMethod {
    
    for (NSUInteger i = 0 ; i < 1000000000; i++) {
        ;
    }
    
    [self performSelectorOnMainThread:@selector(compliteMethod) withObject:nil waitUntilDone:NO];
}

- (void)compliteMethod{
    self.view.backgroundColor = [UIColor greenColor];
}

- (void)testMethodWithParams:(NSString *)param {
    
    @synchronized (self) {
        for (NSUInteger i = 0; i < 1000; i++) {
            [dataSources addObject:param];
        }
    }
    
    //
    [self performSelectorOnMainThread:@selector(compliteMethod2) withObject:nil waitUntilDone:NO];
}

- (void)compliteMethod2 {
    self.view.backgroundColor = [UIColor yellowColor];
}

@end
