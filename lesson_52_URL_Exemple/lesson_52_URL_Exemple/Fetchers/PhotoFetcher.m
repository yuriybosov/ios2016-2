//
//  PhotoFetcher.m
//  lesson_52_URL_Exemple
//
//  Created by Yurii Bosov on 3/23/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "PhotoFetcher.h"

@implementation PhotoFetcher

- (instancetype)initWithPerPage:(NSUInteger)aPerPage {
    self = [super init];
    if (self) {
        page = 0;
        perPage = aPerPage;
    }
    return self;
}

- (void)reset {
    page = 0;
    loadedAll = NO;
    clearDataAfterRefresh = YES;
}
- (BOOL)canLoadData {
    return !loadedAll && !curentTask;
}

- (void)loadDataComplition:(FetcherComplitionBlock)complition {
    
    curentTask = [[DataManager sharedInstance] photoItemsFromPage:page perPage:perPage complition:^(Responce *responce) {
        
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
           
            curentTask = nil;
            if (responce.success) {
                page++;
                loadedAll = [responce.data count] < perPage;
            } else {
                loadedAll = YES;
            }
            
            if (complition) {
                complition(responce, clearDataAfterRefresh);
            }
            
            clearDataAfterRefresh = NO;
            
        });
    }];
}

@end
