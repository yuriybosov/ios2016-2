//
//  PhotoFetcher.h
//  lesson_52_URL_Exemple
//
//  Created by Yurii Bosov on 3/23/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataManager.h"

typedef void(^FetcherComplitionBlock)(Responce *responce, BOOL clearDataAfterRefresh);

@interface PhotoFetcher : NSObject {
    NSInteger page;
    NSInteger perPage;
    BOOL loadedAll;
    BOOL clearDataAfterRefresh;
    
    NSURLSessionDataTask *curentTask;
}

- (instancetype)initWithPerPage:(NSUInteger)aPerPage;

- (void)reset;
- (BOOL)canLoadData;
- (void)loadDataComplition:(FetcherComplitionBlock)complition;

@end
