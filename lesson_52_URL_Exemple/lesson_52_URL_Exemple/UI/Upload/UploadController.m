//
//  UploadController.m
//  lesson_52_URL_Exemple
//
//  Created by Yurii Bosov on 3/12/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "UploadController.h"
#import "DataManager.h"
#import "PhotoChooserController.h"

@interface UploadController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate> {
    IBOutlet UIImageView *photoView;
    IBOutlet UIProgressView *progressView;
    IBOutlet UIButton *uploadButton;
}

@end

@implementation UploadController

- (void)dealloc {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        self.title = @"Upload";
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    progressView.progress = 0;
    uploadButton.enabled = NO;
}

#pragma mark - Button Actions

- (IBAction)photoButtonClicked:(id)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        [alert addAction:[UIAlertAction actionWithTitle:@"Take photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self showImagePickerWithSource:UIImagePickerControllerSourceTypeCamera];
        }]];
    }
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Choose photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self showImagePickerWithSource:UIImagePickerControllerSourceTypePhotoLibrary];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)photoChooserButtonClicked:(id)sender {
    PhotoChooserController *photoChooser = [self.storyboard instantiateViewControllerWithIdentifier:@"photoChooser"];
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:photoChooser];
    [self presentViewController:nc animated:YES completion:nil];
}

- (IBAction)uploadButtonClicked:(id)sender {
    
    uploadButton.enabled = NO;
    
    [[DataManager sharedInstance] uploadPhoto:photoView.image progress:^(NSProgress *uploadProgress) {
        
        [progressView setProgress: (float)uploadProgress.completedUnitCount / (float)uploadProgress.totalUnitCount  animated:YES];
        
    } complition:^(Responce *responce) {
        
        NSLog(@"data %@", responce.data);
        NSLog(@"message %@", responce.message);
        
        if (responce.success) {
            photoView.image = nil;
            progressView.progress = 0;
        } else {
            [progressView setProgress:0 animated:YES];
            uploadButton.enabled = YES;
        }
    }];
}

- (void)showImagePickerWithSource:(UIImagePickerControllerSourceType)type {
    UIImagePickerController *picker = [UIImagePickerController new];
    picker.sourceType = type;
    picker.delegate = self;
//    picker.allowsEditing = YES;
    
    [self presentViewController:picker animated:YES completion:nil];
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    
    UIImage *image = info[UIImagePickerControllerEditedImage] ?: info[UIImagePickerControllerOriginalImage];
    photoView.image = image;
    uploadButton.enabled = YES;
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

@end
