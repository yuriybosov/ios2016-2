//
//  PhotoItemCollectionCell.h
//  lesson_52_URL_Exemple
//
//  Created by Yurii Bosov on 3/26/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhotoItem.h"

@interface PhotoItemCollectionCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UILabel *lbTitle;
@property (nonatomic, weak) IBOutlet UILabel *lbText;

@property (nonatomic, strong) PhotoItem *item;

@end
