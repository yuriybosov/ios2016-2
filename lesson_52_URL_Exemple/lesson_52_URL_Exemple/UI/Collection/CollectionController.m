//
//  CollectionController.m
//  lesson_52_URL_Exemple
//
//  Created by Yurii Bosov on 3/12/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "CollectionController.h"
#import "PhotoFetcher.h"
#import "LoadMoreCollectionCell.h"
#import "PhotoItemCollectionCell.h"
#import <UICollectionViewLeftAlignedLayout.h>
#import <LGRefreshView.h>
#import "Settings.h"

@interface CollectionController () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout> {
    
    NSMutableArray *dataSource;
    PhotoFetcher *fetcher;
    LGRefreshView *refreshView;
}

@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;

@end

@implementation CollectionController

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        self.title = @"Collection";
    }
    return self;
}

- (void)dealloc {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    fetcher = [[PhotoFetcher alloc] initWithPerPage:5];
    dataSource = [NSMutableArray new];
    
    UICollectionViewLeftAlignedLayout *layout = [[UICollectionViewLeftAlignedLayout alloc] init];
    layout.minimumLineSpacing = 0;
    layout.minimumInteritemSpacing = 0;
    self.collectionView.collectionViewLayout = layout;
    
    __weak typeof(self) weakSelf = self;
    refreshView = [[LGRefreshView alloc] initWithScrollView:self.collectionView refreshHandler:^(LGRefreshView *aRefreshView) {
        
        __strong typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf->fetcher reset];
        
        [weakSelf fetchData];
    }];
    
    [self fetchData];
}

- (IBAction)logout:(id)sender {
    [[Settings sharedInstance] logout];
    [UIApplication sharedApplication].keyWindow.rootViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"loginNavigation"];
}

#pragma mark - Fetch Data

- (void)fetchData {
    if ([fetcher canLoadData]) {
        
        __weak typeof(self) weakSelf = self;
        [fetcher loadDataComplition:^(Responce *responce, BOOL clearDataAfterRefresh) {
            [weakSelf fetchDataComplited:responce
                   clearDataAfterRefresh:clearDataAfterRefresh];
        }];
    }
}

- (void)fetchDataComplited:(Responce *)responce
     clearDataAfterRefresh:(BOOL)clearData {
    
    [refreshView endRefreshing];
    
    if (responce.success) {
        if ([responce.data isKindOfClass:[NSArray class]] &&
            [responce.data count]) {
            
            if (clearData) {
                [dataSource removeAllObjects];
            }
            [dataSource addObjectsFromArray:responce.data];
        }
    }else{
#warning TODO show alert with error
        NSLog(@"error %@", responce.message);
    }
    
    [self.collectionView reloadData];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (dataSource.count) {
        return dataSource.count + ([fetcher canLoadData] ? 1 : 0);
    }
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = nil;
    
    if (dataSource.count == indexPath.row) {
        LoadMoreCollectionCell *moreCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"moreCell" forIndexPath:indexPath];
        [moreCell.indicatorView startAnimating];
        
        cell = moreCell;
        
    } else {
        PhotoItemCollectionCell *itemCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"itemCell" forIndexPath:indexPath];
        itemCell.item = dataSource[indexPath.item];
        
        cell = itemCell;
    }
    
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([cell isKindOfClass:[LoadMoreCollectionCell class]]) {
        [self fetchData];
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGSize size;
    
    // если у нас load more cell
    if (indexPath.item == dataSource.count) {
        size = CGSizeMake(collectionView.frame.size.width,
                          50);
    } else {
        size = CGSizeMake(collectionView.frame.size.width/3,
                          collectionView.frame.size.width/3);
    }
    
    return size;
}

@end
