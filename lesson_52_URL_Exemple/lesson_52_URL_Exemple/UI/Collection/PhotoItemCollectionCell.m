//
//  PhotoItemCollectionCell.m
//  lesson_52_URL_Exemple
//
//  Created by Yurii Bosov on 3/26/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "PhotoItemCollectionCell.h"

@implementation PhotoItemCollectionCell

- (void)setItem:(PhotoItem *)item {
    _item = item;
    self.lbTitle.text = _item.name;
    self.lbText.text = _item.ID.description;
}

@end
