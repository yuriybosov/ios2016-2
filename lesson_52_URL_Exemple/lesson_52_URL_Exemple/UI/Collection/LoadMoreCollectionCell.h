//
//  LoadMoreCollectionCell.h
//  lesson_52_URL_Exemple
//
//  Created by Yurii Bosov on 3/26/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadMoreCollectionCell : UICollectionViewCell

@property (nonatomic, weak)IBOutlet UIActivityIndicatorView *indicatorView;

@end
