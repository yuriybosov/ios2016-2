//
//  TabBarController.h
//  lesson_52_URL_Exemple
//
//  Created by Yurii Bosov on 3/12/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TabBarController : UITabBarController

@end
