//
//  StartController.m
//  lesson_52_URL_Exemple
//
//  Created by Yurii Bosov on 3/16/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "StartController.h"
#import "Settings.h"

@interface StartController ()

@end

@implementation StartController

- (void)dealloc {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    UIViewController *controller = nil;
    
    // проверяем наличие залогиненого пользователя
    if ([[Settings sharedInstance] isAuthorizedUser]) {
        // показать таббар контроллер
        controller = [self.storyboard instantiateViewControllerWithIdentifier:@"tabbar"];
    } else {
        // показать логин контроллер
        controller = [self.storyboard instantiateViewControllerWithIdentifier:@"loginNavigation"];
    }
    
    [UIApplication sharedApplication].keyWindow.rootViewController = controller;
}

@end
