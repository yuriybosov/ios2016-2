//
//  LoginController.m
//  lesson_52_URL_Exemple
//
//  Created by Yurii Bosov on 3/16/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "LoginController.h"
#import "DataManager.h"

@interface LoginController () <UITextFieldDelegate> {
    IBOutlet UITextField *login;
    IBOutlet UITextField *password;
}

@end

@implementation LoginController

- (void)dealloc {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Login";
}

#pragma mark - Button Actions

- (IBAction)loginButtonClicked:(id)sender {
    //1. validate input data
    //2. send login request
    
#warning TODO - repleace validate methods
    if (login.text.length > 0 &&
        password.text.length > 0) {
        
        [[DataManager sharedInstance] autorizationWithLogin:login.text password:password.text complition:^(Responce *responce) {
            if (responce.success) {
                [UIApplication sharedApplication].keyWindow.rootViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"tabbar"];
            }else {
#warning TODO - show error alert
                NSLog(@"%@", responce.message);
            }
        }];
        
    } else {
#warning TODO - show error alert
        NSLog(@"login validate error!!!");
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == login) {
        [password becomeFirstResponder];
    } else {
        [password resignFirstResponder];
        [self loginButtonClicked:nil];
    }
    return NO;
}

@end
