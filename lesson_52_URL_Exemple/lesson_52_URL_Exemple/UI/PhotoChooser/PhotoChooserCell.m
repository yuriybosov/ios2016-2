//
//  PhotoChooserCell.m
//  lesson_52_URL_Exemple
//
//  Created by Yurii Bosov on 4/2/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "PhotoChooserCell.h"

@implementation PhotoChooserCell

- (void)setAsset:(PHAsset *)asset {
    _asset = asset;
    
    [[PHImageManager defaultManager] requestImageForAsset:_asset targetSize:self.photoView.frame.size contentMode:PHImageContentModeAspectFill options:nil resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
        self.photoView.image = result;
    }];
}

@end
