//
//  PhotoChooserController.m
//  lesson_52_URL_Exemple
//
//  Created by Yurii Bosov on 4/2/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "PhotoChooserController.h"
#import <Photos/Photos.h>
#import "PhotoChooserCell.h"

@interface PhotoChooserController () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout> {
    
    PHFetchResult *dataSource;
    NSMutableArray *ddd;
    IBOutlet UIButton *appSettingsButton;
    IBOutlet UICollectionView *collectionView;
}

@end

@implementation PhotoChooserController

- (void)viewDidLoad {
    [super viewDidLoad];
    appSettingsButton.hidden = YES;

    ddd = [NSMutableArray new];
    
    [self checkStatus];
}

#pragma mark - Actions

- (IBAction)cancelButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)openAppSettings:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString] options:@{} completionHandler:nil];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return dataSource.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)aCollectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    PhotoChooserCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PhotoChooserCell" forIndexPath:indexPath];
    
    cell.asset = [dataSource objectAtIndex:indexPath.item];
    
    return cell;
}

#pragma mark - Photos

- (void)checkStatus {
    // проверяем статус авторизации
    PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
    
    switch (status) {
        case PHAuthorizationStatusNotDetermined:
            // нужно выполнить запрос авторизации
            [self authorizedRequest];
            break;
        case PHAuthorizationStatusDenied:
        case PHAuthorizationStatusRestricted:
            // предложить юзеру перейти в настройки приложения и все таки дать доступ :)
            [self showDeniedInfo];
            break;
            
        case PHAuthorizationStatusAuthorized:
            // можно работать с фотогалереей пользователя, как чтение так и запись
            [self photosRequest];
            break;
    }
}

- (void)authorizedRequest {
    //  выполняем запрос на разрешение пользоваться фотоголереей
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self checkStatus];
        });
    }];
}

- (void)showDeniedInfo {
    // отобразить кнопку, которая выполнить переход на настройки приложения
    appSettingsButton.hidden = NO;
}

- (void)photosRequest {
    // разрешение получено, можно получить фото пользователя
    
    // 1. делаем выборку фотографий
    dataSource = [PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeImage options:nil];
    // 2. обновляем коллекцию
    [collectionView reloadData];
}

@end
