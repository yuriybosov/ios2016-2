//
//  PhotoChooserCell.h
//  lesson_52_URL_Exemple
//
//  Created by Yurii Bosov on 4/2/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>
@import Photos;

@interface PhotoChooserCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UIImageView *photoView;
@property (nonatomic, strong) PHAsset *asset;

@end
