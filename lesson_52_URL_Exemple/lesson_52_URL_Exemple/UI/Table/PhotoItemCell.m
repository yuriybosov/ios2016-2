//
//  PhotoItemCell.m
//  lesson_52_URL_Exemple
//
//  Created by Yurii Bosov on 3/23/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "PhotoItemCell.h"
#import <UIImageView+AFNetworking.h>
#import <AFImageDownloader.h>

@implementation PhotoItemCell

- (void)setItem:(PhotoItem *)item {
    _item = item;
    self.lbTitle.text = _item.name;
    self.lbText.text = _item.ID.description;
    
    NSLog(@"dateCreated %@", _item.dateCreated);
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[_item pathURL]];

    NSString *requestHeaders = [UIImageView sharedImageDownloader].sessionManager.requestSerializer.HTTPRequestHeaders[@"Authorization"];
    if (requestHeaders) {
        [request setValue:requestHeaders forHTTPHeaderField:@"Authorization"];
    }
    
    [self.photoView setImageWithURLRequest:request placeholderImage:[UIImage new] success:nil failure:nil];
}

@end
