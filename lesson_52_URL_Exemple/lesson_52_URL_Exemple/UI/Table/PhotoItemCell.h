//
//  PhotoItemCell.h
//  lesson_52_URL_Exemple
//
//  Created by Yurii Bosov on 3/23/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhotoItem.h"

@interface PhotoItemCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *lbTitle;
@property (nonatomic, weak) IBOutlet UILabel *lbText;
@property (nonatomic, weak) IBOutlet UIImageView *photoView;

@property (nonatomic, strong) PhotoItem *item;

@end
