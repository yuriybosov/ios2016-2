//
//  TableController.m
//  lesson_52_URL_Exemple
//
//  Created by Yurii Bosov on 3/12/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "TableController.h"
#import "PhotoFetcher.h"
#import "LoadMoreCell.h"
#import "PhotoItemCell.h"
#import <STZPullToRefresh.h>

@interface TableController () <UITableViewDataSource, UITableViewDelegate, STZPullToRefreshDelegate> {
    
    PhotoFetcher *fetcher;
    NSMutableArray *dataSource;
    STZPullToRefresh *pullToRefresh;
}

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end

@implementation TableController

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        self.title = @"Table";
    }
    return self;
}

- (void)dealloc {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    STZPullToRefreshView *refreshView = [[STZPullToRefreshView alloc] initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, 3)];
    [self.view addSubview:refreshView];
    
    pullToRefresh = [[STZPullToRefresh alloc] initWithTableView:self.tableView refreshView:refreshView tableViewDelegate:self];
    pullToRefresh.delegate = self;
    self.tableView.delegate = pullToRefresh;
    
    fetcher = [[PhotoFetcher alloc] initWithPerPage:5];
    dataSource = [NSMutableArray new];
    
    [self fetchData];
}

#pragma mark - Fetch Data

- (void)fetchData {
    if ([fetcher canLoadData]) {
        
        __weak typeof(self) weakSelf = self;
        [fetcher loadDataComplition:^(Responce *responce, BOOL clearDataAfterRefresh) {
            [weakSelf fetchDataComplited:responce
                   clearDataAfterRefresh:clearDataAfterRefresh];
        }];
    }
}

- (void)fetchDataComplited:(Responce *)responce
     clearDataAfterRefresh:(BOOL)clearData {
    
    [pullToRefresh finishRefresh];
    
    if (responce.success) {
        if ([responce.data isKindOfClass:[NSArray class]] &&
            [responce.data count]) {
            
            if (clearData) {
                [dataSource removeAllObjects];
            }
            [dataSource addObjectsFromArray:responce.data];
        }
    }else{
#warning TODO show alert with error
        NSLog(@"error %@", responce.message);
    }
    
    [self.tableView reloadData];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (dataSource.count) {
        return dataSource.count + ([fetcher canLoadData] ? 1 : 0);
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = nil;
    
    if (indexPath.row == dataSource.count) {
        LoadMoreCell *moreCell = [tableView dequeueReusableCellWithIdentifier:@"moreCell"];
        [moreCell.indicatorView startAnimating];
        
        cell = moreCell;
    } else {
        PhotoItemCell *itemCell = [tableView dequeueReusableCellWithIdentifier:@"itemCell"];
        itemCell.item = dataSource[indexPath.row];
        
        cell = itemCell;
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([cell isKindOfClass:[LoadMoreCell class]]) {
        [self fetchData];
    }
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

#pragma mark - STZPullToRefreshDelegate

- (void)pullToRefreshDidStart {
    [fetcher reset];
    [self fetchData];
}

@end
