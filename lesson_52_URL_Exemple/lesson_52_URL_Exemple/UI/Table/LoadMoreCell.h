//
//  LoadMoreCell.h
//  lesson_52_URL_Exemple
//
//  Created by Yurii Bosov on 3/23/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadMoreCell : UITableViewCell

@property (nonatomic, weak)IBOutlet UIActivityIndicatorView *indicatorView;

@end
