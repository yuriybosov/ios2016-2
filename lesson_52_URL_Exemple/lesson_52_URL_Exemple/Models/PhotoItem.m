//
//  PhotoItem.m
//  lesson_52_URL_Exemple
//
//  Created by Yurii Bosov on 3/23/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "PhotoItem.h"
#import "Constants.h"

@implementation PhotoItem

+ (JSONKeyMapper *)keyMapper {
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"ID":@"id", @"size":@"size", @"name":@"name", @"dateCreated":@"date_create",@"path":@"path"}];
}

+ (NSDateFormatter *)yyyyMMDDHHmmssDateFormatter {
    
    static NSDateFormatter *dateFormatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dateFormatter = [NSDateFormatter new];
                                   //2017-03-16 12:05:06
        dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    });
    return dateFormatter;
}

- (NSURL *)pathURL {
    return [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", kBaseURL, self.path]];
}

- (void)setDateCreatedWithNSString:(NSString *)string {
    
    _dateCreated = [[PhotoItem yyyyMMDDHHmmssDateFormatter] dateFromString:string];
}

- (NSString *)JSONObjectForDateCreated {
    return [[PhotoItem yyyyMMDDHHmmssDateFormatter] stringFromDate:_dateCreated];
}

@end
