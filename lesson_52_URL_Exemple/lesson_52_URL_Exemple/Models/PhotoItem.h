//
//  PhotoItem.h
//  lesson_52_URL_Exemple
//
//  Created by Yurii Bosov on 3/23/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <JSONModel/JSONModel.h>

//{"id":1,"name":"123.jpg","size":47484,"date_create":"2017-03-16 12:02:34"}

@interface PhotoItem : JSONModel

@property (nonatomic, strong) NSNumber <Optional> *ID;
@property (nonatomic, strong) NSNumber <Optional> *size;

@property (nonatomic, strong) NSString <Optional> *name;
@property (nonatomic, strong) NSDate <Optional> *dateCreated;

@property (nonatomic, strong) NSString <Optional> *path;

- (NSURL *)pathURL;

@end
