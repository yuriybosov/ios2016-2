//
//  NSDictionary+NSNull.m
//  lesson_51_1_NSUserDefault_Exemple
//
//  Created by Yurii Bosov on 3/12/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "NSDictionary+NSNull.h"

@implementation NSDictionary (NSNull)

- (id)objectForKeyNotNull:(id)aKey {
    return [self[aKey] isKindOfClass:[NSNull class]] ? nil : self[aKey];
}

@end
