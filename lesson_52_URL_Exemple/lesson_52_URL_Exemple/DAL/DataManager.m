//
//  DataManager.m
//  lesson_52_URL_Exemple
//
//  Created by Yurii Bosov on 3/16/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "DataManager.h"
#import "Settings.h"
#import <AFNetworking.h>
#import "NSDictionary+NSNull.h"
#import "PhotoItem.h"
#import "Constants.h"
#import <UIImageView+AFNetworking.h>
#import <AFImageDownloader.h>

@interface DataManager () {
    AFHTTPSessionManager *sessionManager;
}

@end


@implementation DataManager

+ (instancetype)sharedInstance {
    static DataManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [DataManager new];
    });
    return manager;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        sessionManager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:kBaseURL]];
        if ([Settings sharedInstance].isAuthorizedUser) {
            [sessionManager.requestSerializer setAuthorizationHeaderFieldWithUsername:[Settings sharedInstance].login password:[Settings sharedInstance].password];
            
            [[UIImageView sharedImageDownloader].sessionManager.requestSerializer setAuthorizationHeaderFieldWithUsername:[Settings sharedInstance].login password:[Settings sharedInstance].password];
        }
        
//        AFImageResponseSerializer* serializer = (AFImageResponseSerializer*)[UIImageView sharedImageDownloader].sessionManager.responseSerializer;
//        serializer.acceptableContentTypes = [serializer.acceptableContentTypes setByAddingObject:@"image/jpeg"];
//        serializer.acceptableContentTypes = [serializer.acceptableContentTypes setByAddingObject:@"image/jpeg"];
    }
    return self;
}

- (Responce *)preprocessSuccessData:(id)responseData {
    Responce *responce = [[Responce alloc] init];
    responce.code = 200;
    if ([responseData isKindOfClass:[NSDictionary class]]) {
        
        // 1. success
        responce.success = [[responseData objectForKeyNotNull:@"status"] isEqualToString:@"ok"];
        // 2. message
        responce.message = [responseData objectForKeyNotNull:@"message"];
        // 3. data
        responce.data = [responseData objectForKeyNotNull:@"data"];
    }
    return responce;
}

- (Responce *)preprocessErrorData:(NSError *)error {
    Responce *responce = [[Responce alloc] init];
    responce.success = NO;
    responce.code = error.code;
    responce.message = error.localizedDescription;
    return responce;
}

- (void)clearAuthorizationHeader {
    [sessionManager.requestSerializer clearAuthorizationHeader];
    [[UIImageView sharedImageDownloader].sessionManager.requestSerializer clearAuthorizationHeader];
}

- (NSURLSessionDataTask *)autorizationWithLogin:(NSString *)login password:(NSString *)password complition:(ComplitionBlock)complition {
    
    [sessionManager.requestSerializer setAuthorizationHeaderFieldWithUsername:login password:password];
    
    [[UIImageView sharedImageDownloader].sessionManager.requestSerializer setAuthorizationHeaderFieldWithUsername:[Settings sharedInstance].login password:[Settings sharedInstance].password];
    
    NSURLSessionDataTask *task = [sessionManager POST:@"student/login" parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        Responce *response = [self preprocessSuccessData:responseObject];
        if (response.success) {
            [[Settings sharedInstance] setLogin:login password:password];
        } else {
            [self clearAuthorizationHeader];
        }
        
        if (complition) {
            complition(response);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        Responce *response = [self preprocessErrorData:error];
        if (complition) {
            complition(response);
        }
    }];
    
    return task;
}

- (NSURLSessionDataTask *)uploadPhoto:(UIImage *)photo
                             progress:(ProgressBlock)progress
                           complition:(ComplitionBlock)complition {

    NSURLSessionDataTask *task = [sessionManager POST:@"photo/upload" parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        // set image to form data
        
        // 1. image to NSData
        NSData *data = UIImageJPEGRepresentation(photo, 0);
        NSLog(@"data.length = %lu", data.length);
        
        // 2. set data to formData
        [formData appendPartWithFileData:data
                                    name:@"image"
                                fileName:@"image.jpeg"
                                mimeType:@"image/jpeg"];
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (progress)
                progress(uploadProgress);
        });
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        Responce *response = [self preprocessSuccessData:responseObject];
        if (complition) {
            complition(response);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        Responce *response = [self preprocessErrorData:error];
        if (complition) {
            complition(response);
        }
    }];
    
    return task;
}

- (NSURLSessionDataTask *)photoItemsFromPage:(NSInteger)page perPage:(NSInteger)perPage complition:(ComplitionBlock)complition {
    
    NSString *path = @"photo";
    NSDictionary *params = @{@"page":@(page), @"per_page":@(perPage)};
    
    if (DEBUG){
        NSLog(@"path = %@", path);
        NSLog(@"params = %@", params);
    }
    
    NSURLSessionDataTask *task = [sessionManager POST:path parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        Responce *response = [self preprocessSuccessData:responseObject];
        
        if (response.success && [response.data isKindOfClass:[NSArray class]]) {
            // парсим данные
            NSError *error = nil;
            NSArray *photoItems = [PhotoItem arrayOfModelsFromDictionaries:response.data error:&error];
            if (error) {
                response.success = NO;
                response.message = error.localizedDescription;
            } else{
                response.data = photoItems;
            }
        }
        
        if (complition)
            complition(response);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        Responce *response = [self preprocessErrorData:error];
        if (complition) {
            complition(response);
        }
        
    }];
    
    return task;
}

@end
