//
//  Settings.h
//  lesson_52_URL_Exemple
//
//  Created by Yurii Bosov on 3/16/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Settings : NSObject


+ (instancetype)sharedInstance;

@property (nonatomic, readonly) NSString *login;
@property (nonatomic, readonly) NSString *password;
@property (nonatomic, readonly, getter=isAuthorizedUser) BOOL authorizedUser;

- (void)setLogin:(NSString *)login password:(NSString *)password;
- (void)logout;

@end
