//
//  Settings.m
//  lesson_52_URL_Exemple
//
//  Created by Yurii Bosov on 3/16/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "Settings.h"

@implementation Settings

+ (instancetype)sharedInstance {
    static Settings *setting = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        setting = [Settings new];
    });
    
    return setting;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _login = [[NSUserDefaults standardUserDefaults] objectForKey:@"login"];
        _password = [[NSUserDefaults standardUserDefaults] objectForKey:@"password"];
    }
    return self;
}

- (void)setLogin:(NSString *)aLogin password:(NSString *)aPassword {
    _login = aLogin;
    _password = aPassword;
    [[NSUserDefaults standardUserDefaults] setObject:_login forKey:@"login"];
    [[NSUserDefaults standardUserDefaults] setObject:_password forKey:@"password"];
}

- (void)logout {
    _login = nil;
    _password = nil;
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"login"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"password"];
}

- (BOOL)isAuthorizedUser {
    return (_login != nil && _password != nil);
}

@end
