//
//  DataManager.h
//  lesson_52_URL_Exemple
//
//  Created by Yurii Bosov on 3/16/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Responce.h"

typedef void(^ComplitionBlock)(Responce *responce);
typedef void(^ProgressBlock)(NSProgress *uploadProgress);

@interface DataManager : NSObject

+ (instancetype)sharedInstance;

- (void)clearAuthorizationHeader;
- (NSURLSessionDataTask *)autorizationWithLogin:(NSString *)login
                                       password:(NSString *)password
                                     complition:(ComplitionBlock)complition;

- (NSURLSessionDataTask *)uploadPhoto:(UIImage *)photo
                             progress:(ProgressBlock)progress
                           complition:(ComplitionBlock)complition;

- (NSURLSessionDataTask *)photoItemsFromPage:(NSInteger)page perPage:(NSInteger)perPage complition:(ComplitionBlock)complition;

@end
