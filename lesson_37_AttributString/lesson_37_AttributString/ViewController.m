//
//  ViewController.m
//  lesson_37_AttributString
//
//  Created by Yurii Bosov on 12/11/16.
//  Copyright © 2016 ios. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {
    IBOutlet UILabel *label;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *firstName = @"Yurii";
    NSString *lastName = @"Bosov";
    NSString *ageString = @"31";
    NSString *phone = @"+380505350926";
    
    NSString *totalString = [NSString stringWithFormat:@"Имя: %@\nФамилия: %@\nВозраст: %@\nТелефон: %@", firstName, lastName, ageString, phone];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:totalString attributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:24 ],NSForegroundColorAttributeName:[UIColor grayColor]}];
    
    // будем устанавливать атрибуты для конкретных диапазонов
    // для Имени
    NSRange range = [totalString rangeOfString:firstName];
    if (range.location != NSNotFound) {
        [attributedString addAttributes:@{NSForegroundColorAttributeName:[UIColor redColor]} range:range];
    }
    
    // для Фамилии
    range = [totalString rangeOfString:lastName];
    if (range.location != NSNotFound) {
        [attributedString addAttributes:@{NSForegroundColorAttributeName:[UIColor greenColor]} range:range];
    }
    
    // для Возраста
    range = [totalString rangeOfString:ageString];
    if (range.location != NSNotFound) {
        [attributedString addAttributes:@{NSForegroundColorAttributeName:[UIColor purpleColor], NSFontAttributeName:[UIFont fontWithName:@"TimesNewRomanPS-ItalicMT" size:36]} range:range];
    }
    
    // для телефона
    range = [totalString rangeOfString:phone];
    if (range.location != NSNotFound) {
        [attributedString addAttributes:@{NSLinkAttributeName:@"tel://380505350926"} range:range];
    }
    
    // NSLinkAttributeName ???
    label.attributedText = attributedString;
}

@end
