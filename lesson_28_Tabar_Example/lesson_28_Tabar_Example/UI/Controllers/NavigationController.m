//
//  NavigationController.m
//  lesson_28_Tabar_Example
//
//  Created by Yurii Bosov on 11/3/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import "NavigationController.h"

@interface NavigationController ()

@end

@implementation NavigationController

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return [self.topViewController supportedInterfaceOrientations];
}

@end
