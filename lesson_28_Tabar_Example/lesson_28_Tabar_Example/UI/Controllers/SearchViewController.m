//
//  SearchViewController.m
//  lesson_28_Tabar_Example
//
//  Created by Yurii Bosov on 11/3/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import "SearchViewController.h"

@interface SearchViewController ()

@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Search";
}

@end
