//
//  FavoriteViewController.m
//  lesson_28_Tabar_Example
//
//  Created by Yurii Bosov on 11/3/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import "FavoriteViewController.h"
#import "ItemCell.h"

@interface FavoriteViewController ()

@end

@implementation FavoriteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Favorites";
    
    // регистрируем nib для cell id
    [self.tableView registerNib:[UINib nibWithNibName:@"ItemCell" bundle:nil] forCellReuseIdentifier:@"ItemCell"];
}

#pragma mark - UITableViewDataSources

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return nil;
}

@end
