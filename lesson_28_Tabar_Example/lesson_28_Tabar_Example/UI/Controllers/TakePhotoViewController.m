//
//  TakePhotoViewController.m
//  lesson_28_Tabar_Example
//
//  Created by Yurii Bosov on 11/3/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import "TakePhotoViewController.h"

@interface TakePhotoViewController ()

@end

@implementation TakePhotoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Take Photo";
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (IBAction)doneButtonClicked:(id)sender {
    [self dismissViewControllerAnimated:YES
                             completion:nil];
}

@end
