//
//  TabBarController.m
//  lesson_28_Tabar_Example
//
//  Created by Yurii Bosov on 11/3/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import "TabBarController.h"

@interface TabBarController () <UITabBarControllerDelegate>

@end

@implementation TabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.delegate = self;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - UITabBarControllerDelegate

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {

    BOOL result = YES;
    
    if ([viewController.restorationIdentifier isEqualToString:@"takePhoto"]) {
        result = NO;
        
        // show take photo controller
        UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"takePhotoNavigationController"];
        [self presentViewController:controller
                           animated:YES
                         completion:nil];
    }
    
    return result;
    
//    NSUInteger index = [self.viewControllers indexOfObject:viewController];
//
//    return index == 2 ? NO : YES;
}

@end
