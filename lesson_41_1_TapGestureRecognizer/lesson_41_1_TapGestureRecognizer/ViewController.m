//
//  ViewController.m
//  lesson_41_1_TapGestureRecognizer
//
//  Created by Yurii Bosov on 12/25/16.
//  Copyright © 2016 ios. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {
    IBOutlet UIImageView *imageView1;
    IBOutlet UIImageView *imageView2;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // добавляем на imageView1 и на imageView2 тап-жетсы, указав методы, которые вызовутся при выполнении этих жестов
    
    // создаем жест
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapImageView1:)];
    // добавляем жест на вью
    [imageView1 addGestureRecognizer:tapRecognizer];

    // тоже самое делаем для второй вью
    tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapImageView2:)];
    tapRecognizer.numberOfTapsRequired = 5;
    tapRecognizer.numberOfTouchesRequired = 2;
    
    [imageView2 addGestureRecognizer:tapRecognizer];
    
    // тап жесты не будут работать на вью у которых выключено userInteractionEnabled. по этому включаем это свойство
    imageView1.userInteractionEnabled = YES;
    imageView2.userInteractionEnabled = YES;
    imageView2.multipleTouchEnabled = YES;
}

#pragma makr - Action

- (void)didTapImageView1:(UITapGestureRecognizer *)sender {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (void)didTapImageView2:(UITapGestureRecognizer *)sender {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

@end
