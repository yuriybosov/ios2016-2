//
//  main.m
//  lesson_41_1_TapGestureRecognizer
//
//  Created by Yurii Bosov on 12/25/16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
