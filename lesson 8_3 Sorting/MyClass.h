//
//  MyClass.h
//  lesson 8_3 Sorting
//
//  Created by Yuriy Bosov on 8/28/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyClass : NSObject

@property (nonatomic, assign) NSUInteger value;

// метод сравнения двух объектов класса MyClass
- (NSComparisonResult)myCompare:(MyClass *)obj;

@end
