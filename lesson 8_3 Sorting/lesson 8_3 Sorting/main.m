//
//  main.m
//  lesson 8_3 Sorting
//
//  Created by Yuriy Bosov on 8/28/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyClass.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        NSArray *array1 = @[@"qwer",@"asdf",@"zxcv"];
        NSArray *array2 = @[@(-100),@(200),@(-300),@(400)];
        
        // сортировка массива
        // 1. сортировка строк \ чисел
        NSArray *resultArray = [array1 sortedArrayUsingSelector:@selector(compare:)];
        NSLog(@"resultArray = %@", resultArray);
        
        resultArray = [array2 sortedArrayUsingSelector:@selector(compare:)];
        NSLog(@"resultArray = %@", resultArray);
        
        // 2.сортировка строк \ чисел в обратном порядке используя NSSortDescriptor
        NSSortDescriptor *sd = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:NO selector:@selector(compare:)];
        resultArray = [array2 sortedArrayUsingDescriptors:@[sd]];
        NSLog(@"resultArray = %@", resultArray);
        
        // 3. сортиврока кастомных объектов с помощью кастомного селектора сравнения
        MyClass *obj1 = [MyClass new];
        obj1.value = 200;
        MyClass *obj2 = [MyClass new];
        obj2.value = 1000;
        MyClass *obj3 = [MyClass new];
        obj3.value = 50;
        MyClass *obj4 = [MyClass new];
        obj4.value = 100;
        
        
        NSArray *array3 = @[obj1, obj2, obj3, obj4];
        resultArray = [array3 sortedArrayUsingSelector:@selector(myCompare:)];
        NSLog(@"resultArray = %@", resultArray);
    }
    return 0;
}
