//
//  MyClass.m
//  lesson 8_3 Sorting
//
//  Created by Yuriy Bosov on 8/28/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "MyClass.h"

@implementation MyClass

- (NSComparisonResult)myCompare:(MyClass *)obj {
    
    if (self.value < obj.value) {
        return NSOrderedAscending;
    } else if (self.value > obj.value) {
        return NSOrderedDescending;
    } else {
        return NSOrderedSame;
    }
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%lu", self.value];
}

@end
