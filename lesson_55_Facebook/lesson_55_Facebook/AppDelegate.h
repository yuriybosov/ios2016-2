//
//  AppDelegate.h
//  lesson_55_Facebook
//
//  Created by Yurii Bosov on 5/18/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

