//: Playground - noun: a place where people can play

import UIKit

protocol MyProtocol {
    func func1()
    func func2() -> Bool
}

class A : NSObject {
    fileprivate let a: Int
    private     let b: Int
    
    init(_ a: Int, _ b: Int) {
        self.a = a
        self.b = b
    }
    
    override var description: String {
        return "class \(A.self), a=\(a), b=\(b)"
    }
    
//    func func1() {
//        print("\(a)")
//    }
//    
//    func func2() -> Bool {
//        print("\(b)")
//        return true
//    }
}

extension A : MyProtocol {
    func func1() {
        print("!!! - \(a)")
    }
    
    func func2() -> Bool {
        print("???")
        return true
    }
}

class B : A {
    let c: Int
    
    var response: [String: Any?]?
    
    init(_ a: Int, _ b: Int, _ c: Int) {
        self.c = c
        super.init(a, b)
    }
    override init(_ a: Int, _ b: Int) {
        self.c = 0
        super.init(a, b)
    }
    init(){
        self.c = 0
        super.init(0, 0)
    }
    
    func test() {
        response = [String:Any?]()
        response?["1"] = nil
        response?["2"] = "qqq"
        response?["3"] = "www"
        
        print("\(response!)")
    }
}

let obj = A(10, 20)

obj.func1()
obj.func2()

let objB = B(10, 20, 30)
objB.test()

