//
//  ClassA.m
//  lesson 3_custom_class
//
//  Created by Yuriy Bosov on 7/14/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "ClassA.h"

@implementation ClassA

+ (void)staticMethod1 {
    NSLog(@"staticMethod1");
}

+ (BOOL)staticMethod2 {
    NSLog(@"staticMethod2");
    
    BOOL result = NO;
    return result;
}

- (void)method1 {
    NSLog(@"method1");
    
    _property2 = 400;
}

- (BOOL)method2 {
    NSLog(@"method2");
    
    BOOL result = NO;
    return result;
}

- (void)methodWithArgument1:(NSInteger)arg1 {
    NSLog(@"methodWithArgument1 = %li", arg1);
}

- (void)methodWithArgument1:(CGFloat)arg1
              withArgument2:(CGFloat)arg2 {
    
    NSLog(@"methodWithArgument1 = %f withArgument2 = %f", arg1, arg2);
}

- (void)methodWithArgument1:(BOOL)arg1
              withArgument2:(BOOL)arg2
              withArgument3:(BOOL)arg3 {
    NSLog(@"methodWithArgument1 = %i withArgument2 = %i withArgument3 = %i", arg1, arg2, arg3);
}

- (NSInteger)property1 {
    return _property1;
}

// переопределение метода родительского класса
- (NSString *)description {
    return @"description classa A";
}

@end
