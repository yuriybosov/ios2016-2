//
//  main.m
//  lesson 3_custom_class
//
//  Created by Yuriy Bosov on 7/14/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ClassA.h"
#import "ClassC.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        // обьявление переменной objA и ее инициализация
        ClassA *objA = [[ClassA alloc] init];
        ClassC *objC = [[ClassC alloc] init];
        
        [ClassA staticMethod1];
        BOOL b = [ClassA staticMethod2];
        
        [objA method1];
        b = [objA method2];
        
        [objA methodWithArgument1:10];
        
        [objA methodWithArgument1:15.1
                    withArgument2:100.35];
        
        [objA methodWithArgument1:YES
                    withArgument2:NO
                    withArgument3:NO];
        
        // меняем значение свойству
        objA.property1 = 200;
        // аналогичная операция
//        [objA setProperty1:200];
        
        // вычитываем значение свойства
        NSLog(@"property1 = %li", objA.property1);
        // аналогичная операция
//        NSLog(@"property1 = %li", [objA property1]);
        
        NSLog(@"%@", [objA description]);
        NSLog(@"%@", [objC description]);
    }
    return 0;
}
