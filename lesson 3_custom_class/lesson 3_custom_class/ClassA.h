//
//  ClassA.h
//  lesson 3_custom_class
//
//  Created by Yuriy Bosov on 7/14/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ClassA : NSObject
{
    // переменные класса
    @protected
    NSInteger a;
    
    @private
    NSInteger b;
}

// свойства класса
@property (nonatomic, assign) NSInteger property1;
@property (nonatomic, readonly) NSInteger property2;

// методы класса
// < + или - > (<тип возвращаемого значения>) <название метода>
// где, + указывает что метод статический, - указывает что данный метод можно вызывать у объекта

+ (void)staticMethod1;
+ (BOOL)staticMethod2;

- (void)method1;
- (BOOL)method2;

// метод с один агрументов
- (void)methodWithArgument1:(NSInteger)arg1;

// метод с двумя агрументами
- (void)methodWithArgument1:(CGFloat)arg1
              withArgument2:(CGFloat)agr2;


// метод с тремя агрументами
- (void)methodWithArgument1:(BOOL)arg1
              withArgument2:(BOOL)agr2
              withArgument3:(BOOL)agr3;

@end
