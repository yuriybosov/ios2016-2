//
//  ClassC.m
//  lesson 3_custom_class
//
//  Created by Yuriy Bosov on 7/14/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "ClassC.h"

@implementation ClassC

// расширение метода родительского класса
- (NSString *)description {
    
    NSString *superDescription = [super description];
    NSString *selfDescription = @"description classa C";
    
    return [NSString stringWithFormat:@"%@, %@", superDescription, selfDescription];
}

@end
