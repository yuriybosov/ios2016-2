//
//  AppDelegate.h
//  lesson_39_UITextView
//
//  Created by Yurii Bosov on 12/18/16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

