//
//  ViewController.m
//  lesson_39_UITextView
//
//  Created by Yurii Bosov on 12/18/16.
//  Copyright © 2016 ios. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UITextViewDelegate> {
    IBOutlet UITextView *textView;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

#pragma mark - Action

- (IBAction)hideKeyboard:(id)sender {
    [self.view endEditing:YES];
}

#pragma mark - UITextViewDelegate

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (BOOL)textView:(UITextView *)aTextView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // добавим на return key событие resignFirstResponder (спрячем клаву)
    NSRange newLineRange = [text rangeOfCharacterFromSet:[NSCharacterSet newlineCharacterSet]];
    if  (newLineRange.location != NSNotFound &&
         text.length == 1) {
        [textView resignFirstResponder];
    }
    
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

@end
