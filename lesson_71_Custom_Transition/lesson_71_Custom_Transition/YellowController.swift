//
//  YellowController.swift
//  lesson_71_Custom_Transition
//
//  Created by Yurii Bosov on 7/6/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

import UIKit

class YellowController: BaseController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .yellow
    }
}
