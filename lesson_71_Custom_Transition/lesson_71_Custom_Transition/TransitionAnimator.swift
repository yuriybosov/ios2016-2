//
//  TransitionAnimator.swift
//  lesson_71_Custom_Transition
//
//  Created by Yurii Bosov on 7/6/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

import UIKit

enum Direction: Int {
    case present
    case dismiss
}

class TransitionAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    var duration: TimeInterval = 0.3
    var direction: Direction = .present
    var originRect: CGRect = .zero
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return duration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        // make animate
        guard
        let viewTo = transitionContext.view(forKey: .to),
        let viewFrom = transitionContext.view(forKey: .from)
        else {
            return
        }
        let containerView = transitionContext.containerView
        let finalFrame = containerView.frame

        containerView.addSubview(viewTo)
        
        let x = originRect.size.width / finalFrame.size.width
        let y = originRect.size.height / finalFrame.size.height
        
        if direction == .present {
            
            // 1.
            viewTo.center = CGPoint(x: originRect.midX, y:originRect.midY)
            viewTo.transform = CGAffineTransform(scaleX: x, y: y)
            containerView.bringSubview(toFront: viewTo)
            
            // 2
            UIView.animate(withDuration: duration, animations: { 
                viewTo.transform = CGAffineTransform.identity
                viewTo.center = CGPoint(x: finalFrame.midX, y:finalFrame.midY)
            }, completion: { (success) in
                transitionContext.completeTransition(success)
            })
            
        } else {
            
            containerView.bringSubview(toFront: viewFrom)
            
            UIView.animate(withDuration: duration, animations: {
                
                viewFrom.center = CGPoint(x: self.originRect.midX, y:self.originRect.midY)
                viewFrom.transform = CGAffineTransform(scaleX: x, y: y)
                
            }, completion: { (success) in
                transitionContext.completeTransition(success)
            })
        }
    }
}
