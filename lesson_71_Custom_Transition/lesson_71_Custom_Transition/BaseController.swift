//
//  BaseController.swift
//  lesson_71_Custom_Transition
//
//  Created by Yurii Bosov on 7/6/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

import UIKit

class BaseController: UIViewController {

    deinit {
        print("deinit \(self)")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let swipe = UISwipeGestureRecognizer(target: self , action: #selector(BaseController.close))
        swipe.direction = .down
        self.view.addGestureRecognizer(swipe)
    }
    
    func close() {
        self.dismiss(animated: true, completion: nil)
    }
}
