//
//  ViewController.swift
//  lesson_71_Custom_Transition
//
//  Created by Yurii Bosov on 7/6/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    fileprivate let animator = TransitionAnimator()
    
    @IBOutlet weak var redView: UIView!
    @IBOutlet weak var yellowView: UIView!
    @IBOutlet weak var greenView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func redViewTap(_ tap: UITapGestureRecognizer) {
        let controller = RedController()
        controller.transitioningDelegate = self
        animator.originRect = redView.frame
            
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func yellowViewTap(_ tap: UITapGestureRecognizer) {
        let controller = YellowController()
        controller.transitioningDelegate = self
        animator.originRect = yellowView.frame
        
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func greenViewTap(_ tap: UITapGestureRecognizer) {
        let controller = GreenController()
        controller.transitioningDelegate = self
        animator.originRect = greenView.frame
        
        self.present(controller, animated: true, completion: nil)
    }
}

extension ViewController: UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        animator.direction = .present
        return animator
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        animator.direction = .dismiss
        return animator
    }
}

