//
//  RedController.swift
//  lesson_71_Custom_Transition
//
//  Created by Yurii Bosov on 7/6/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

import UIKit

class RedController: BaseController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .red
        
        let rect = UIView()
        rect.backgroundColor = .blue
        rect.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(rect)
        
        // width
        NSLayoutConstraint(item: rect, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 100).isActive = true
        
        // height
        NSLayoutConstraint(item: rect, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 100).isActive = true
        
        // top
        NSLayoutConstraint(item: rect, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1, constant: 20).isActive = true
        
        // trailing
        NSLayoutConstraint(item: rect, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: -20).isActive = true
    }
}
