//
//  TEST+CoreDataProperties.m
//  lesson_53_CoreData
//
//  Created by Yurii Bosov on 4/20/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "TEST+CoreDataProperties.h"

@implementation TEST (CoreDataProperties)

+ (NSFetchRequest<TEST *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"TEST"];
}

@dynamic name;
@dynamic ttt;

@end
