//
//  ViewController.m
//  lesson_53_CoreData
//
//  Created by Yurii Bosov on 4/9/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"
#import "Note+CoreDataClass.h"
#import "TEST+CoreDataClass.h"
#import "AppDelegate.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

#pragma mark - Actions

- (IBAction)createNoteButtonClicked {
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    // 1.
    NSEntityDescription *entityDescr = [NSEntityDescription entityForName:@"Note" inManagedObjectContext:appDelegate.persistentContainer.viewContext];
    
    // 2.
    Note *note = [[Note alloc] initWithEntity:entityDescr insertIntoManagedObjectContext:appDelegate.persistentContainer.viewContext];
    
    note.createDate = [NSDate date];
    note.modifyDate = note.createDate;
    note.title = @"Title";
    note.text = @"Text";
    
    
    entityDescr = [NSEntityDescription entityForName:@"TEST" inManagedObjectContext:appDelegate.persistentContainer.viewContext];
    TEST *test = [[TEST alloc] initWithEntity:entityDescr insertIntoManagedObjectContext:appDelegate.persistentContainer.viewContext];
    
    [note setRrrr:test];
    
    // 3.
    [appDelegate saveContext];
}

- (IBAction)getNotesListButtonClicked {
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    // 1 - создали запрос
    NSFetchRequest *request = [Note fetchRequest];
    request.returnsObjectsAsFaults = NO;
    
    // 1.1. для запроса укажем сортировку по modifyDate
    NSSortDescriptor *sd = [NSSortDescriptor sortDescriptorWithKey:@"modifyDate" ascending:NO];
    request.sortDescriptors = @[sd];
    
    // 2 - получаем данные с помощью реквеста
    // первый способ
    NSError *error = nil;
    NSArray *notes = [appDelegate.persistentContainer.viewContext executeFetchRequest:request error:&error];
    NSLog(@"error %@", error);
    NSLog(@"notes %@", notes);

    // второй способ
//    [appDelegate.persistentContainer.viewContext performBlock:^{
//        NSArray *notes = [request execute:nil];
//        NSLog(@"notes %@", notes);
//    }];
}

- (IBAction)removeAllNotesButtonClicked {
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
 
    NSFetchRequest *request = [Note fetchRequest];
    
    NSBatchDeleteRequest *deleteRequest = [[NSBatchDeleteRequest alloc] initWithFetchRequest:request];
    
    NSError *error = nil;
    NSPersistentStoreResult *result = [appDelegate.persistentContainer.viewContext executeRequest:deleteRequest error:&error];
    
    NSLog(@"error %@", error);
    NSLog(@"result %@", result);
    
    [appDelegate saveContext];
}

- (IBAction)removeIncorrectNotesButtonClicked {
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    // удалим те записи, у которых нет поля modifyDate
    // для этого:
    // 1. найдем эти записи
    NSFetchRequest *request = [Note fetchRequest];
    
    // 1.1 через предикат задаем условие выборки, в нашем случае получить те записи у которых modifyDate равно nil
    request.predicate = [NSPredicate predicateWithFormat:@"modifyDate == nil"];
    
    NSArray *notes = [appDelegate.persistentContainer.viewContext executeFetchRequest:request error:nil];
    
    // 2. удали эти записи
    if (notes.count) {
        // пример удаление по одному элементу
        for (Note *note in notes) {
            [appDelegate.persistentContainer.viewContext deleteObject:note];
        }
        [appDelegate saveContext];
    }
}

@end
