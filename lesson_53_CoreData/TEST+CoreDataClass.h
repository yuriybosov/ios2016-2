//
//  TEST+CoreDataClass.h
//  lesson_53_CoreData
//
//  Created by Yurii Bosov on 4/20/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Note;

NS_ASSUME_NONNULL_BEGIN

@interface TEST : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "TEST+CoreDataProperties.h"
