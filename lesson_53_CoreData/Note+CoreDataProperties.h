//
//  Note+CoreDataProperties.h
//  lesson_53_CoreData
//
//  Created by Yurii Bosov on 4/20/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "Note+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@class TEST;

@interface Note (CoreDataProperties)

+ (NSFetchRequest<Note *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSDate *createDate;
@property (nullable, nonatomic, retain) NSObject *image;
@property (nullable, nonatomic, copy) NSString *imagePath;
@property (nullable, nonatomic, copy) NSDate *modifyDate;
@property (nullable, nonatomic, copy) NSString *text;
@property (nullable, nonatomic, copy) NSString *title;
@property (nullable, nonatomic, retain) TEST *rrrr;

@end

NS_ASSUME_NONNULL_END
