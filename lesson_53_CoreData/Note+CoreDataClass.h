//
//  Note+CoreDataClass.h
//  lesson_53_CoreData
//
//  Created by Yurii Bosov on 4/9/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@class NSObject;

NS_ASSUME_NONNULL_BEGIN

@interface Note : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Note+CoreDataProperties.h"
