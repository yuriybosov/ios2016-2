//
//  TEST+CoreDataProperties.h
//  lesson_53_CoreData
//
//  Created by Yurii Bosov on 4/20/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "TEST+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface TEST (CoreDataProperties)

+ (NSFetchRequest<TEST *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *name;
@property (nullable, nonatomic, retain) Note *ttt;

@end

NS_ASSUME_NONNULL_END
