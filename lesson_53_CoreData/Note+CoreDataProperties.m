//
//  Note+CoreDataProperties.m
//  lesson_53_CoreData
//
//  Created by Yurii Bosov on 4/20/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "Note+CoreDataProperties.h"

@implementation Note (CoreDataProperties)

+ (NSFetchRequest<Note *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Note"];
}

@dynamic createDate;
@dynamic image;
@dynamic imagePath;
@dynamic modifyDate;
@dynamic text;
@dynamic title;
@dynamic rrrr;

@end
