//
//  ViewController.swift
//  lesson_77_RSwift
//
//  Created by Yurii Bosov on 7/27/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = R.string.myLocalizable.test()
    }
}

