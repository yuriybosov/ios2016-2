//
//  ViewController.m
//  lesson_37_2_TextField_Left_Right_View
//
//  Created by Yurii Bosov on 12/11/16.
//  Copyright © 2016 ios. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UITextFieldDelegate> {
    IBOutlet UITextField *passwordTextField;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // для поля ввода добавляем правую вью, это картинка с ошибкой
    // режим отображения правой вью ставим в никогда не отображаться
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 20)];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    imageView.image = [UIImage imageNamed:@"invalidFieldIndicatorIcon"];
    
    // назначили правую виьюшку для поля ввода
    passwordTextField.rightView = imageView;
    // настроили режим отображения в никогда
    passwordTextField.rightViewMode = UITextFieldViewModeNever;
    
    // добавили событие, которые будем вызываться когда пользователь что то изменил в поле ввода (добавил или удалил символ)
    [passwordTextField addTarget:self
                          action:@selector(textFieldChangedValue:)
                forControlEvents:UIControlEventEditingChanged];
}

#pragma mark - Action

- (IBAction)textFieldChangedValue:(UITextField *)sender{
    passwordTextField.rightViewMode = (passwordTextField.text.length < 8) ? UITextFieldViewModeAlways : UITextFieldViewModeNever;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

@end
