//
//  ViewController.m
//  lesson 13_UIView_IBOutlet
//
//  Created by Yuriy Bosov on 9/25/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {
    UIView *v2;
    UIView *v3;
}

@property (nonatomic, weak) IBOutlet UIView *v1;

@end


@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // задаем первой view произвольный цвет по RGB 226,20,21
    self.v1.backgroundColor = [UIColor colorWithRed:226.f/255.f
                                              green:20/255.f
                                               blue:21.f/225.f
                                              alpha:1.f];
    
    // создаем v2 программно
    CGRect frame = CGRectMake(_v1.frame.origin.x + _v1.frame.size.width + 20,
                              _v1.frame.origin.y,
                              _v1.frame.size.width,
                              _v1.frame.size.height);
    v2 = [[UIView alloc] initWithFrame:frame];
    v2.backgroundColor = [UIColor yellowColor];
    [self.view addSubview:v2];
    
    // создаем v2 программно, использую .nib-файл
    // для этого воспольтзуемся следующим методом loadNibNamed:owner:options - данный метод создает массив из view, которые описаны в nib-файле
    NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"CustomView" owner:nil options:nil];
    v3 = arrayOfViews.firstObject;
    v3.frame = CGRectMake(v2.frame.origin.x + v2.frame.size.width + 20,
                          v2.frame.origin.y,
                          v3.frame.size.width,
                          v3.frame.size.height);
    [self.view addSubview:v3];
}

#pragma mark - Button Actions

- (IBAction)showOrHiddenButtonClicked:(id)sender {
    self.v1.hidden = !self.v1.hidden;
}

- (IBAction)addBorderButtonClicked:(id)sender {
    v2.layer.borderColor = [[UIColor blackColor] CGColor];
    v2.layer.borderWidth = 2;
}

- (IBAction)addCornerRadiusButtonClicked:(id)sender {
    v3.layer.borderColor = [[UIColor redColor] CGColor];
    v3.layer.borderWidth = 1;
    v3.layer.cornerRadius = 5;
}

@end
