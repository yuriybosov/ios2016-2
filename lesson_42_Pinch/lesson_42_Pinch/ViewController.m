//
//  ViewController.m
//  lesson_42_Pinch
//
//  Created by Yurii Bosov on 12/29/16.
//  Copyright © 2016 ios. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UIGestureRecognizerDelegate> {
    IBOutlet UIView *rect;
    
    CGFloat scale;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    scale = 1;
}

- (IBAction)doubleTap:(id)sender {
    // по двойному тапу будем менять scale обьекту
    
    if (scale < 1.5) {
        scale = 1.5;
    } else if (scale < 2) {
        scale = 2;
    } else if (scale < 3.0) {
        scale = 3.0;
    } else {
        scale = 1;
    }
    
    // применим новый scale к нашему квадрату, используя тарнсформацию
    CGAffineTransform transform =  CGAffineTransformMakeScale(scale, scale);
    rect.transform = transform;
}

- (IBAction)pinch:(UIPinchGestureRecognizer *)sender {
    rect.transform = CGAffineTransformScale(rect.transform, sender.scale, sender.scale);
    sender.scale = 1;
}

- (IBAction)rotate:(UIRotationGestureRecognizer *)sender {
    rect.transform = CGAffineTransformRotate(rect.transform, sender.rotation);
    sender.rotation = 0;
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    
    return YES;
}

@end
