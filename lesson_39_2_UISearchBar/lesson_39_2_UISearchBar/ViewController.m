//
//  ViewController.m
//  lesson_39_2_UISearchBar
//
//  Created by Yurii Bosov on 12/18/16.
//  Copyright © 2016 ios. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UISearchBarDelegate> {
    IBOutlet UISearchBar *mySearchBar;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Search";
    
//    mySearchBar.showsBookmarkButton = YES;
//    mySearchBar.showsScopeBar = YES;
//    mySearchBar.showsSearchResultsButton = YES;
}

#pragma mark - UISearchBarDelegate

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    // показали cancel button
    [searchBar setShowsCancelButton:YES animated:YES];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    // скрыли cancel button
    [searchBar setShowsCancelButton:NO animated:YES];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    searchBar.text = nil;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    NSLog(@"searchText = %@", searchText);
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    NSLog(@"searchText = %@", searchBar.text);
    [searchBar resignFirstResponder];
}

@end
