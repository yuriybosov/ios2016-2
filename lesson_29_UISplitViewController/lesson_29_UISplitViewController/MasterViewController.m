//
//  MasterViewController.m
//  lesson_29_UISplitViewController
//
//  Created by Yurii Bosov on 11/6/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailedViewController.h"

@interface MasterViewController () {
    NSArray *dataSource;
}

@end

@implementation MasterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    dataSource = @[@"Profile",@"News",@"Answer",@"Messages",@"Friends"];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID" forIndexPath:indexPath];
    
    cell.textLabel.text = dataSource[indexPath.row];
    
    return cell;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"showDetail"]){
        
        UITableViewCell *cell = sender;
        UINavigationController *nc = segue.destinationViewController;
        
        DetailedViewController *vc = nc.viewControllers[0];
        vc.title = cell.textLabel.text;
    }
}

@end
