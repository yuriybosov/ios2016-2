//
//  SplitViewController.m
//  lesson_29_UISplitViewController
//
//  Created by Yurii Bosov on 11/6/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import "SplitViewController.h"

@interface SplitViewController () <UISplitViewControllerDelegate>

@end

@implementation SplitViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // если нужно что бы мастер на iPad отображался всегда, то задаем preferredDisplayMode значение UISplitViewControllerDisplayModeAllVisible
    // проверяем что программа запущена на iPad
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        self.preferredDisplayMode = UISplitViewControllerDisplayModeAllVisible;
    }
    
    self.delegate = self;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

#pragma mark - UISplitViewControllerDelegate

//
- (BOOL)splitViewController:(UISplitViewController *)splitViewController collapseSecondaryViewController:(UIViewController *)secondaryViewController ontoPrimaryViewController:(UIViewController *)primaryViewController {
    return YES;
}

@end
