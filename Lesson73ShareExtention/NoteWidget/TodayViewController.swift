//
//  TodayViewController.swift
//  NoteWidget
//
//  Created by Yurii Bosov on 7/23/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

import UIKit
import NotificationCenter
import NoteData

class TodayViewController: UIViewController, NCWidgetProviding {
    
    @IBOutlet weak var tableView: UITableView?
    var dataSource = Note.noteList()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.extensionContext?.widgetLargestAvailableDisplayMode = .expanded
    }
    
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {
        dataSource = Note.noteList()
        print("widget dataSource \(dataSource.count)")
        tableView?.reloadData()
        completionHandler(NCUpdateResult.newData)
    }
    
    func widgetActiveDisplayModeDidChange(_ activeDisplayMode: NCWidgetDisplayMode, withMaximumSize maxSize: CGSize) {
        if activeDisplayMode == .expanded {
            
            let minH = min(self.tableView?.contentSize.height ?? 110, 44 * 5)
            
            self.preferredContentSize = CGSize(width: self.view.frame.size.width, height:minH)
        }else {
            self.preferredContentSize = CGSize(width: maxSize.width, height: 110)
        }
    }
}

extension TodayViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellID", for: indexPath)
        cell.textLabel?.text = dataSource[indexPath.row].title
        cell.detailTextLabel?.text = dataSource[indexPath.row].text
        return cell
    }
}

extension TodayViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //com.note.app
        if let url = URL(string: "com.note.app") {
            self.extensionContext?.open(url, completionHandler: nil)
        }
    }
}
