//
//  Dispatcher.h
//  lesson 14_Protocol_Delegate
//
//  Created by Yuriy Bosov on 9/15/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Truck.h"

@interface Dispatcher : NSObject <TruckProtocol>

@end
