//
//  TruckProtocol.h
//  lesson 14_Protocol_Delegate
//
//  Created by Yuriy Bosov on 9/15/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#ifndef TruckProtocol_h
#define TruckProtocol_h

@import Foundation;
@class Truck;

typedef enum : NSUInteger {
    CargoTypeNone,
    CargoTypeContainer,
    CargoTypeAuto,
    CargoTypeOil
} CargoType;


@protocol TruckProtocol <NSObject>

@required

- (NSString *)pointForTruck:(Truck *)truck;
- (CargoType)cargoTypeForTruck:(Truck *)truck;

@optional

- (void)truck:(Truck *)truck startToPoint:(NSString *)point withCargo:(CargoType)cargoType;

- (void)truck:(Truck *)truck finishedInPoint:(NSString *)point withCargo:(CargoType)cargoType;

- (void)truckFailedMove:(Truck *)truck;

@end

#endif /* TruckProtocol_h */
