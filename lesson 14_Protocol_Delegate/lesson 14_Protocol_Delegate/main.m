//
//  main.m
//  lesson 14_Protocol_Delegate
//
//  Created by Yuriy Bosov on 9/15/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Dispatcher.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        Dispatcher *dispatcher = [[Dispatcher alloc] init];
        
        Truck *man = [Truck new];
        man.name = @"MAN";
        man.delegate = dispatcher;
        
        Truck *volvo = [Truck new];
        volvo.name = @"VOLVO";
        volvo.delegate = dispatcher;
        
        Truck *scania = [Truck new];
        scania.name = @"SCANIA";
        scania.delegate = dispatcher;
        
        Truck *gas = [Truck new];
        gas.name = @"GAS";
        gas.delegate = dispatcher;
        
        [man move];
        [volvo move];
        [scania move];
        [gas move];
    }
    return 0;
}
