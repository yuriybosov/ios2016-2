//
//  Truck.m
//  lesson 14_Protocol_Delegate
//
//  Created by Yuriy Bosov on 9/15/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "Truck.h"

@implementation Truck

+ (NSString *)cargoTypeToString:(CargoType)type {
    NSString *result = nil;
    
    switch (type) {
        case CargoTypeNone:
            result = @"груз не задан";
            break;
        case CargoTypeOil:
            result = @"нефть";
            break;
        case CargoTypeAuto:
            result = @"авто";
            break;
        case CargoTypeContainer:
            result = @"контейнер";
            break;
    }
    
    return result;
}

- (void)dealloc {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (BOOL)prepareToMove {
    self.point = [self.delegate pointForTruck:self];
    self.cargoType = [self.delegate cargoTypeForTruck:self];
    
    return (self.point != nil && self.cargoType != CargoTypeNone);
}

- (void)move {
    if ([self prepareToMove]) {
        
        // start
        if ([self.delegate respondsToSelector:@selector(truck:startToPoint:withCargo:)]) {
            
            [self.delegate truck:self
                    startToPoint:self.point
                       withCargo:self.cargoType];
            
        }
        
        // end
        if ([self.delegate respondsToSelector:@selector(truck:finishedInPoint:withCargo:)]){
            
            [self.delegate truck:self
                 finishedInPoint:self.point
                       withCargo:self.cargoType];
        }
        
        //
        self.cargoType = CargoTypeNone;
        self.point = nil;
        
    }
    else {
        if ([self.delegate respondsToSelector:@selector(truckFailedMove:)]) {
            [self.delegate truckFailedMove:self];
        }
    }
}

@end
