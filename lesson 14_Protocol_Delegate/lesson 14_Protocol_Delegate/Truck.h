//
//  Truck.h
//  lesson 14_Protocol_Delegate
//
//  Created by Yuriy Bosov on 9/15/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TruckProtocol.h"

@interface Truck : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *point;
@property (nonatomic, assign) CargoType cargoType;
@property (nonatomic, weak) id<TruckProtocol> delegate;

+ (NSString *)cargoTypeToString:(CargoType)type;

- (void)move;

@end
