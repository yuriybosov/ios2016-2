//
//  Dispatcher.m
//  lesson 14_Protocol_Delegate
//
//  Created by Yuriy Bosov on 9/15/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "Dispatcher.h"

@implementation Dispatcher

#pragma mark - TruckProtocol - Requared

- (void)dealloc {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (NSString *)pointForTruck:(Truck *)truck {
    
    NSString *result = nil;
    
    if ([truck.name isEqualToString:@"MAN"]) {
        result = @"Kyiv";
    } else if ([truck.name isEqualToString:@"VOLVO"]){
        result = @"Lviv";
    } else if ([truck.name isEqualToString:@"SCANIA"]){
        result = @"Odessa";
    }
    
    return result;
}

- (CargoType)cargoTypeForTruck:(Truck *)truck {
    
    CargoType result = CargoTypeNone;
    
    if ([truck.name isEqualToString:@"MAN"]) {
        result = CargoTypeAuto;
    } else if ([truck.name isEqualToString:@"VOLVO"]){
        result = CargoTypeOil;
    } else if ([truck.name isEqualToString:@"SCANIA"]){
        result = CargoTypeContainer;
    }
    
    return result;
}

#pragma mark - TruckProtocol - Optional

- (void)truck:(Truck *)truck startToPoint:(NSString *)point withCargo:(CargoType)cargoType {
    
    NSLog(@"%@ начал движение в %@, везет %@", truck.name, point, [Truck cargoTypeToString:cargoType]);
}

- (void)truck:(Truck *)truck finishedInPoint:(NSString *)point withCargo:(CargoType)cargoType {
    
    NSLog(@"%@ приехал в %@, привез %@", truck.name, point, [Truck cargoTypeToString:cargoType]);
}

- (void)truckFailedMove:(Truck *)truck {
    NSLog(@"%@ не смог начать движение, нет вводных данных", truck.name);
}

@end
