#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        
        // добавление объектов
        [dict setObject:@"obj1" forKey:@"key1"];
        [dict setObject:@"obj2" forKey:@"key2"];
        [dict setObject:@"obj3" forKey:@"key3"];
        
        // добавление объектов из другого словаря
        NSDictionary *dict1 = @{@"key4":@"obj4",
                                @"key5":@"obj5",
                                @"key6":@"obj6"};
        [dict addEntriesFromDictionary:dict1];
        
        // удаление объектов
        // удаление по ключу
        [dict removeObjectForKey:@"key1"];
        // удаление по ключу массиву ключей
        NSArray *keys = @[@"key2", @"key3"];
        [dict removeObjectsForKeys:keys];
        // удаление всех объектов (очистка словаря)
        [dict removeAllObjects];
        
        NSLog(@"dict = %@", dict);
    }
    return 0;
}
