//
//  AppDelegate.h
//  lesson 22_Dinamyc_Cell
//
//  Created by Yuriy Bosov on 10/13/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

