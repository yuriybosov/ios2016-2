//
//  CommentCell.m
//  lesson 22_Dinamyc_Cell
//
//  Created by Yuriy Bosov on 10/13/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "CommentCell.h"

@implementation CommentCell

- (void)setupComment:(Comment *)comment {
    self.lbComment.text = comment.text;
    NSString *imageName = comment.isMy ? @"bubleRight" : @"bubleLeft";
    
    UIImage *originalImage = [UIImage imageNamed:imageName];
    UIImage *resizeImage = [originalImage resizableImageWithCapInsets:UIEdgeInsetsMake(originalImage.size.height/2, originalImage.size.width/2, originalImage.size.height/2 - 1, originalImage.size.width/2 - 1)];
    
    self.bubleImage.image = resizeImage;
}
@end
