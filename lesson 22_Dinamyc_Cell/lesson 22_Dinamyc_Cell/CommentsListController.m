//
//  CommentsListController.m
//  lesson 22_Dinamyc_Cell
//
//  Created by Yuriy Bosov on 10/13/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "CommentsListController.h"
#import "CommentCell.h"

@interface CommentsListController () {
    NSArray *dataSource;
}

@end

@implementation CommentsListController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"iMessager :)";
    dataSource = [Comment listOfComments];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Comment *comment = [dataSource objectAtIndex:indexPath.row];
    NSString *cellID = comment.isMy ? @"cellRight" : @"cellLeft";
    CommentCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell setupComment:comment];
    
    return cell;
}

#pragma mark - Table view delegate

//// tableView:heightForRowAtIndexPath: -
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    return 44;
//}

// tableView:estimatedHeightForRowAtIndexPath: -
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

@end
