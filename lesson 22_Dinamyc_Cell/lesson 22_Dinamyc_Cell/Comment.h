//
//  Comment.h
//  lesson 22_Dinamyc_Cell
//
//  Created by Yuriy Bosov on 10/13/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Comment : NSObject

@property (nonatomic, strong) NSString *text;
@property (nonatomic, assign) BOOL isMy;

+ (NSArray *)listOfComments;

@end
