//
//  Comment.m
//  lesson 22_Dinamyc_Cell
//
//  Created by Yuriy Bosov on 10/13/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "Comment.h"

@implementation Comment

+ (NSArray *)listOfComments {
    
    NSMutableArray *result = [NSMutableArray new];
    
    // 1
    Comment *comment = [Comment new];
    comment.text = @"Hello";
    comment.isMy = NO;
    [result addObject:comment];
    
    // 2
    comment = [Comment new];
    comment.text = @":)";
    comment.isMy = YES;
    [result addObject:comment];
    
    // 3
    comment = [Comment new];
    comment.text = @"asf sadf asd asasd asd sadfas asd as as ass as asd asd asfd asdf sadf asdf as asd asd asd sad as adsf dsa fdasf sadf sad dsaf dasf asdf dsa afds fdsaas dfas fds dsaf";
    comment.isMy = YES;
    [result addObject:comment];
    
    // 4
    comment = [Comment new];
    comment.text = @"asdf asf asdf dsa sad sdafsad dfs afasd fasda sdfd";
    comment.isMy = NO;
    [result addObject:comment];
    
    // 5
    comment = [Comment new];
    comment.text = @"asd fasd dsaf sda dsfs dafas f asdf asdf sdaf asdf sdaf asdf dsaf dsaf asdf \nasdf asd \nasdfasd\nasdf asddaf asdf sd\nasdf asdf";
    comment.isMy = NO;
    [result addObject:comment];
    
    return result;
}

@end
