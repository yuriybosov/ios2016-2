//
//  CommentCell.h
//  lesson 22_Dinamyc_Cell
//
//  Created by Yuriy Bosov on 10/13/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Comment.h"

@interface CommentCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *lbComment;
@property (nonatomic, weak) IBOutlet UIImageView *bubleImage;

- (void)setupComment:(Comment *)comment;

@end
