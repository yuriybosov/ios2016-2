//
//  Group.h
//  lesson 6_1_array_example
//
//  Created by Yuriy Bosov on 7/24/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Group : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSArray *students;

+ (Group *)groupWithName:(NSString *)name
                students:(NSArray *)students;

- (NSUInteger)countOfMen;
- (NSUInteger)countOfWomen;

- (NSUInteger)countOf30Plus;
- (NSUInteger)countOf30Min;

- (CGFloat)avgAge;
- (NSString *)allNames;

@end
