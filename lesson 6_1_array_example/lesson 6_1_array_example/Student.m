//
//  Student.m
//  lesson 6_1_array_example
//
//  Created by Yuriy Bosov on 7/24/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "Student.h"

@implementation Student

+ (Student *)studentWithName:(NSString *)name
                         age:(NSUInteger )age
                      gender:(Gender)gender {
    Student *student = [[Student alloc] init];
    
    student.name = name;
    student.age = age;
    student.gender = gender;
    
    return student;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"name: %@, age: %lu, gender: %@", _name, _age, (_gender == GenderMale ? @"man" : @"woman")];
}

@end
