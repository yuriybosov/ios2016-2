//
//  main.m
//  lesson 6_1_array_example
//
//  Created by Yuriy Bosov on 7/24/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Student.h"
#import "Group.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        NSArray *students = @[[Student studentWithName:@"Alex G" age:25                                 gender:GenderMale],
                              [Student studentWithName:@"Kostya" age:28 gender:GenderMale],
                              [Student studentWithName:@"Vitali" age:31 gender:GenderMale],
                              [Student studentWithName:@"Ruslan" age:35 gender:GenderMale],
                              [Student studentWithName:@"Anna" age:26 gender:GenderFemale],
                              [Student studentWithName:@"Iuliia" age:26 gender:GenderFemale],
                              [Student studentWithName:@"Alex V" age:33 gender:GenderMale],
                              [Student studentWithName:@"Alex Sh" age:29 gender:GenderMale]];
        
        
        Group *iOSGroup = [Group groupWithName:@"ios 2016 - 2" students:students];
        NSLog(@"%@", iOSGroup);
        
        NSLog(@"кол-во М: %lu, кол-во Ж: %lu", [iOSGroup countOfMen],[iOSGroup countOfWomen]);
        
        NSLog(@"кол-во больше 30 лет: %lu, кол-во меньше 30 лет: %lu", [iOSGroup countOf30Plus],[iOSGroup countOf30Min]);
        
        NSLog(@"средний возраст: %f", [iOSGroup avgAge]);
        
        NSLog(@"имена всех студентов: %@", [iOSGroup allNames]);
    }
    return 0;
}
