//
//  Group.m
//  lesson 6_1_array_example
//
//  Created by Yuriy Bosov on 7/24/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "Group.h"
#import "Student.h"

@implementation Group

+ (Group *)groupWithName:(NSString *)name
                students:(NSArray *)students {
    Group *group = [Group new];
    
    group.name = name;
    group.students = students;
    
    return group;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"group name: %@, count of students: %lu, students: %@", _name, _students.count, _students];
}

- (NSUInteger)countOfMen {
    NSUInteger result = 0;
    
    for (Student *student in _students) {
        if (student.gender == GenderMale) {
            result++;
        }
    }
    
    return result;
}

- (NSUInteger)countOfWomen {
    return _students.count - [self countOfMen];
}

- (NSUInteger)countOf30Plus {
    
    NSUInteger result = 0;
    
    for (Student *student in _students) {
        if (student.age >= 30) {
            result++;
        }
    }
    
    return result;
}

- (NSUInteger)countOf30Min {
    return _students.count - [self countOf30Plus];
}

- (CGFloat)avgAge {
    CGFloat result = 0;
    if (_students.count > 0) {
        CGFloat sumAge = 0;
        for (Student *student in _students) {
            sumAge += student.age;
        }
        
        result = sumAge / _students.count;
    }
    return result;
}

- (NSString *)allNames {
    NSArray *names = [_students valueForKey:@"age"];
    return [names componentsJoinedByString:@", "];
}

@end
