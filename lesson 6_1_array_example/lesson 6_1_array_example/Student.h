//
//  Student.h
//  lesson 6_1_array_example
//
//  Created by Yuriy Bosov on 7/24/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    GenderMale,
    GenderFemale
} Gender;

@interface Student : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) NSUInteger age;
@property (nonatomic, assign) Gender gender;

+ (Student *)studentWithName:(NSString *)name
                         age:(NSUInteger )age
                      gender:(Gender)gender;

@end
