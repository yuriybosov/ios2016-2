//
//  ViewController.m
//  lesson_45_game_x_o
//
//  Created by Yurii Bosov on 1/19/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

@end
