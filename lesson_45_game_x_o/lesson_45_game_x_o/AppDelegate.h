//
//  AppDelegate.h
//  lesson_45_game_x_o
//
//  Created by Yurii Bosov on 1/19/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

