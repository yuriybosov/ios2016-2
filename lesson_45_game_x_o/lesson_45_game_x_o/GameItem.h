//
//  GameItem.h
//  lesson_45_game_x_o
//
//  Created by Yurii Bosov on 1/19/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    GameItemStateEmpty,
    GameItemState_X,
    GameItemState_O,
} GameItemState;

@interface GameItem : NSObject

@property (nonatomic, assign) GameItemState state;
@property (nonatomic, assign) CGRect rect;

@end
