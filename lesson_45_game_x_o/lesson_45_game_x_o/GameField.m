//
//  GameField.m
//  lesson_45_game_x_o
//
//  Created by Yurii Bosov on 1/19/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "GameField.h"
#import "GameItem.h"

@interface GameField () {
    NSMutableArray<GameItem *> *items;
    NSUInteger step;
}

@end


@implementation GameField

- (void)drawRect:(CGRect)rect {
    
    [self initGameItems];
    
    // 1. рисуем игровое поле
    [self drawGameField];
    
    // 2. рисуем игровые элементы (х или о)
    [self drawGameItems];
}

- (void)initGameItems {
    if (!items) {
        items = [NSMutableArray new];
        
        CGFloat w = MIN(self.frame.size.width,
                        self.frame.size.height)/3;
        
        for (NSUInteger x = 0; x < 3; x++) {
            for (NSUInteger y = 0; y < 3; y++) {
                
                GameItem *item = [GameItem new];
                item.rect = CGRectMake(x * w + 1,
                                       y * w + 1,
                                       w - 2,
                                       w - 2);
                [items addObject:item];
                
            }
        }
        step = items.count;
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    if (step > 0) {
        
        UITouch *touch = touches.anyObject;
        CGPoint point = [touch locationInView:self];
        
        for (GameItem *itm in items) {
            if (CGRectContainsPoint(itm.rect, point)) {
                
                if (itm.state == GameItemStateEmpty) {
                    itm.state = step % 2 ? GameItemState_X : GameItemState_O;
                    step--;
                    
                    [self setNeedsDisplay];
                    
                    // проверить win
                    NSArray *win = [self winCombination];
                    if (win) {
                        step = 0;
                        //
                    }
                }
                break;
            }
        }
    }
    
    // нужно определить по какому игровому элементу мы тапнули, и если игровой элемент свободный - то рисуем крестик или нолик
    
    // после того как поставили крестик или нолик нужно проверить на win
    
    // если win - то его отрисовать
}

// рисуем игровое поле
- (void)drawGameField {
    CGFloat w = MIN(self.frame.size.width,
                    self.frame.size.height)/3;
    
    [[UIColor greenColor] setStroke];
    
    //1
    UIBezierPath *path = [UIBezierPath bezierPath];
    path.lineWidth = 1;
    [path moveToPoint:CGPointMake(0, w)];
    [path addLineToPoint:CGPointMake(3*w, w)];
    [path stroke];
    
    //2
    [path moveToPoint:CGPointMake(0, 2*w)];
    [path addLineToPoint:CGPointMake(3*w, 2*w)];
    [path stroke];
    
    //3
    [path moveToPoint:CGPointMake(w, 0)];
    [path addLineToPoint:CGPointMake(w, 3*w)];
    [path stroke];
    
    //3
    [path moveToPoint:CGPointMake(2*w, 0)];
    [path addLineToPoint:CGPointMake(2*w, 3*w)];
    [path stroke];
    
//    [path moveToPoint:CGPointMake(0, 3*w)];
//    [path addLineToPoint:CGPointMake(3*w, 3*w)];
//    [path stroke];
}

// рисуем игровые элементы
- (void)drawGameItems {
    for (GameItem *itm in items) {
        if (itm.state == GameItemState_O) {
            
            UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:itm.rect];
            [path stroke];
            
        } else if (itm.state == GameItemState_X) {
            
            UIBezierPath *path = [UIBezierPath bezierPath];
            
            //
            [path moveToPoint:itm.rect.origin];
            [path addLineToPoint:CGPointMake(itm.rect.origin.x + itm.rect.size.width,
                                             itm.rect.origin.y + itm.rect.size.height)];
            //
            [path moveToPoint:CGPointMake(itm.rect.origin.x + itm.rect.size.width, itm.rect.origin.y)];
            [path addLineToPoint:CGPointMake(itm.rect.origin.x, itm.rect.origin.y + itm.rect.size.height)];
            
            [path stroke];
        }
    }
}

- (NSArray<GameItem *> *)winCombination {
    NSMutableArray<GameItem *> *result = nil;
    
    if (items.count == 9) {
        
        if ([self checkIndex1:0 index2:3 index3:6 result:&result]){
        }else if (!result && [self checkIndex1:1 index2:4 index3:7 result:&result]) {
        }else if (!result && [self checkIndex1:2 index2:5 index3:8 result:&result]) {
        }else if (!result && [self checkIndex1:0 index2:1 index3:2 result:&result]) {
        }else if (!result && [self checkIndex1:3 index2:4 index3:5 result:&result]) {
        }else if (!result && [self checkIndex1:6 index2:7 index3:8 result:&result]) {
        }else if (!result && [self checkIndex1:0 index2:4 index3:8 result:&result]) {
        }else if (!result && [self checkIndex1:2 index2:4 index3:6 result:&result]) {
        }
    }
    
    return result;
}

- (BOOL)checkIndex1:(NSUInteger)index1
             index2:(NSUInteger)index2
             index3:(NSUInteger)index3
             result:(NSMutableArray**)result {
    
    if (items[index1].state != GameItemStateEmpty &&
        items[index1].state == items[index2].state &&
        items[index2].state == items[index3].state) {
        
        *result = [NSMutableArray new];
        [*result addObjectsFromArray:@[items[index1],
                                       items[index2],
                                       items[index3]]];
        NSLog(@"!!!!WIN!!!!");
    }
    return [*result count] > 0;
}

@end
