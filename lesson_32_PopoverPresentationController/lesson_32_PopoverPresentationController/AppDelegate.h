//
//  AppDelegate.h
//  lesson_32_PopoverPresentationController
//
//  Created by Yurii Bosov on 11/20/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

