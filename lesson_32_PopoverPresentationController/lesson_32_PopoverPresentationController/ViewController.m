//
//  ViewController.m
//  lesson_32_PopoverPresentationController
//
//  Created by Yurii Bosov on 11/20/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        self.navigationController.navigationBarHidden = YES;
    }
}

- (CGSize)preferredContentSize {
    
    CGSize fittingSize = CGSizeMake(300, 0);
    CGSize size = [self.view systemLayoutSizeFittingSize:fittingSize
                           withHorizontalFittingPriority:UILayoutPriorityRequired
                                 verticalFittingPriority:UILayoutPriorityFittingSizeLevel];
    return size;
}

- (IBAction)doneButtonClicked:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
