//
//  MyTableViewController.m
//  lesson_32_PopoverPresentationController
//
//  Created by Yurii Bosov on 11/20/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import "MyTableViewController.h"
#import "ViewController.h"

@interface MyTableViewController () <UITableViewDelegate, UITableViewDataSource> {
    
    IBOutlet UIBarButtonItem *toolBarButtonItem;
}

@end

@implementation MyTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Lesson 32";
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 20;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID" forIndexPath:indexPath];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%li", indexPath.row + 1];
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // получаем ячейку, по которой нажали
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    // высчитываем квадрат
    CGRect sourceRect = cell.bounds;
    
    // show popover from tableviewcell
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewControllerNC"];
    vc.modalPresentationStyle = UIModalPresentationPopover;
    
    UIPopoverPresentationController *ppc = vc.popoverPresentationController;
    ppc.permittedArrowDirections = UIPopoverArrowDirectionAny;
    ppc.sourceView = cell;
    ppc.sourceRect = sourceRect;
    ppc.backgroundColor = [UIColor yellowColor];
    
    [self presentViewController:vc animated:YES completion:nil];
}

#pragma mark - Button Actions

- (IBAction)toolbarButtonClicked:(id)sender{
    // show popover from barbuttonitem
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewControllerNC"];
    vc.modalPresentationStyle = UIModalPresentationPopover;
    
    UIPopoverPresentationController *ppc = vc.popoverPresentationController;
    ppc.permittedArrowDirections = UIPopoverArrowDirectionAny;
    ppc.sourceView = self.view;
    ppc.barButtonItem = toolBarButtonItem;
    ppc.backgroundColor = [UIColor greenColor];
    
    [self presentViewController:vc animated:YES completion:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    UIPopoverPresentationController *ppc = segue.destinationViewController.popoverPresentationController;
    ppc.backgroundColor = [UIColor redColor];
}

@end
