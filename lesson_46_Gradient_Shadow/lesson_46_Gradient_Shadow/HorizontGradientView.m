//
//  HorizontGradientView.m
//  lesson_46_Gradient_Shadow
//
//  Created by Yurii Bosov on 1/22/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "HorizontGradientView.h"

@interface HorizontGradientView (){
    CAGradientLayer *gradientLayer;
}

@end

@implementation HorizontGradientView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    // 1. создали градиент леер
    gradientLayer = [CAGradientLayer layer];
    // 2. задаем ему массив цветов
    gradientLayer.colors = @[(__bridge id)[[UIColor yellowColor] CGColor],
                             (__bridge id)[[UIColor greenColor]CGColor],
                             (__bridge id)[[UIColor blueColor]CGColor]];
    // 3. настроим направление градиента по горизонтали
    gradientLayer.startPoint = CGPointMake(0.0, 0.5);
    gradientLayer.endPoint = CGPointMake(1.0, 0.5);
    
    // 4. добавляем его на layer вью
    [self.layer addSublayer:gradientLayer];
}

- (void)drawRect:(CGRect)rect {
    gradientLayer.frame = self.bounds;
}

@end
