//
//  ViewController.m
//  lesson_46_Gradient_Shadow
//
//  Created by Yurii Bosov on 1/22/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"
#import "VerticalGradientView.h"
#import "HorizontGradientView.h"

@interface ViewController () {
    IBOutlet VerticalGradientView *v1; // отобразим в ней градиент вертикальный
    IBOutlet HorizontGradientView *v2; // отобразим в ней градиент горизонтальный
    IBOutlet UIView *v3; // отобразим в ней тень
    
    IBOutlet UIView *v4; //
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    v1.contentMode = UIViewContentModeRedraw;
    v2.contentMode = UIViewContentModeRedraw;
    
    // тень
    // для красоты сделали закругление для вью
    v3.layer.cornerRadius = 8;
    
    // цвет тени
    v3.layer.shadowColor = [UIColor blackColor].CGColor;
    // отступ тени
    v3.layer.shadowOffset = CGSizeMake(0, 3);
    // прозрачность тени
    v3.layer.shadowOpacity = 0.2;
    // радиус тени
    v3.layer.shadowRadius = 3;
    
    // отоброжение маски
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.contents = (__bridge id _Nullable)([[UIImage imageNamed:@"photoMask"] CGImage]);
    maskLayer.frame = v4.bounds;
    v4.layer.mask = maskLayer;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

@end
