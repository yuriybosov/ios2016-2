//
//  VerticalGradientView.m
//  lesson_46_Gradient_Shadow
//
//  Created by Yurii Bosov on 1/22/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "VerticalGradientView.h"

@interface VerticalGradientView () {
    CAGradientLayer *gradientLayer;
}

@end

@implementation VerticalGradientView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    // 1. создали градиент леер
    gradientLayer = [CAGradientLayer layer];
    // 2. задаем ему массив цветов
    gradientLayer.colors = @[(__bridge id)[[UIColor redColor] CGColor],
                     (__bridge id)[[[UIColor redColor] colorWithAlphaComponent:0] CGColor]];
    // 3. добавляем его на layer вью
    [self.layer addSublayer:gradientLayer];
}

- (void)drawRect:(CGRect)rect {
    gradientLayer.frame = self.bounds;
}

@end
