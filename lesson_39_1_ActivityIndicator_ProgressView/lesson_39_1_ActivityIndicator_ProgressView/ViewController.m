//
//  ViewController.m
//  lesson_39_1_ActivityIndicator_ProgressView
//
//  Created by Yurii Bosov on 12/18/16.
//  Copyright © 2016 ios. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {
    IBOutlet UIActivityIndicatorView *activityIndicator;
    IBOutlet UIProgressView *progressView;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    activityIndicator.hidden = YES;
    activityIndicator.hidesWhenStopped = YES;
    
    progressView.progress = 0;
    
}

#pragma mark - Action

- (IBAction)changeProgressValue:(id)sender {
    [progressView setProgress:progressView.progress  + 0.25 animated:YES];
}

- (IBAction)startButtonClicked:(id)sender{
    [activityIndicator startAnimating];
    activityIndicator.hidden = NO;
    
    //
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}

- (IBAction)stopButtonClicked:(id)sender{
    [activityIndicator stopAnimating];
    
    //
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}

@end
