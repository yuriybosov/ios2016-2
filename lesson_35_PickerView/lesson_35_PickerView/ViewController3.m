//
//  ViewController3.m
//  lesson_35_PickerView
//
//  Created by Yurii Bosov on 12/4/16.
//  Copyright © 2016 comfy. All rights reserved.
//

#import "ViewController3.h"

@interface ViewController3 () <UIPickerViewDataSource, UIPickerViewDelegate> {
    IBOutlet UIPickerView *pickeView;
    IBOutlet UILabel *label1;
    IBOutlet UILabel *label2;
    
    NSDictionary *dataSource;
    NSString *currentKey;
}

@end

@implementation ViewController3

- (void)viewDidLoad {
    [super viewDidLoad];
    dataSource = @{@"1":@[@"A",@"B",@"C",@"D"],
                   @"2":@[@"1",@"2",@"3",@"4"],
                   @"3":@[@"aa",@"bb",@"cc",@"dd"]};
    currentKey = [dataSource.allKeys firstObject];
    
    label1.text = currentKey;
    label2.text = [[dataSource objectForKey:currentKey] objectAtIndex:0];
}

#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (component == 0) {
        return [dataSource.allKeys count];
    } else{
        NSArray *array = [dataSource objectForKey:currentKey];
        return array.count;
    }
}

#pragma mark - UIPickerViewDelegate

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    if (component == 0) {
        return [dataSource.allKeys objectAtIndex:row];
    } else{
        NSArray *array = [dataSource objectForKey:currentKey];
        return [array objectAtIndex:row];
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (component == 0) {
        // need reload componet 2!!!
        currentKey = [dataSource.allKeys objectAtIndex:row];
        label1.text = currentKey;
        [pickerView reloadComponent:1];
        [pickerView selectRow:0
                  inComponent:1 animated:YES];
        label2.text = [[dataSource objectForKey:currentKey] objectAtIndex:0];
        
    }else {
        label2.text = [[dataSource objectForKey:currentKey] objectAtIndex:row];
    }
}

@end
