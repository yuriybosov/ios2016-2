//
//  ViewController.m
//  lesson_35_PickerView
//
//  Created by Yurii Bosov on 12/4/16.
//  Copyright © 2016 comfy. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UIPickerViewDataSource, UIPickerViewDelegate> {
    IBOutlet UIPickerView *pickeView;
    IBOutlet UILabel *label;
    NSArray *dataSource;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    dataSource = @[@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8"];
    
    NSUInteger selectedIndex = dataSource.count/2;
    [pickeView selectRow:selectedIndex
             inComponent:0
                animated:NO];
    label.text = [dataSource objectAtIndex:selectedIndex];
}

#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return dataSource.count;
}

#pragma mark - UIPickerViewDelegate

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [dataSource objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    label.text = [dataSource objectAtIndex:row];
}

@end
