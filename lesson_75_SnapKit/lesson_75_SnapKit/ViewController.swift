//
//  ViewController.swift
//  lesson_75_SnapKit
//
//  Created by Yurii Bosov on 7/25/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

import UIKit
import SnapKit

class ViewController: UIViewController {
    
    let view1 = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view1.backgroundColor = .red
        view.addSubview(view1)
        
        view1.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.width.height.equalTo(min(view.frame.size.width,
                                   view.frame.size.height))
        }
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .all
    }
}

