//
//  MyViewController.m
//  lesson 16_UIView
//
//  Created by Yuriy Bosov on 9/22/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "MyViewController.h"
#import "AppDelegate.h"

@interface MyViewController ()

@end

@implementation MyViewController

#pragma mark - button actions

- (IBAction)buttonClicked {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    // делаем замену rootViewController у окна приложения
    // получаем доступ на appdelegate
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    // назначам новый rootViewController
    appDelegate.window.rootViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"vc1"];
}

@end
