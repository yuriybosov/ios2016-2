//
//  ViewController.m
//  lesson 16_UIView
//
//  Created by Yuriy Bosov on 9/22/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"

@interface ViewController ()

@end

@implementation ViewController

#pragma mark - init
// описываем все методы инициализации
// !!! в init-методах не работаем с UI !!!
// 1. этот инит вызывается при создании контроллера программно
- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

// 2. этот инит вызывается при создании контроллера из сторибоарда
- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        
    }
    return self;
}

// 3. этот инит вызывается при создании контроллера из nib-файла
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

#pragma mark - memory warning

// при нехватке памяти система пожет послать уведомление о нехватке памяти. в этом случае вызывается этот метод. в нем нужно максимально очистить память (к примеру, удалить картинки, медио-файлы, большие обьемы данных)
- (void)didReceiveMemoryWarning{
    ;
}

#pragma mark - view

// жизненый цикл view
// 1. viewDidLoad - вызывается один раз перед тем, как экран с view будет показан. В этом методе можно начинать работать с UI
- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"%s", __PRETTY_FUNCTION__);
    NSLog(@"frame = %@", NSStringFromCGRect(self.view.frame));
}

// 2. viewWillAppear - вызывается перед каждым показом контроллера
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    NSLog(@"%s", __PRETTY_FUNCTION__);
    NSLog(@"frame = %@", NSStringFromCGRect(self.view.frame));
}
// 3. viewDidAppear - вызывается после каждого показом контроллера
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    NSLog(@"%s", __PRETTY_FUNCTION__);
    NSLog(@"frame = %@", NSStringFromCGRect(self.view.frame));
}

// 4. viewWillDisappear - вызывается перед каждым скрытием контроллера
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

// 5. viewDidDisappear - вызывается после каждого скрытием контроллера
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

#pragma mark - button actions

- (IBAction)buttonClicked {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // делаем замену rootViewController у окна приложения
    // получаем доступ на appdelegate
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    // назначам новый rootViewController
    appDelegate.window.rootViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"vc2"];
}

@end
