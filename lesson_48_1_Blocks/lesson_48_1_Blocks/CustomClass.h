//
//  CustomClass.h
//  lesson_48_1_Blocks
//
//  Created by Yurii Bosov on 2/26/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^CustomBlock) (NSInteger param1, NSInteger param2, NSInteger param3, NSInteger param4);

@interface CustomClass : NSObject {
    NSInteger var;
}

// объявление блока как свойство класса
@property (nonatomic, copy) void (^block) (NSInteger, NSInteger);
@property (nonatomic, assign) NSInteger property;
@property (nonatomic, copy) CustomBlock customBlock;

// объявление метода, который принимает блок
- (void)testMethodWithBlock:(void(^)(NSInteger, NSInteger))block;
- (void)testMethod2WithBlock:(CustomBlock)customBlock;

- (void)executeBlock;

- (void)test;

@end
