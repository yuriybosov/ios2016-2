//
//  CustomClass.m
//  lesson_48_1_Blocks
//
//  Created by Yurii Bosov on 2/26/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "CustomClass.h"

@implementation CustomClass

- (void)dealloc {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (void)testMethodWithBlock:(void (^)(NSInteger, NSInteger))block {
    self.block = block;
}

- (void)testMethod2WithBlock:(CustomBlock)customBlock {
    
}

- (void)executeBlock {
    
    if (self.block) {
        self.block(1,1);
    }
}

- (void)test {

    __weak typeof(self) weakSelf = self;
    
    [self testMethodWithBlock:^(NSInteger param1, NSInteger param2) {
        if (weakSelf) {
            __strong typeof(weakSelf) strongSelf = weakSelf;
            strongSelf->var = param1;
            strongSelf.property = param2;
        }
    }];
    [self executeBlock];
}

@end
