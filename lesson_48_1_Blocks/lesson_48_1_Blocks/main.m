//
//  main.m
//  lesson_48_1_Blocks
//
//  Created by Yurii Bosov on 2/26/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CustomClass.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        // примеры объявление и использование блоков
        // <тип возвращаемого значение> (^название блока) (типы входящих параметров)
        
        // 1. блок который ничего не возвращает и ничего не принимает
        // 1.1 - обьявление блока
        void (^block) (void);
        // 1.2 - инициализация блока
        block = ^(void) {
            NSLog(@"вызов блока без параметров");
        };
        // 1.3 - вызов блока
        block();

        // 2. блок который ничего не возвращает, но принимает два параметра BOOL и CGLoat
        // 2.1 - объявление и инициализация
        void (^blockWithTwoParams) (BOOL, CGFloat) = ^(BOOL param1, CGFloat param2) {
            NSLog(@"%i, %f", param1, param2);
        };
        // 2.2 - вызов блока
        blockWithTwoParams(YES, 10.222);
        
        // 3. блок который возвращает результат и принимает параметры
        BOOL (^isChetnoe) (NSInteger) = ^(NSInteger chislo) {
            BOOL result = (chislo % 2 == 0);
            return result;
        };
        NSLog(@"%i", isChetnoe(10));
        NSLog(@"%i", isChetnoe(11));
        
        // Блоки и переменные
        // если внутри блока нужно поменять значение переменной, то при объявлении этой переменной нужно поставить __block
        __block NSInteger i = 0;
        
        void (^testBlock)() = ^(){
            i++;
            NSLog(@"%li", i);
        };
        testBlock();
        
        // Блоки в пользовательских классах
        CustomClass *obj = [CustomClass new];

        // __weak нужно писать, если работаешь внутри блока с объектов, которому принадлежит этот блок
        __weak typeof(obj) weakObj = obj;
        
        [obj testMethodWithBlock:^(NSInteger param1, NSInteger param2) {
            NSLog(@"%@, %li, %li", weakObj, param1, param2);
        }];
        [obj executeBlock];
        
        [obj test];
        
        [obj testMethod2WithBlock:^(NSInteger param1, NSInteger param2, NSInteger param3, NSInteger param4) {
            ;
        }];
    }
    return 0;
}
