//
//  ViewController.m
//  lesson 19_UITableView
//
//  Created by Yuriy Bosov on 10/2/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "ViewController.h"
#import "MyTableViewCell.h"

@interface ViewController () <UITableViewDataSource, UITableViewDelegate>

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

#pragma mark - Rotation

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return  UIInterfaceOrientationMaskAll;
}

// UITableViewDataSource - это протокол, который отвечает за наполнение таблицы
#pragma mark - UITableViewDataSource

// numberOfSectionsInTableView: - возращает кол-во секций, метод опциональный, по умолчанию всегда 1 секция
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}

// tableView:numberOfRowsInSection: - возращает кол-во ячеек для секции (для какой именно секции мы можем понять по параметру section)
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger result = 0;
    
    switch (section) {
        case 0:
            result = 10;
            break;
        case 1:
            result = 5;
            break;
        case 2:
            result = 20;
            break;
        case 3:
            result = 4;
            break;
        default:
            break;
    }
    
    return result;
}

// tableView:cellForRowAtIndexPath: - метод должен вернуть ячейку для indexPath. Так же в этом методе нужно заполнить данными нашу ячейку
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *cellID = nil;
    switch (indexPath.section) {
        case 0:
            cellID = @"cellID1";
            break;
        case 1:
            cellID = @"cellID2";
            break;
        case 2:
            cellID = @"cellID3";
            break;
        case 3:
            cellID = @"cellID4";
            break;
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    if (indexPath.section == 0) {
        if (cell.selectedBackgroundView.backgroundColor != [UIColor purpleColor])  {
            cell.selectedBackgroundView = [[UIView alloc]init];
        }
        cell.selectedBackgroundView.backgroundColor = [UIColor purpleColor];
    }
    
    if (indexPath.section == 1) {
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    if ([cell isKindOfClass:[MyTableViewCell class]]) {
    
        ((MyTableViewCell *)cell).myLabel.text = [NSString stringWithFormat:@"s = %li, r = %li", indexPath.section, indexPath.row];
    }
    
    cell.textLabel.text = [NSString stringWithFormat:@"section %li", indexPath.section];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"row %li", indexPath.row];
    
    return cell;
}

#pragma mark - UITableViewDelegate

// tableView:didSelectRowAtIndexPath: - метод вызывается при нажатии на ячейку
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // убираем выделение с ячейки
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSLog(@"Select %@", indexPath);
}

@end
