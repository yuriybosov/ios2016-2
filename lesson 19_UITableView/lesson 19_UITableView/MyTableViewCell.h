//
//  MyTableViewCell.h
//  lesson 19_UITableView
//
//  Created by Yuriy Bosov on 10/2/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *myLabel;

@end
