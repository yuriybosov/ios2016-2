//
//  MyTableViewCell.m
//  lesson 19_UITableView
//
//  Created by Yuriy Bosov on 10/2/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "MyTableViewCell.h"

@implementation MyTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
