//
//  Postamat.swift
//  lesson_70_Swift_Request
//
//  Created by Yurii Bosov on 7/2/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

import UIKit
import Alamofire

class Postamat: NSObject {
    var id: String?
    var title: String?
    var imgPath: String?
    
    init(data: [String:Any]) {
        if let id = data["id"] as? String {
            self.id = id
        }
        if let title = data["title"] as? String {
            self.title = title
        }
        if let imgPath = data["img"] as? String {
            self.imgPath = imgPath
        }
    }
    
    static func getPostamats(complition: @escaping([Postamat]?, Error?) -> Void) {
        // start request
        // parse responce
        // call complition block
        if let url = URL(string: "https://bpk-postamat.privatbank.ua/api/GetActiveServices?fmt=json") {
            request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).validate().responseJSON(completionHandler: { data in
                
                switch data.result {
                case .success(let value):
                    
                    var postomats = [Postamat]()
                    if let jsonData = value as? [String:Any] {
                        if let services = jsonData["services"] as? [[String: Any]] {
                            for postamatData in services {
                                postomats.append(Postamat(data: postamatData))
                            }
                        }
                    }
                    postomats = postomats.filter({ (obj) -> Bool in
                        var result = false
                        if  let id = obj.id,
                            let title = obj.title,
                            let imgPath = obj.imgPath {
                            result = !id.isEmpty && !title.isEmpty && !imgPath.isEmpty
                        }
                        return result
                    })
                    complition(postomats, nil)
                    
                case .failure(let error):
                    complition(nil, error)
                }
            })
        } else {
            print("incorect url")
            complition(nil, nil)
        }
    }
}
