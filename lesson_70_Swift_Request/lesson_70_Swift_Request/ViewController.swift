//
//  ViewController.swift
//  lesson_70_Swift_Request
//
//  Created by Yurii Bosov on 7/2/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

import UIKit
import SVProgressHUD
import AlamofireImage

class ViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView?
    fileprivate var dataSource = [Postamat]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView?.delegate = self
        collectionView?.dataSource = self
        collectionView?.register(UINib(nibName: "PostamatCell", bundle: nil), forCellWithReuseIdentifier: "cellID")
        
        SVProgressHUD.show()
        Postamat.getPostamats { [weak self] (data, error) in
            
            SVProgressHUD.dismiss()
            
            if let dataArray = data {
                self?.showPostamats(dataArray)
            }
            if let errorData = error {
                self?.showRequestError(errorData)
            }
        }
    }
    
    private func showRequestError(_ error: Error) {
        //
    }
    
    private func showPostamats(_ data: [Postamat]) {
        dataSource.append(contentsOf: data)
        collectionView?.reloadData()
    }
}

extension ViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellID", for: indexPath)
        if let postamatCell = cell as? PostamatCell {
            let posatmat = dataSource[indexPath.row]
            postamatCell.title?.text = posatmat.title
            
            postamatCell.image?.image = nil
            if let path = posatmat.imgPath {
                if let url = URL(string: path) {
                    postamatCell.image?.af_setImage(withURL: url)
                }
            }
        }
        return cell
    }
}

extension ViewController: UICollectionViewDelegate {
}

extension ViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let columCount: CGFloat = 1
        let width: CGFloat = (collectionView.frame.size.width - (columCount + 1) * 5)/columCount
        return CGSize(width: width,
                      height: width/2)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5,
                            left: 5,
                            bottom: 5,
                            right: 5)
    }
}


