//
//  PostamatCell.swift
//  lesson_70_Swift_Request
//
//  Created by Yurii Bosov on 7/2/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

import UIKit

class PostamatCell: UICollectionViewCell {
    @IBOutlet weak var title: UILabel?
    @IBOutlet weak var image: UIImageView?
}
