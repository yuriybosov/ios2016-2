//
//  main.m
//  lesson 13_1_Protocol_Example
//
//  Created by Yuriy Bosov on 9/11/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Human.h"
#import "Printer.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        Human *human = [Human new];
        
        Printer *printer = [Printer new];
        printer.name = @"Canon LPB 2900";
        printer.currentPageCount = 10;
        printer.delegate = human;
        
        [printer printCountPage:8];
        
        [printer printCountPage:5];
        
    }
    return 0;
}
