//
//  Printer.m
//  lesson 13_1_Protocol_Example
//
//  Created by Yuriy Bosov on 9/11/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "Printer.h"

@implementation Printer

- (void)printCountPage:(NSUInteger)count {

    if (_currentPageCount > 0) {
        //
        [_delegate printer:self didStartWithPageCount:count];
        
        for (NSUInteger i = 1; i <= count; i++) {
            
            if (_currentPageCount > 0) {
                _currentPageCount--;
                [_delegate printer:self
          inProcessWithCurrentPage:i
                         totalPage:count];
            } else {
                [_delegate printer:self
              didFailedWithMessage:@"закончилась бумага"];
                break;
            }
            
            if (i == count) {
                [_delegate printerDidEnd:self];
            }
        }
        
    } else {
        [_delegate printer:self
      didFailedWithMessage:@"нет бумаги"];
    }

}

@end
