//
//  PrinterProtocol.h
//  lesson 13_1_Protocol_Example
//
//  Created by Yuriy Bosov on 9/11/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#ifndef PrinterProtocol_h
#define PrinterProtocol_h

#import <Foundation/Foundation.h>

@class Printer;

@protocol PrinterProtocol <NSObject>

- (void)printer:(Printer *)printer didStartWithPageCount:(NSUInteger)count;
- (void)printerDidEnd:(Printer *)printer;
- (void)printer:(Printer *)printer inProcessWithCurrentPage:(NSUInteger)currentPage totalPage:(NSUInteger)totalPage;
- (void)printer:(Printer *)printer didFailedWithMessage:(NSString *)errorMessage;

@end

#endif /* PrinterProtocol_h */
