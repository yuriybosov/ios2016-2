//
//  Printer.h
//  lesson 13_1_Protocol_Example
//
//  Created by Yuriy Bosov on 9/11/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PrinterProtocol.h"

@interface Printer : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) NSUInteger currentPageCount;
@property (nonatomic, weak) id<PrinterProtocol> delegate;

- (void)printCountPage:(NSUInteger)count;

@end
