//
//  Human.m
//  lesson 13_1_Protocol_Example
//
//  Created by Yuriy Bosov on 9/11/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "Human.h"
#import "Printer.h"

@implementation Human

- (void)printer:(Printer *)printer didStartWithPageCount:(NSUInteger)count {
    NSLog(@"принтер %@ начал печать %lu страниц", printer.name, count);
}

- (void)printer:(Printer *)printer inProcessWithCurrentPage:(NSUInteger)currentPage totalPage:(NSUInteger)totalPage {
    NSLog(@"принтер %@ напечатал %lu из %lu страниц", printer.name, currentPage, totalPage);
}

- (void)printerDidEnd:(Printer *)printer {
    NSLog(@"принтер %@ завершил работу", printer.name);
}

- (void)printer:(Printer *)printer didFailedWithMessage:(NSString *)errorMessage {
    NSLog(@"принтер %@ сообщил об ошибке %@", printer.name, errorMessage);
}

@end
