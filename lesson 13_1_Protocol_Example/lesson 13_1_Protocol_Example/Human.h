//
//  Human.h
//  lesson 13_1_Protocol_Example
//
//  Created by Yuriy Bosov on 9/11/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PrinterProtocol.h"

@interface Human : NSObject <PrinterProtocol>

@end
