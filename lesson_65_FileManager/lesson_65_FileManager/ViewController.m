//
//  ViewController.m
//  lesson_65_FileManager
//
//  Created by Yurii Bosov on 6/18/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

#pragma mark - Button Action

- (IBAction)createFolderInDocumentDirectory:(id)sender {
    // для работы в файловой системой используем NSFileManager;
    // 
}

- (IBAction)createFolderInCasheDirectory:(id)sender {
    
}

- (IBAction)createFolderInTempDirectory:(id)sender {
    
}


@end
