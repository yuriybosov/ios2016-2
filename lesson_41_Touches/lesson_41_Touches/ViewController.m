//
//  ViewController.m
//  lesson_41_Touches
//
//  Created by Yurii Bosov on 12/25/16.
//  Copyright © 2016 ios. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {
    IBOutlet UIView *rect;
    
    CGPoint deltaPoint;
    BOOL rectIsSelected;
}

@end

@implementation ViewController

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    // проверяем что тач находится в области квадрата
    UITouch *touch = touches.anyObject;
    
    // определяем координаты тача относительно вью
    CGPoint point = [touch locationInView:self.view];
    
    // проверяем что координаты "попали" в наш квадрат
    if(CGRectContainsPoint(rect.frame, point)) {
        
        rectIsSelected = YES;
        
        // "попали" в квадрат
        // сделаем выделение квадратику
        rect.layer.borderWidth = 2;
        rect.layer.borderColor = [[UIColor redColor] CGColor];
        
        deltaPoint = CGPointMake(rect.center.x - point.x,
                                 rect.center.y - point.y);
    }
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    // проверим, выделен ли наш квадратик
    if (rectIsSelected) {
        // если да - то начинаем его перемещать
        UITouch *touch = touches.anyObject;
        CGPoint point = [touch locationInView:self.view];
        CGPoint newPoint = CGPointMake(point.x + deltaPoint.x,
                                       point.y + deltaPoint.y);
        // x
        if (newPoint.x < rect.frame.size.width/2) {
            newPoint.x = rect.frame.size.width/2;
        } else if (newPoint.x > self.view.frame.size.width - rect.frame.size.width/2) {
            newPoint.x = self.view.frame.size.width - rect.frame.size.width/2;
        }
        
        // y
        if (newPoint.y < rect.frame.size.height/2 + 20) {
            newPoint.y = rect.frame.size.height/2 + 20;
        } else if (newPoint.y > self.view.frame.size.height - rect.frame.size.height/2) {
            newPoint.y = self.view.frame.size.height - rect.frame.size.height/2;
        }
        
        rect.center = newPoint;
    }
    
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    rectIsSelected = NO;
    // уберем выделение квадратику
    rect.layer.borderWidth = 0;
    rect.layer.borderColor = [[UIColor clearColor] CGColor];
}

- (void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    rectIsSelected = NO;
    // уберем выделение квадратику
    rect.layer.borderWidth = 0;
    rect.layer.borderColor = [[UIColor clearColor] CGColor];
}

@end
